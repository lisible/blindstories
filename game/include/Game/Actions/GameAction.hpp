#ifndef GAME_ACTIONS_GAMEACTION_HPP
#define GAME_ACTIONS_GAMEACTION_HPP

#include <string>
#include <vector>

namespace game
{
namespace actions
{
    /**
     * Represents an action
     */
    class GameAction
    {
    private:
        // The action identifier
        std::string action;
        // The subjects of the action
        std::vector<std::string> subjects;
        // The linkers of the action
        std::vector<std::string> linkers;

    public:
        GameAction();
        ~GameAction();

        /**
         * Sets the action
         * @param actionIdentifier The identifier of the action
         */
        void setAction(const std::string& actionIdentifier);
        /**
         * Returns the action identifier
         * @return The action identifier
         */
        const std::string& getAction() const;


        /**
         * Adds a subject
         * @param subject The subject to add
         */
        void addSubject(const std::string& subject);
        /**
         * Returns the i-th subject
         * @param i The index of the subject
         * @return The i-th subject
         */
        const std::string& getSubject(size_t i) const;
        /**
         * Returns the number of subjects
         * @return The number of subjects
         */
        size_t getSubjectCount() const;

        /**
         * Adds a linker
         * @param linker The linker to add
         */
        void addLinker(const std::string& linker);
        /**
         * Returns the i-th linker
         * @param i The index of the linker
         * @return The i-th linker
         */
        const std::string& getLinker(size_t i) const;
        /**
         * Returns the number of linkers
         * @return The number of linkers
         */
        size_t getLinkerCount() const;
    };
}
}

#endif