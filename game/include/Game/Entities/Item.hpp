#ifndef GAME_ENTITIES_ITEM_HPP
#define GAME_ENTITIES_ITEM_HPP

#include <string>

namespace game
{
namespace entities
{
    class Item
    {
    private:
        std::string identifier;
    public:
        Item(const std::string& identifier);
        ~Item();

        const std::string& getIdentifier() const;
    };
}
}

#endif