#ifndef GAME_ENTITIES_ITEMDESCRIPTION_HPP
#define GAME_ENTITIES_ITEMDESCRIPTION_HPP

namespace game
{
namespace entities
{
    struct ItemDescription
    {
        std::string name;
        std::string description;
    };
}
}

#endif