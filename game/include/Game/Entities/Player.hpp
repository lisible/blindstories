#ifndef GAME_ENTITIES_PLAYER_HPP
#define GAME_ENTITIES_PLAYER_HPP

#include "World/Direction.hpp"
#include "Entities/Item.hpp"
#include <map>

#include <PotatoEngine/Maths/Maths.hpp>

namespace game
{
namespace entities
{
    class Player
    {
    private:
        // The position of the player
        pem::vec2i position;
        // The health of the player
        int health;
        // Player inventory
        std::map<unsigned, Item> inventory;
    public:
        Player();
        ~Player();

        void setPosition(unsigned x, unsigned y);
        const pem::vec2i& getPosition() const;

        void move(world::Direction direction);

        const std::map<unsigned, Item>& getInventory() const;

    };
}
}

#endif