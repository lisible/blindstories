#ifndef GAME_UI_GUIMAP_HPP
#define GAME_UI_GUIMAP_HPP

#include <PotatoEngine/UI/GUIComponent.hpp>
#include <PotatoEngine/Maths/Vector2.hpp>
#include "World/World.hpp"

namespace game
{
namespace ui
{
    class GUIMap : public pe::ui::GUIComponent
    {
    private:
        world::World& world;

        pem::vec2u playerPosition;
    public:
        GUIMap(const std::string& identifier, world::World& world);
        ~GUIMap() override;

        PEvoid setPlayerPosition(unsigned x, unsigned y);

        PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform) override;
    };
}
}


#endif