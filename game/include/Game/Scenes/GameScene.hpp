#ifndef GAME_SCENES_GAMESCENE_HPP
#define GAME_SCENES_GAMESCENE_HPP

#include <PotatoEngine/types.hpp>
#include <PotatoEngine/Scene/Scene.hpp>

#include "Actions/GameAction.hpp"

#include "ExpressionParser/ActionAnalyzer.hpp"

#include "World/World.hpp"
#include "World/WorldGenerator.hpp"
#include "Entities/Player.hpp"
#include "Entities/ItemDescription.hpp"
#include "UI/GUIMap.hpp"

#include <iostream>
#include <unordered_map>

namespace game
{
namespace scenes
{
	class GameScene : public pe::scene::Scene
	{
    private:
        // The action analyzer
        expressionparser::ActionAnalyzer analyzer;

        // The world
        world::World world;
        world::WorldGenerator worldGenerator;

        // The item registry
        std::unordered_map<std::string, entities::ItemDescription> itemRegistry;

        // The player
        entities::Player player;

        ui::GUIMap* map;

        std::string previousInput;
        std::string typedText;
    private:

    public:
        GameScene();
        ~GameScene() override;

        /**
         * Returns the identifier of the ui of the scene
         * @return The identifier of the ui of the scene
         */
        std::string getUIIdentifier() const override;

        /**
         * Run on initialization
         */
        PEvoid onInitialization() override;

        /**
         * Run on each tick
         */
        PEvoid onTick() override;

        /**
         * On action pressed
         * @param action The action
         */
        PEvoid onActionPressed(const std::string& action) override;

        /**
         * Handles the typed text
         * @param text The typed text
         */
        PEvoid onTextTyped(const std::string& text) override;

    private:
        void loadBiomes();
        void loadResources();
        void loadItems();
        void initActions();
        void initWorld();

        void setMessage(const std::string& message);

        void move(world::Direction d);
        void describeSituation();
        void describePlayerInventory();

        void onExit();
        void onPost();
        void onTTSRepeat();
        void onBackspace();
        void onPrevious();
        void updateTextInput();


        // Actions
        void actionGo(const actions::GameAction& action);
        void actionCheck(const actions::GameAction& action);
        void actionTake(const actions::GameAction& action);
	};
}
}

#endif
