#ifndef GAME_SCENES_SCENEFACTORY_HPP
#define GAME_SCENES_SCENEFACTORY_HPP

#include <PotatoEngine/Scene/ISceneFactory.hpp>

namespace game
{
namespace scenes
{
    /**
     * The scene factory of the game
     */
    class SceneFactory : public pe::scene::ISceneFactory
    {
    public:
        SceneFactory();
        ~SceneFactory() override;

        /**
         * Creates a scene corresponding to the identifier
         * @param sceneIdentifier The identifier of the scene
         * @return The corresponding scene
         */
        virtual pe::scene::Scene* createScene(const std::string& sceneIdentifier) override;


    };
}
}

#endif