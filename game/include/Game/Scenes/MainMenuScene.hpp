#ifndef GAME_SCENES_MAINMENUSCENE_HPP
#define GAME_SCENES_MAINMENUSCENE_HPP

#include <PotatoEngine/types.hpp>
#include <PotatoEngine/Scene/Scene.hpp>


namespace game
{
namespace scenes
{
	class MainMenuScene : public pe::scene::Scene
	{
		public:
			MainMenuScene();
			~MainMenuScene();

			/**
			 * Returns the identifier of the ui of the scene
			 * @return The identifier of the ui of the scene
			 */
			std::string getUIIdentifier() const override;
	
			/**
 			 * Run on initialization
 			 */		
			PEvoid onInitialization() override;	

			/**
 			 * Run on each tick
 			 */
			PEvoid onTick() override;

			/**
			 * On action pressed
			 * @param action The action
			 */
			PEvoid onActionPressed(const std::string& action) override;

        private:
#ifdef _WIN32
            PEvoid openTwitterPageW32();
#elif  __linux__
			PEvoid openTwitterPageUNIX();
#endif
	};
}
}

#endif
