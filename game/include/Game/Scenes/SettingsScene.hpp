#ifndef GAME_SCENES_SETTINGSSCENE_HPP
#define GAME_SCENES_SETTINGSSCENE_HPP

#include <PotatoEngine/Scene/Scene.hpp>

namespace game
{
namespace scenes
{
    class SettingsScene : public pe::scene::Scene
    {
    private:
    public:
        SettingsScene();
        ~SettingsScene();

        /**
         * Returns the identifier of the ui of the scene
         * @return The identifier of the ui of the scene
         */
        std::string getUIIdentifier() const override;

        /**
          * Run on initialization
          */
        PEvoid onInitialization() override;

        /**
          * Run on each tick
          */
        PEvoid onTick() override;

        /**
         * On action pressed
         * @param action The action
         */
        PEvoid onActionPressed(const std::string& action) override;
    };
}
}

#endif