#ifndef GAME_SCENES_SPLASHSCREENSCENE_HPP
#define GAME_SCENES_SPLASHSCREENSCENE_HPP

#include <PotatoEngine/types.hpp>
#include <PotatoEngine/Scene/Scene.hpp>

namespace game
{
namespace scenes
{
	class SplashScreenScene : public pe::scene::Scene
	{
		private:
			PEvoid createUI();

			PEuint64 cpt;
		public:
			SplashScreenScene();
			~SplashScreenScene();

			/**
			 * Returns the identifier of the ui of the scene
			 * @return The identifier of the ui of the scene
			 */
			std::string getUIIdentifier() const override;
	
			/**
 			 * Run on initialization
 			 */		
			PEvoid onInitialization() override;	

			/**
 			 * Run on each tick
 			 */
			PEvoid onTick() override;
	};
}
}

#endif
