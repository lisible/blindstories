#ifndef GAME_EXPRESSIONPARSER_TOKEN_HPP
#define GAME_EXPRESSIONPARSER_TOKEN_HPP

#include "ExpressionParser/WordType.hpp"
#include <string>

namespace game
{
namespace expressionparser
{
    class Token
    {
    private:
        std::string identifier;
        WordType type;
    public:
        Token();
        ~Token();

        void setIdentifier(const std::string& identifier);
        const std::string& getIdentifier() const;

        void setType(WordType type);
        WordType getType() const;
    };
}
}

#endif