#ifndef GAME_EXPRESSIONPARSER_WORDTYPE_HPP
#define GAME_EXPRESSIONPARSER_WORDTYPE_HPP

namespace game
{
namespace expressionparser
{
    enum class WordType
    {
        ACTION,
        SUBJECT,
        LINKER,
        WHITESPACE,
        UNKNOWN
    };
}
}

#endif