#ifndef GAME_EXPRESSIONPARSER_WORD_HPP
#define GAME_EXPRESSIONPARSER_WORD_HPP

#include "ExpressionParser/WordType.hpp"
#include <string>
#include <regex>

namespace game
{
namespace expressionparser
{
    class Word
    {
    private:
        // The identifier of the word
        std::string identifier;
        // The type of the word
        WordType type;
        // The regexes of the word
        std::vector<std::regex> regexes;
    public:
        Word();
        ~Word();

        /**
         * Sets the identifier of the word
         * @param identifier The identifier
         */
        void setIdentifier(const std::string& identifier);
        /**
         * Returns the identifier of the word
         * @return The identifier of the word
         */
        const std::string& getIdentifier() const;

        /**
         * Sets the type of the word
         * @param type The type
         */
        void setType(WordType type);
        /**
         * Reutnrs the type of the word
         * @return The type of the word
         */
        WordType getType() const;

        /**
         * Adds a regex to the word
         * @param regex The regex to add
         */
        void addRegex(const std::regex& regex);
        /**
         * Returns the regexes corresponding to the word
         * @return The regexes corresponding to the word
         */
        const std::vector<std::regex>& getRegexes() const;
    };
}
}

#endif