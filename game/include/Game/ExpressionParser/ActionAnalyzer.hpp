#ifndef GAME_EXPRESSIONPARSER_ACTIONANALYZER_HPP
#define GAME_EXPRESSIONPARSER_ACTIONANALYZER_HPP

#include "Actions/GameAction.hpp"
#include "ExpressionParser/WordType.hpp"
#include "ExpressionParser/Word.hpp"
#include "ExpressionParser/Lexer.hpp"
#include "ExpressionParser/Parser.hpp"
#include <vector>
#include <string>

namespace game
{
namespace expressionparser
{
    class ActionAnalyzer
    {
    private:
        std::vector<Word> tokens;
        Lexer lexer;
        Parser parser;
    public:
        ActionAnalyzer();
        ~ActionAnalyzer();

        /**
         * Adds a word to the analyzer
         * @param wordIdentifier The identifier of the word
         * @param wordType The type of the word
         * @param regexCount The number of regexes for the word
         * @param ... The regexes for the word
         */
        void addWord(const std::string& wordIdentifier, WordType wordType, unsigned regexCount, ...);

        /**
         * Registers a callback for the given action
         * @param actionIdentifier The action to run the callback for
         * @param callback The callback
         */
        void addCallback(const std::string& actionIdentifier, std::function<void(const actions::GameAction&)> callback);

        /**
         * Analyzes a text
         * @param text The text to analyze
         */
        void analyze(const std::string& text);
    };
}
}

#endif