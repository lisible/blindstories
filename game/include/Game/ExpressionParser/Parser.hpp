#ifndef GAME_EXPRESSIONPARSER_PARSER_HPP
#define GAME_EXPRESSIONPARSER_PARSER_HPP

#include "Actions/GameAction.hpp"
#include "ExpressionParser/Token.hpp"
#include <functional>
#include <unordered_map>
#include <string>
#include <list>

namespace game
{
namespace expressionparser
{
    class Parser
    {
    private:
        std::unordered_map<std::string, std::function<void(const actions::GameAction&)>> callbacks;
    public:
        Parser();
        ~Parser();

        void addCallback(const std::string& action, std::function<void(const actions::GameAction&)> callback);
        void analyze(const std::list<Token>& tokenList);
    };
}
}

#endif