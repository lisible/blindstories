#ifndef GAME_EXPRESSIONPARSER_LEXER_HPP
#define GAME_EXPRESSIONPARSER_LEXER_HPP

#include "ExpressionParser/Word.hpp"
#include "ExpressionParser/Token.hpp"
#include <string>
#include <list>

namespace game
{
namespace expressionparser
{
    class Lexer
    {
    private:
        Token extractNextToken(std::string& text, const std::vector<Word>& words);
    public:
        Lexer();
        ~Lexer();

        std::list<Token> analyze(const std::string& inputText, const std::vector<Word>& words);
    };
}
}

#endif