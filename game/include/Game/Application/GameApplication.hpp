#ifndef GAME_APPLICATION_GAMEAPPLICATION_HPP
#define GAME_APPLICATION_GAMEAPPLICATION_HPP

#include <PotatoEngine/Application/Application.hpp>
#include <PotatoEngine/types.hpp>

namespace game
{
namespace app
{
	/**
 	 * Application class implementation for the game
 	 */	
	class GameApplication : public pe::app::Application
	{
		protected:
			/**
 			 * Initializes the application
 			 */
			PEvoid init() override;

		public:
			GameApplication();
			~GameApplication() override;
	};
}
}

#endif
