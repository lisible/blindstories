#ifndef GAME_WORLD_BIOMEGENERATIONDATA_HPP
#define GAME_WORLD_BIOMEGENERATIONDATA_HPP

#include "World/ResourceGenerationData.hpp"

namespace game
{
namespace world
{
    struct BiomeGenerationData
    {
        // The identifier of the biome
        std::string biomeIdentifier;
        // The minimum height for the biome
        unsigned minHeight;
        // The maximum height for the biome
        unsigned maxHeight;
        // The resource generation data of the biome
        std::vector<ResourceGenerationData> resourceGenerationDataList;
    };
}
}

#endif