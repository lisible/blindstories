#ifndef GAME_WORLD_DIRECTION_HPP
#define GAME_WORLD_DIRECTION_HPP

#include <string>

namespace game
{
namespace world
{
    enum class Direction
    {
        NORTH,
        EAST,
        WEST,
        SOUTH,
        UNKNOWN
    };

    inline int getRelativeX(Direction direction)
    {
        switch(direction)
        {
            case Direction::EAST:
                return 1;
            case Direction::WEST:
                return -1;
            case Direction::NORTH:
            case Direction::SOUTH:
            default:
                return 0;
        }
    }

    inline int getRelativeY(Direction direction)
    {
        switch(direction)
        {
            case Direction::SOUTH:
                return 1;
            case Direction::NORTH:
                return -1;
            case Direction::WEST:
            case Direction::EAST:
            default:
                return 0;
        }
    }

    inline Direction directionFromString(const std::string& str)
    {
        if(str == "east")
            return Direction::EAST;
        else if(str == "west")
            return Direction::WEST;
        else if(str == "north")
            return Direction::NORTH;
        else if(str == "south")
            return Direction::SOUTH;
        else
            return Direction::UNKNOWN;
    }
}
}

#endif