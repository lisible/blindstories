#ifndef GAME_WORLD_AREA_HPP
#define GAME_WORLD_AREA_HPP

#include <string>
#include <map>

namespace game
{
namespace world
{
    /**
     * Represents an area of the world
     */
    class Area
    {
    private:
        // The biome of the area
        std::string biomeIdentifier;
        double height;

        std::map<std::string, unsigned> resources;
    public:
        Area();
        ~Area();

        /**
         * Sets the biome of the area
         * @param biomeIdentifier The identifier of the biome of the area
         */
        void setBiomeIdentifier(const std::string &biomeIdentifier);
        /**
         * Returns the identifier of the biome of the area
         * @return The identifier of the biome of the area
         */
        const std::string& getBiomeIdentifier() const;

        void addResource(const std::string& resourceIdentifier, unsigned quantity);
        const std::map<std::string, unsigned>& getResources() const;

        void setHeight(double height);
        double getHeight() const;
    };
}
}

#endif