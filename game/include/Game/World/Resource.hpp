#ifndef GAME_WORLD_RESOURCE_HPP
#define GAME_WORLD_RESOURCE_HPP

#include "World/LootData.hpp"
#include <string>
#include <list>

namespace game
{
namespace world
{
    /**
     * Represents a resource in the world
     */
    class Resource
    {
    private:
        // The identifier of the resource
        std::string identifier;
        // The name of the resource
        std::string name;
        // The description of the resource
        std::string description;
        // The action to gather the resource
        std::string gatherAction;
        // The loots of the resource
        std::list<LootData> loots;
    public:
        Resource(const std::string& resourceIdentifier);
        Resource(const Resource& resource);
        ~Resource();

        /**
         * Returns the identifier of a resource
         * @return The identifier of a resource
         */
        const std::string& getIdentifier() const;

        /**
         * Sets the name of the resource
         * @param name The name of the resource
         */
        void setName(const std::string& name);
        /**
         * Returns the name of the resource
         * @return The name of the resource
         */
        const std::string& getName() const;

        /**
         * Sets the description of the resource
         * @param description The description of the reosurce
         */
        void setDescription(const std::string& description);
        /**
         * Returns the description of the resource
         * @return The description of the resource
         */
        const std::string& getDescription();

        /**
         * Adds a loot to the resource
         * @param loot The loot to add
         */
        void addLoot(const LootData& loot);
        /**
         * Returns the resource's loots
         * @return The resource's loots
         */
        const std::list<LootData>& getLoots() const;

        /**
         * Sets the gather action of the resource
         * @param action The action to gather the resource
         */
        void setGatherAction(const std::string& action);
        /**
         * Returns the gather action of the resource
         * @return The gather action of the resource
         */
        const std::string& getGatherAction() const;
    };
}
}

#endif