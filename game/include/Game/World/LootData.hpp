#ifndef GAME_WORLD_LOOTDATA_HPP
#define GAME_WORLD_LOOTDATA_HPP

#include <string>

namespace game
{
namespace world
{
    /**
     * Describes a loot of a resource
     */
    struct LootData
    {
        // The identifier of the loot item
        std::string itemIdentifier;
        // The minimum quantity of loot
        unsigned minimumQuantity;
        // The maximum quantity of loot
        unsigned maximumQuantity;
    };
}
}

#endif