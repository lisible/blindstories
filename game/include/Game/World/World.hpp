#ifndef GAME_WORLD_WORLD_HPP
#define GAME_WORLD_WORLD_HPP

#include "World/Area.hpp"
#include "World/Biome.hpp"
#include "World/Resource.hpp"

#include <vector>
#include <map>

namespace game
{
namespace world
{
    /**
     * Represents the world
     */
    class World
    {
    private:
        // The areas of the world
        std::vector<std::vector<Area>> areas;

        // The biomes of the world
        std::map<std::string, Biome> biomeRegistry;
        // The resources of the world
        std::map<std::string, Resource> resourceRegistry;
    public:
        World();
        ~World();

        /**
         * Sets the size of the world
         * @param size The size of the world
         */
        void setSize(unsigned size);
        /**
         * Returns the size of the world
         * @return The size of the world
         */
        unsigned getSize() const;

        /**
         * Registers the given biome in the biome registry
         * @param biome The biome to register
         */
        void registerBiome(const Biome& biome);
        /**
         * Returns the biome with the given identifier
         * @param biomeIdentifier The identifier of the biome
         * @return The biome with the given identifier
         */
        const Biome& getBiome(const std::string& biomeIdentifier) const;

        /**
         * Registers a resource
         * @param resource The resource to register
         */
        void registerResource(const Resource& resource);
        /**
         * Returns a resource
         * @param resourceIdentifier The identifier of the resource to fetch
         * @return The resource with the given identifier
         */
        const Resource& getResource(const std::string& resourceIdentifier);

        /**
         * Returns an area of the world
         * @param x The x coordinate of the area
         * @param y The y coordinate of the area
         * @return The area of the world at the given coordinates
         */
        Area& getArea(int x, int y);
        Area& getArea(const pem::vec2i& position);
    };
}
}

#endif