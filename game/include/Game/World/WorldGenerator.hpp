#ifndef GAME_WORLD_WORLDGENERATOR_HPP
#define GAME_WORLD_WORLDGENERATOR_HPP

#include "World/World.hpp"
#include "World/BiomeGenerationData.hpp"
#include "ThirdParty/PerlinNoise.hpp"

#include <list>


namespace game
{
namespace world
{
    class WorldGenerator
    {
    private:
        // Perlin noise used to generate the height of the terrain
        siv::PerlinNoise heightNoise;

        std::list<BiomeGenerationData> biomeGenerationDataList;
    private:
        void generateBiomes(World& world);
        void generateResources(World& world);

    public:
        WorldGenerator();
        ~WorldGenerator();

        /**
         * Seeds the generator
         * @param s The seed to use
         */
        void seed(unsigned s);

        /**
         * Adds a biome generation data to the generator
         * @param biomeGenerationData The biome generation data
         */
        void addBiomeGenerationData(const BiomeGenerationData& biomeGenerationData);

        /**
         * Generates the world
         * @param world The world to generate
         */
        void generate(World& world);
    };
}
}

#endif