#ifndef GAME_WORLD_RESOURCEGENERATIONDATA_HPP
#define GAME_WORLD_RESOURCEGENERATIONDATA_HPP

#include <string>

namespace game
{
namespace world
{
    struct ResourceGenerationData
    {
        std::string resourceIdentifier;
        unsigned minimumQuantity;
        unsigned maximumQuantity;
    };
}
}

#endif