#ifndef GAME_WORLD_BIOME_HPP
#define GAME_WORLD_BIOME_HPP

#include <string>
#include <PotatoEngine/Graphics/Color.hpp>

namespace game
{
namespace world
{
    /**
     * Represents a biome of the world
     */
    class Biome
    {
    private:
        // The identifier of the biome
        std::string identifier;
        // The name of the biome
        std::string name;
        // The description of the biome
        std::string description;

        // The debug color of the biome
        pe::graphics::Color debugColor;
    public:
        Biome(const Biome& biome);
        Biome(const std::string& identifier);
        ~Biome();

        /**
         * Returns the identifier of the biome
         * @return The identifier of the biome
         */
        const std::string& getIdentifier() const;

        /**
         * Sets the name of the biome
         * @param name The name of the biome
         */
        void setName(const std::string& name);
        /**
         * Returns the name of the biome
         * @return The name of the biome
         */
        const std::string& getName() const;

        /**
         * Sets the description of the biome
         * @param description The description of the biome
         */
        void setDescription(const std::string& description);
        /**
         * Returns the description of the biome
         * @return The description of the biome
         */
        const std::string& getDescription() const;

        /**
         * Sets the debug color of the biome
         * @param color The debug color
         */
        void setDebugColor(const pe::graphics::Color& color);
        /**
         * Returns the debug color of the biome
         * @return The debug color of the biome
         */
        const pe::graphics::Color& getDebugColor() const;
    };
}
}

#endif