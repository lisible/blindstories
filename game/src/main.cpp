#include <iostream>
#include "Application/GameApplication.hpp"

int main(int argc, char** argv)
{
	// Instancing and launching the application	
	game::app::GameApplication g;
	g.run();

#ifdef _WIN32
	//system("pause");
#endif

	return 0;
}
