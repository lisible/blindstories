#include "Actions/GameAction.hpp"

namespace game
{
namespace actions
{
    GameAction::GameAction()
        : action("unknown")
	{
	}
    GameAction::~GameAction()
	{
	}

    void GameAction::setAction(const std::string& actionIdentifier)
	{
        action = actionIdentifier;
	}
    const std::string& GameAction::getAction() const
	{
        return action;
	}

    void GameAction::addSubject(const std::string& subject)
	{
        subjects.push_back(subject);
	}
    const std::string& GameAction::getSubject(size_t i) const
	{
        return subjects[i];
	}
    size_t GameAction::getSubjectCount() const
    {
        return subjects.size();
    }

    void GameAction::addLinker(const std::string& linker)
	{
        linkers.push_back(linker);
	}
    const std::string& GameAction::getLinker(size_t i) const
	{
        return linkers[i];
	}
    size_t GameAction::getLinkerCount() const
    {
        return linkers.size();
    }
}
}