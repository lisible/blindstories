#include "UI/GUIMap.hpp"
#include <PotatoEngine/Graphics/Renderer2D.hpp>

namespace game
{
namespace ui
{
    GUIMap::GUIMap(const std::string& identifier, world::World& world)
        : GUIComponent(identifier)
        , world(world)
    {

    }
    GUIMap::~GUIMap()
    {

    }

    PEvoid GUIMap::setPlayerPosition(unsigned x, unsigned y)
    {
        playerPosition.x = x;
        playerPosition.y = y;
    }


    PEvoid GUIMap::render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform)
    {
        for(size_t y = 0; y < world.getSize(); y++)
        {
            for(size_t x = 0; x < world.getSize(); x++)
            {
                auto color = world.getBiome(world.getArea(x, y).getBiomeIdentifier()).getDebugColor();
                if(playerPosition.x == x && playerPosition.y == y)
                    color = pe::graphics::RED;

                renderer.drawRectangle(pem::Rectangle(x*4, y*4, 4, 4), color, getTransformMatrix() * transform);
            }
        }
    }
}
}