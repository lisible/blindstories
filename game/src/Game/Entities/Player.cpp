#include "Entities/Player.hpp"

namespace game
{
namespace entities
{
    Player::Player()
	{
	}
    Player::~Player()
	{
	}

	void Player::setPosition(unsigned x, unsigned y)
	{
		position.x = x;
        position.y = y;
	}
    void Player::move(world::Direction direction)
	{
        position.x += world::getRelativeX(direction);
        position.y += world::getRelativeY(direction);
	}

    const pem::vec2i& Player::getPosition() const
    {
        return position;
    }

    const std::map<unsigned, Item>& Player::getInventory() const
    {
        return inventory;
    }
}
}