#include "Entities/Item.hpp"

namespace game
{
namespace entities
{
    Item::Item(const std::string& identifier)
        :   identifier(identifier)
	{
	}
    Item::~Item()
	{
	}

    const std::string& Item::getIdentifier() const
	{
        return identifier;
	}
}
}