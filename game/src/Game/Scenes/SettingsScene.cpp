#include "Scenes/SettingsScene.hpp"

namespace game
{
namespace scenes
{
    SettingsScene::SettingsScene()
    {
    }
    SettingsScene::~SettingsScene()
    {
    }


    std::string SettingsScene::getUIIdentifier() const
    {
        return "settings_menu";
    }

    PEvoid SettingsScene::onInitialization()
    {

    }

    PEvoid SettingsScene::onTick()
    {

    }


    PEvoid SettingsScene::onActionPressed(const std::string& action)
    {
        if(action == "exit")
            pop_scene();
    }
}
}