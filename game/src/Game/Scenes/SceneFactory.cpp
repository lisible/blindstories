#include "Scenes/SceneFactory.hpp"
#include "Scenes/SplashScreenScene.hpp"
#include "Scenes/MainMenuScene.hpp"
#include "Scenes/SettingsScene.hpp"
#include "Scenes/GameScene.hpp"


namespace game
{
namespace scenes
{
    SceneFactory::SceneFactory()
    {
    }
    SceneFactory::~SceneFactory()
    {
    }

    pe::scene::Scene* SceneFactory::createScene(const std::string& sceneIdentifier)
    {
        if(sceneIdentifier == "SplashScreenScene")
            return new SplashScreenScene();
        if(sceneIdentifier == "MainMenuScene")
            return new MainMenuScene();
        if(sceneIdentifier == "GameScene")
            return new GameScene();
        if(sceneIdentifier == "SettingsScene")
            return new SettingsScene();

        return nullptr;
    }
}
}
