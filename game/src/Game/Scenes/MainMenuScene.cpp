#include "Scenes/MainMenuScene.hpp"
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#elif __linux__
#include <unistd.h>
#endif

namespace game
{
namespace scenes
{

	MainMenuScene::MainMenuScene()
	{
	}

	MainMenuScene::~MainMenuScene()
	{}

    std::string MainMenuScene::getUIIdentifier() const
    {
        return "main_menu";
    }

	PEvoid MainMenuScene::onInitialization()
	{
		register_click_callback("buttonPlay", [&]() { push_scene("GameScene"); });
		register_click_callback("buttonSettings", [&]() { push_scene("SettingsScene"); });
		register_click_callback("buttonQuit", [&]() { pop_scene(); });
		register_post_callback("buttonPlay", [&]() { push_scene("GameScene"); });
		register_post_callback("buttonSettings", [&]() { push_scene("SettingsScene"); });
		register_post_callback("buttonQuit", [&]() { pop_scene(); });

#ifdef _WIN32
        registerClickCallback("labelTwitter", [&](){openTwitterPageW32();});
        registerPostCallback("labelTwitter", [&](){openTwitterPageW32();});
#elif __linux__
		register_click_callback("labelTwitter", [&]() { openTwitterPageUNIX(); });
		register_post_callback("labelTwitter", [&]() { openTwitterPageUNIX(); });
#endif

		//tts_say("Welcome, use the arrow keys to navigate through the menu");
	}

	PEvoid MainMenuScene::onTick()
	{
	}

#ifdef _WIN32
    PEvoid MainMenuScene::openTwitterPageW32()
    {
        CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
        ShellExecute(NULL, "open", "https://twitter.com/claymeuns", NULL, NULL, SW_SHOWNORMAL);
    }
#elif  __linux__
	PEvoid MainMenuScene::openTwitterPageUNIX()
	{
		pid_t pid = fork();

		if(pid == 0)
        {
            close(STDOUT_FILENO);
            close(STDERR_FILENO);
            execlp("/usr/bin/xdg-open", "xdg-open", "https://twitter.com/claymeuns", NULL);
        }
	}
#endif

	PEvoid MainMenuScene::onActionPressed(const std::string& action)
	{
		if(action == "exit")
            pop_scene();
		else if(action == "next")
			ui_focus_next();
		else if(action == "previous")
			ui_focus_previous();
		else if(action == "post")
			ui_post();
	}
}
}
