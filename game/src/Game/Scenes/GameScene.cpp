#include "Scenes/GameScene.hpp"
#include "UI/GUIMap.hpp"

namespace game
{
namespace scenes
{
	GameScene::GameScene()
        : previousInput("")
		, typedText("")
	{}

	GameScene::~GameScene()
	{
        log_message("GameScene destructed");
    }

	std::string GameScene::getUIIdentifier() const
	{
		return "game_scene_debug";
	}

	PEvoid GameScene::onInitialization()
	{
        loadBiomes();
        loadResources();
        loadItems();
		initActions();
        initWorld();

        player.setPosition(world.getSize()/2, world.getSize()/2);

        map = new ui::GUIMap("map", world);
        map->setPosition(800-4*55,600-4*55);
        map->setPlayerPosition(player.getPosition().x, player.getPosition().y);
        ui_add_component("_root", map);

        describeSituation();
    }

	PEvoid GameScene::onTick()
	{
	}

	PEvoid GameScene::onActionPressed(const std::string& action)
	{
		if(action == "exit")
			onExit();
		else if(action == "ttsRepeat")
			onTTSRepeat();
		else if(action == "post")
			onPost();
		else if(action == "backspace")
			onBackspace();
        else if(action == "previous")
            onPrevious();
	}

	PEvoid GameScene::onTextTyped(const std::string& text)
	{
		// TODO change that
		if(typedText.length() < 50)
			typedText += text;
		updateTextInput();
	}

	void GameScene::updateTextInput()
	{
		ui_edit_attribute("labelPrompt", "text", "> " + typedText);
	}

	void GameScene::onExit()
	{
		// Popping the scene from the stack
		pop_scene();
	}
	void GameScene::onPost()
	{
		// Analyzing the input
		analyzer.analyze(typedText);

		// Erasing the typed text
        if(typedText != "")
            previousInput = typedText;

        typedText = "";
		updateTextInput();
	}
	void GameScene::onTTSRepeat()
	{
		tts_say(typedText);
	}
	void GameScene::onBackspace()
	{
		// Removing the last char of the input text
		if(typedText.begin() != typedText.end())
		{
			typedText = std::string(typedText.begin(), typedText.end()-1);
			updateTextInput();
		}
	}
    void GameScene::onPrevious()
    {
        typedText = previousInput;
        updateTextInput();
    }

    void GameScene::setMessage(const std::string& message)
    {
        ui_edit_attribute("textAreaDescription", "text", message);
        tts_say(message);
    }

    // TODO move that elsewhere
    void GameScene::loadBiomes()
    {
        auto biomeFile = open_data_file("biomes.xml");
        auto biomesRoot = biomeFile->FirstChildElement("biomes");
        auto biomeDescription = biomesRoot->FirstChildElement("biome");
        while(biomeDescription != nullptr)
        {
            const char* biomeIdentifier = biomeDescription->Attribute("id");
			auto biomeMinHeight = static_cast<unsigned>(biomeDescription->IntAttribute("minHeight"));
			auto biomeMaxHeight = static_cast<unsigned>(biomeDescription->IntAttribute("maxHeight"));
            const char* biomeName = biomeDescription->FirstChildElement("name")->GetText();
            const char* biomeDesc = biomeDescription->FirstChildElement("description")->GetText();
            const char* biomeDebugColor = biomeDescription->FirstChildElement("debugColor")->GetText();
            log_message(biomeName);
            log_message(biomeDesc);
            log_message("");

            world::Biome biome(biomeIdentifier);
            biome.setName(biomeName);
            biome.setDescription(biomeDesc);
            biome.setDebugColor(pe::graphics::Color(biomeDebugColor));
            world.registerBiome(biome);


			world::BiomeGenerationData biomeGenerationData;
			biomeGenerationData.biomeIdentifier = biomeIdentifier;
			biomeGenerationData.minHeight = biomeMinHeight;
			biomeGenerationData.maxHeight = biomeMaxHeight;

            auto biomeResourceRoot = biomeDescription->FirstChildElement("resources");
            auto biomeResourceDescription = biomeResourceRoot->FirstChildElement("resource");
            while(biomeResourceDescription != nullptr)
            {
                const char* resourceIdentifier = biomeResourceDescription->Attribute("id");
                unsigned resourceMinimumQuantity = static_cast<unsigned>(biomeResourceDescription->IntAttribute("minQuantity"));
                unsigned resourceMaximumQuantity = static_cast<unsigned>(biomeResourceDescription->IntAttribute("maxQuantity"));

                world::ResourceGenerationData resourceGenerationData;
                resourceGenerationData.resourceIdentifier = resourceIdentifier;
                resourceGenerationData.minimumQuantity = resourceMinimumQuantity;
                resourceGenerationData.maximumQuantity = resourceMaximumQuantity;

                biomeGenerationData.resourceGenerationDataList.push_back(resourceGenerationData);

                biomeResourceDescription = biomeResourceDescription->NextSiblingElement();
            }

			worldGenerator.addBiomeGenerationData(biomeGenerationData);

            biomeDescription = biomeDescription->NextSiblingElement();
        }
    }

    void GameScene::loadResources()
    {
        auto resourceFile = open_data_file("resources.xml");
        auto resourcesRoot = resourceFile->FirstChildElement("resources");
        auto resourceDescription = resourcesRoot->FirstChildElement("resource");
        while(resourceDescription != nullptr)
        {
            const char* resourceIdentifier = resourceDescription->Attribute("id");
            const char* resourceName = resourceDescription->FirstChildElement("name")->GetText();
            const char* resourceDesc = resourceDescription->FirstChildElement("description")->GetText();
            const char* resourceGatherAction = resourceDescription->FirstChildElement("gatherAction")->GetText();

            world::Resource resource(resourceIdentifier);
            resource.setName(resourceName);
            resource.setDescription(resourceDesc);
            resource.setGatherAction(resourceGatherAction);
            world.registerResource(resource);

            analyzer.addWord("resource_" + std::string(resourceIdentifier), expressionparser::WordType::SUBJECT, 1, "^" + std::string(resourceIdentifier));

            resourceDescription = resourceDescription->NextSiblingElement();
        }
    }

    void GameScene::loadItems()
    {
        auto itemFile = open_data_file("items.xml");
        auto itemsRoot = itemFile->FirstChildElement("items");
        auto itemDescription = itemsRoot->FirstChildElement("item");
        while(itemDescription != nullptr)
        {
            entities::ItemDescription itemDesc;
            const char* itemIdentifier = itemDescription->Attribute("id");
            itemDesc.name = itemDescription->FirstChildElement("name")->GetText();
            itemDesc.description = itemDescription->FirstChildElement("description")->GetText();
            itemRegistry[itemIdentifier] = itemDesc;
            itemDescription = itemDescription->NextSiblingElement();

            analyzer.addWord("item_" + std::string(itemIdentifier), expressionparser::WordType::SUBJECT, 1, "^" + std::string(itemIdentifier));
        }
    }

    void GameScene::initWorld()
    {
        world.setSize(50);
        worldGenerator.seed(54321);
        worldGenerator.generate(world);
    }

	void GameScene::initActions()
	{
		analyzer.addWord("go", expressionparser::WordType::ACTION, 4, "^go", "^move", "^walk", "^run");
        analyzer.addWord("cut", expressionparser::WordType::ACTION, 2, "^cut", "^chop");
        analyzer.addWord("take", expressionparser::WordType::ACTION, 1, "^take");
        analyzer.addWord("check", expressionparser::WordType::ACTION, 1, "^check");

		analyzer.addWord("north", expressionparser::WordType::SUBJECT, 1, "^north");
		analyzer.addWord("east", expressionparser::WordType::SUBJECT, 1, "^east");
		analyzer.addWord("west", expressionparser::WordType::SUBJECT, 1, "^west");
		analyzer.addWord("south", expressionparser::WordType::SUBJECT, 1, "^south");
        analyzer.addWord("inventory", expressionparser::WordType::SUBJECT, 3, "^inventory", "^bag", "^pocket");

		analyzer.addCallback("go", [&](const actions::GameAction& action){actionGo(action);});
        analyzer.addCallback("check", [&](const actions::GameAction& action){actionCheck(action);});
        analyzer.addCallback("take", [&](const actions::GameAction& action){actionTake(action);});

        // TODO
        // analyzer.addCallback("chop", [])
        // analyzer.addCallback("pick", [])
	}

    void GameScene::move(world::Direction d)
    {
        player.move(d);
        auto mapPositionX(static_cast<unsigned>(player.getPosition().x));
        auto mapPositionY(static_cast<unsigned>(player.getPosition().y));
        map->setPlayerPosition(mapPositionX, mapPositionY);

        describeSituation();
    }

    void GameScene::describeSituation()
    {
        std::string message;

        auto playerArea = world.getArea(player.getPosition());
        auto playerBiome = world.getBiome(playerArea.getBiomeIdentifier());

        message += "You're in " + playerBiome.getDescription() + ".\n";


        auto playerAreaResources = playerArea.getResources();
        if(playerAreaResources.size() == 0)
        {
            message += "There is nothing interesting here.";
        }
        else
        {
            for(auto it = playerAreaResources.cbegin(); it != playerAreaResources.cend(); ++it)
            {
                if(it == playerAreaResources.cbegin())
                    message += "You see ";

                message += std::to_string(it->second) + " ";
                auto resourceObject = world.getResource(it->first);

                if(it != --playerAreaResources.cend())
                    message += resourceObject.getName() + ", ";
                else
                    message += resourceObject.getName() + ".";
            }
        }

        tts_say(message);
        setMessage(message);
    }

    void GameScene::describePlayerInventory()
    {
        const auto& inventory = player.getInventory();
        if(inventory.cbegin() == inventory.cend())
        {
            setMessage("You have nothing in your inventory.");
            return;
        }

        std::string message("You have ");
        for(auto it = inventory.cbegin(); it != inventory.cend(); ++it)
        {
            message += std::to_string(it->first) + " ";

            auto& item = itemRegistry.at(it->second.getIdentifier());
            message += item.name;

            if(it != --inventory.cend())
                message += ", ";
            else
                message += ".";

        }

        setMessage(message);
    }

	// Actions
    void GameScene::actionCheck(const actions::GameAction& action)
    {
        if(action.getSubjectCount() != 1)
        {
            setMessage("Check what ?");
            return;
        }

        if(action.getSubject(0) == "inventory")
        {
            describePlayerInventory();
        }
    }
	void GameScene::actionGo(const actions::GameAction& action)
	{
        if(action.getSubjectCount() != 1)
        {
            setMessage("Go where ?");
        }
        else
        {
            world::Direction d = world::directionFromString(action.getSubject(0));
            if(d == world::Direction::UNKNOWN)
            {
                setMessage("Go where ?");
            }
            else
            {
                auto playerPos = player.getPosition();
                pem::vec2i newPos;
                newPos.x = playerPos.x + world::getRelativeX(d);
                newPos.y = playerPos.y + world::getRelativeY(d);

                auto worldSize = static_cast<int>(world.getSize());
                if(newPos.x < 0 || newPos.x >= worldSize ||
                   newPos.y < 0 || newPos.y >= worldSize)
                {
                    setMessage("Huh, it's the edge of the world :/");
                }
                else
                {
                    move(d);
                }
            }
        }
	}
    void GameScene::actionTake(const actions::GameAction& action)
    {
        if(action.getSubjectCount() != 1)
        {
            setMessage("Take what ?");
            return;
        }

        std::string subject = action.getSubject(0);
        if(world.getResource(subject).getGatherAction() != "take")
        {
            setMessage("You can't take that.");
        }
        else
        {
            // harvest();
        }
    }
}
}
