#include "ExpressionParser/Parser.hpp"

namespace game
{
namespace expressionparser
{
    Parser::Parser()
	{
	}
    Parser::~Parser()
	{
	}

    void Parser::addCallback(const std::string& action, std::function<void(const actions::GameAction&)> callback)
	{
        if(callbacks.count(action) == 0)
        {
            callbacks.insert(std::make_pair(action, callback));
        }
	}
    void Parser::analyze(const std::list<Token>& tokenList)
    {
        actions::GameAction action;

        for(auto& token : tokenList)
        {
            if(action.getAction() == "unknown" && token.getType() == WordType::ACTION)
            {
                action.setAction(token.getIdentifier());
            }
            else if(token.getType() == WordType::SUBJECT)
            {
                action.addSubject(token.getIdentifier());
            }
            else if(token.getType() == WordType::LINKER)
            {
                action.addLinker(token.getIdentifier());
            }
            else if(token.getType() == WordType::UNKNOWN)
            {
                action.setAction("unknown");
            }
        }

        if(callbacks.count(action.getAction()) != 0)
            callbacks[action.getAction()](action);
	}
}
}