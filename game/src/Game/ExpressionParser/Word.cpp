#include "ExpressionParser/Word.hpp"

namespace game
{
namespace expressionparser
{
    Word::Word()
        : identifier("unknown")
        , type(WordType::UNKNOWN)
	{
	}
    Word::~Word()
	{
	}

    void Word::setIdentifier(const std::string& identifier)
	{
        this->identifier = identifier;
	}
    const std::string& Word::getIdentifier() const
	{
        return identifier;
	}

    void Word::setType(WordType type)
	{
        this->type = type;
	}
    WordType Word::getType() const
	{
        return type;
	}

    void Word::addRegex(const std::regex& regex)
	{
        regexes.push_back(regex);
	}
    const std::vector<std::regex>& Word::getRegexes() const
	{
        return regexes;
	}
}
}
