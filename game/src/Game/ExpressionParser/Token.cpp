#include "ExpressionParser/Token.hpp"

namespace game
{
namespace expressionparser
{
    Token::Token()
        : identifier("unknown")
        , type(WordType::UNKNOWN)
	{
	}
    Token::~Token()
	{
	}

    void Token::setIdentifier(const std::string& identifier)
	{
        this->identifier = identifier;
	}
    const std::string& Token::getIdentifier() const
	{
        return identifier;
	}

    void Token::setType(WordType type)
	{
        this->type = type;
	}
    WordType Token::getType() const
	{
        return type;
	}
}
}