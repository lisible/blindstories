#include "ExpressionParser/Lexer.hpp"
#include "ExpressionParser/Token.hpp"
#include <algorithm>

namespace game
{
namespace expressionparser
{
    Lexer::Lexer()
	{
	}
    Lexer::~Lexer()
	{
	}

    Token Lexer::extractNextToken(std::string& text, const std::vector<Word>& words)
    {
        Token token;
        std::smatch matchResults;

        if(std::regex_search(text, matchResults, std::regex("(( |\\.|,|;|:|!|\\?))+")))
        {
            text.erase(matchResults[0].first, matchResults[0].second);
        }

        for(auto& word : words)
        {
            for(auto& regex : word.getRegexes())
            {

                if(std::regex_search(text, matchResults, regex))
                {
                    token.setType(word.getType());
                    token.setIdentifier(word.getIdentifier());
                    text.erase(matchResults[0].first, matchResults[0].second);
                    return token;
                }
            }
        }

        token.setType(WordType::UNKNOWN);
        token.setIdentifier(text);
        text.erase(text.begin(), text.end());
        return token;
    }

    std::list<Token> Lexer::analyze(const std::string& inputText, const std::vector<Word>& words)
	{
        std::list<Token> tokenList;
        std::string text(inputText);

        while(text.length() != 0)
        {
            tokenList.push_back(extractNextToken(text, words));
        }

        return tokenList;
	}
}
}