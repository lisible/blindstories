#include "ExpressionParser/ActionAnalyzer.hpp"

#include <cstdarg>


// TODO remove
#include <iostream>
namespace game
{
namespace expressionparser
{
    ActionAnalyzer::ActionAnalyzer()
	{
	}
    ActionAnalyzer::~ActionAnalyzer()
	{
	}

    void ActionAnalyzer::addWord(const std::string& wordIdentifier, WordType wordType, unsigned regexCount, ...)
	{
        Word word;
        word.setIdentifier(wordIdentifier);
        word.setType(wordType);

        std::va_list argumentList;
        va_start(argumentList, regexCount);

        for(unsigned i = 0; i < regexCount; i++)
        {
            std::regex regex(va_arg(argumentList, const char*), std::regex_constants::icase);
            word.addRegex(regex);
        }
        va_end(argumentList);

        tokens.push_back(word);
	}

    void ActionAnalyzer::addCallback(const std::string& actionIdentifier, std::function<void(const actions::GameAction&)> callback)
    {
        parser.addCallback(actionIdentifier, callback);
    }

    void ActionAnalyzer::analyze(const std::string& text)
    {
        auto tokenList = lexer.analyze(text, tokens);
        for(auto& token : tokenList)
        {
            std::cout << token.getIdentifier() << std::endl;
        }
        parser.analyze(tokenList);
    }
}
}