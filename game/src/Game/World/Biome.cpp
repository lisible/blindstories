#include "World/Biome.hpp"

namespace game
{
namespace world
{
    Biome::Biome(const Biome& biome)
        : identifier(biome.getIdentifier())
        , name(biome.getName())
	  	, description(biome.getDescription())
        , debugColor(biome.getDebugColor())
    {
    }
    Biome::Biome(const std::string& identifier)
        : identifier(identifier)
        , name("biome_name")
        , debugColor("000000")
	{
	}
    Biome::~Biome()
	{
	}

    const std::string& Biome::getIdentifier() const
    {
        return identifier;
    }

    void Biome::setName(const std::string& name)
    {
        this->name = name;
    }
    const std::string& Biome::getName() const
	{
        return name;
	}

    void Biome::setDescription(const std::string& description)
	{
        this->description = description;
	}
    const std::string& Biome::getDescription() const
	{
        return description;
	}


    void Biome::setDebugColor(const pe::graphics::Color& color)
	{
        debugColor = color;
	}
    const pe::graphics::Color& Biome::getDebugColor() const
	{
        return debugColor;
	}
}
}