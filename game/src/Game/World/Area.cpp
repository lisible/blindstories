#include "World/Area.hpp"

namespace game
{
namespace world
{
    Area::Area()
        : biomeIdentifier("default")
	{
	}
    Area::~Area()
	{
	}

    /**
     * Sets the biome of the area
     * @param biomeIdentifier The identifier of the biome of the area
     */
    void Area::setBiomeIdentifier(const std::string &biomeIdentifier)
	{
        this->biomeIdentifier = biomeIdentifier;
	}
    /**
     * Returns the identifier of the biome of the area
     * @return The identifier of the biome of the area
     */
    const std::string& Area::getBiomeIdentifier() const
	{
        return biomeIdentifier;
	}

    void Area::addResource(const std::string& resourceIdentifier, unsigned quantity)
    {
        resources.insert(std::make_pair(resourceIdentifier, quantity));
    }
    const std::map<std::string, unsigned>& Area::getResources() const
    {
        return resources;
    }

    void Area::setHeight(double height)
    {
        this->height = height;
    }
    double Area::getHeight() const
    {
        return height;
    }
}
}