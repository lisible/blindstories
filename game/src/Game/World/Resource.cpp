#include "World/Resource.hpp"

namespace game
{
namespace world
{
    Resource::Resource(const std::string& resourceIdentifier)
        : identifier(resourceIdentifier)
        , name("unknown resource")
        , description("")
        , gatherAction("pick")
	{
	}
    Resource::Resource(const Resource& resource)
        : identifier(resource.identifier)
        , name(resource.name)
        , description(resource.description)
        , gatherAction(resource.gatherAction)
    {
    }
    Resource::~Resource()
	{
	}


    const std::string& Resource::getIdentifier() const
    {
        return identifier;
    }

    void Resource::setName(const std::string& name)
	{
        this->name = name;
	}
    const std::string& Resource::getName() const
	{
        return name;
	}

    void Resource::setDescription(const std::string& description)
	{
        this->description = description;
	}
    const std::string& Resource::getDescription()
	{
        return description;
	}

    void Resource::addLoot(const LootData& loot)
	{
        loots.push_back(loot);
	}
    const std::list<LootData>& Resource::getLoots() const
	{
        return loots;
	}


    void Resource::setGatherAction(const std::string& action)
    {
        gatherAction = action;
    }
    const std::string& Resource::getGatherAction() const
    {
        return gatherAction;
    }
}
}