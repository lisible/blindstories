#include "World/World.hpp"

namespace game
{
namespace world
{
    World::World()
	{
	}
    World::~World()
	{
	}

    void World::setSize(unsigned size)
	{
        areas.resize(size, std::vector<Area>(size));
	}
    unsigned World::getSize() const
	{
        return areas.size();
	}

    void World::registerBiome(const Biome& biome)
    {
        biomeRegistry.insert(std::make_pair(biome.getIdentifier(), biome));
    }

    const Biome& World::getBiome(const std::string& biomeIdentifier) const
    {
        if(biomeRegistry.count(biomeIdentifier) == 0 || biomeIdentifier == "default")
            return biomeRegistry.cbegin()->second;

        return biomeRegistry.at(biomeIdentifier);
    }

    void World::registerResource(const Resource& resource)
    {
        resourceRegistry.insert(std::make_pair(resource.getIdentifier(), resource));
    }
    const Resource& World::getResource(const std::string& resourceIdentifier)
    {
        if(resourceRegistry.count(resourceIdentifier) == 0 || resourceIdentifier == "default")
            return resourceRegistry.cbegin()->second;

        return resourceRegistry.at(resourceIdentifier);
    }

    Area& World::getArea(int x, int y)
	{
        return areas.at(y).at(x);
	}
    Area& World::getArea(const pem::vec2i& position)
    {
        return getArea(position.x, position.y);
    }
}
}