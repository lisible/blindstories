#include "World/WorldGenerator.hpp"
#include <PotatoEngine/Utils/Maths.hpp>
#include <cstdlib>

namespace game
{
namespace world
{
    WorldGenerator::WorldGenerator()
	{
	}
    WorldGenerator::~WorldGenerator()
	{
	}

    void WorldGenerator::seed(unsigned s)
	{
        heightNoise.reseed(s);
	}

    void WorldGenerator::generate(World& world)
	{
        generateBiomes(world);
        generateResources(world);
	}

    void WorldGenerator::generateBiomes(World& world)
    {
        for(size_t y = 0; y < world.getSize(); y++)
        {
            for(size_t x = 0; x < world.getSize(); x++)
            {
                // Generating the height of the terrain based on a perlin noise
                double dheight = heightNoise.octaveNoise(static_cast<double>(x/10.f), static_cast<double>(y/10.f), 256);
                double height = pe::utils::map(dheight, -1, 1, 0.0, 100.0);

                world.getArea(x, y).setHeight(height);
                for(auto& biomeGenerationData : biomeGenerationDataList)
                {
                    if(biomeGenerationData.minHeight <= height && biomeGenerationData.maxHeight > height)
                    {
                        world.getArea(x, y).setBiomeIdentifier(biomeGenerationData.biomeIdentifier);
                        break;
                    }
                }
            }
        }
    }
    void WorldGenerator::generateResources(World& world)
    {
        for(size_t y = 0; y < world.getSize(); y++)
        {
            for(size_t x = 0; x < world.getSize(); x++)
            {
                auto& area = world.getArea(x, y);
                auto& biome = world.getBiome(area.getBiomeIdentifier());
                for(auto& biomeGenerationData : biomeGenerationDataList)
                {
                    if(biomeGenerationData.biomeIdentifier == biome.getIdentifier())
                    {
                        for(auto& resourceGenerationData : biomeGenerationData.resourceGenerationDataList)
                        {
                            // TODO remove rand
                            auto quantity = resourceGenerationData.minimumQuantity + (std::rand() % static_cast<int>(resourceGenerationData.maximumQuantity - resourceGenerationData.minimumQuantity + 1));
                            if(quantity > 0)
                                area.addResource(resourceGenerationData.resourceIdentifier, quantity);
                        }
                        break;
                    }
                }
            }
        }
    }


    void WorldGenerator::addBiomeGenerationData(const BiomeGenerationData& biomeGenerationData)
    {
        biomeGenerationDataList.push_back(biomeGenerationData);
    }
}
}