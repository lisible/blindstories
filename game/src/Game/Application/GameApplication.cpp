#include "Application/GameApplication.hpp"
#include "Scenes/SceneFactory.hpp"
#include <PotatoEngine/Logging/LoggingSystem.hpp>
#include <PotatoEngine/Logging/ConsoleLogger.hpp>
#include <PotatoEngine/Windowing/SDLWindowingSystem.hpp>
#include <PotatoEngine/Messaging/Messaging.hpp>
#include <PotatoEngine/Input/InputSystem.hpp>
#include <PotatoEngine/Input/KeyMap.hpp>
#include <PotatoEngine/Audio/TextToSpeechSystem.hpp>
#include <PotatoEngine/Audio/FestivalTextToSpeechSystem.hpp>
#include <PotatoEngine/Logic/LogicSystem.hpp>
#include <PotatoEngine/Resources/ResourceSystem.hpp>
#include <PotatoEngine/Graphics/GraphicSystem.hpp>
#include <PotatoEngine/UI/GUISystem.hpp>

namespace game
{
namespace app
{
	GameApplication::GameApplication()
		: Application("BlindStories")
	{
	}

	GameApplication::~GameApplication()
	{
	}

	PEvoid GameApplication::init()
	{
		pe::app::Application::init();
		
		// Creating and registering the logging system
		auto loggingSystem = new pe::logging::LoggingSystem();
		loggingSystem->addLogger(new pe::logging::ConsoleLogger());
		
		registerSystem(loggingSystem);	

		// Creating and registering the windowing system
		auto windowingSystem = new pe::windowing::SDLWindowingSystem();
		windowingSystem->init();

		registerSystem(windowingSystem);

		// Creating and registering the input system
		pe::input::KeyMap keyMap;
		keyMap.addKey(pe::input::Keyboard::KEY_ESCAPE, "exit");
		keyMap.addKey(pe::input::Keyboard::KEY_ENTER, "post");
		keyMap.addKey(pe::input::Keyboard::KEY_UP, "previous");
		keyMap.addKey(pe::input::Keyboard::KEY_LEFT, "previous");
		keyMap.addKey(pe::input::Keyboard::KEY_DOWN, "next");
		keyMap.addKey(pe::input::Keyboard::KEY_RIGHT, "next");
		keyMap.addKey(pe::input::Keyboard::KEY_TAB, "ttsRepeat");
		keyMap.addKey(pe::input::Keyboard::KEY_BACKSPACE, "backspace");

		auto inputSystem = new pe::input::InputSystem();
		inputSystem->setKeyMap(keyMap);

		registerSystem(inputSystem);
		
		// Creating and registering the TTS system
#ifdef __linux__
		auto ttsSystem = new pe::audio::FestivalTextToSpeechSystem();
		registerSystem(ttsSystem);
#endif


		// Requesting window creation
		auto data = new pemsg::SimpleMessageData();
		data->stringValue1 = "game";
		data->integerValue1 = 800;
		data->integerValue2 = 600;	
		sendMessage(pemsg::Message(pemsg::MSG_WINDOW_WINDOWCREATION_REQUEST,
											data, 
											[=](PEvoid*){delete data;}));

		sendMessage(pemsg::Message(pemsg::MSG_WINDOW_WINDOWSHOW_REQUEST));

		auto resourceSystem = new pe::resources::ResourceSystem();
		resourceSystem->setUIComponentFactory(new pe::ui::GUIComponentFactory());
		registerSystem(resourceSystem);

		auto graphicSystem = new pe::graphics::GraphicSystem();
		pe::graphics::OrthographicCamera camera(0,0,800,600,-1,1);
		graphicSystem->setCamera(camera);
		registerSystem(graphicSystem);
		sendMessage(pemsg::Message(pemsg::MSG_GRAPHICS_INITIALIZE));

		// Creating and registering the logic system
		auto logicSystem = new pe::logic::LogicSystem();
		registerSystem(logicSystem);

		logicSystem->setSceneFactory(new game::scenes::SceneFactory());

		// Pushing the initial scene
		logicSystem->pushScene("MainMenuScene");

		// Creating and registering the ui system
		auto uiSystem = new pe::ui::GUISystem();
		registerSystem(uiSystem);


		/*sendMessage(pemsg::Message(pemsg::MSG_LOGIC_PUSHSCENE_REQUEST,
									new game::scenes::SplashScreenScene()));*/
	}
}
}
