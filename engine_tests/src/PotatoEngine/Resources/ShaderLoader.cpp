#include "main.hpp"
#include <PotatoEngine/Resources/ShaderLoader.hpp>
#include <PotatoEngine/Resources/Resource.hpp>
#include <PotatoEngine/Graphics/Shader.hpp>
#include <PotatoEngine/Windowing/SDLWindow.hpp>
TEST_CASE("Shaders can be loaded using the shader loader", "[pe::resources::ShaderLoader]")
{
	pe::windowing::SDLWindow window;
	window.create("test", 800, 600);


	pe::resources::Resource<pe::graphics::Shader> resource = pe::resources::ShaderLoader::loadShaderResource("testShader");
}
