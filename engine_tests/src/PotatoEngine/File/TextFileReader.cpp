#include "main.hpp"

#include <PotatoEngine/File/File.hpp>
#include <PotatoEngine/File/TextFileReader.hpp>

TEST_CASE("Text files can be read", "[pe::file::TextFileReader]")
{
	pe::file::File f("./test.txt");	
	pe::file::TextFileReader reader;

	SECTION("Reading some chars")
	{
		reader.openFile(f);
		std::string string(reader.readString(7));
		reader.closeFile();
		
		REQUIRE(string == "Bonjour");
	}
}

TEST_CASE("Text files can be read line by line", "[pe::file::TextFileReader]")
{
	pe::file::File f("./test2.txt");
	pe::file::TextFileReader reader;

	SECTION("Reading a line")
	{
		reader.openFile(f);
		std::string line(reader.readLine());	
		reader.closeFile();

		REQUIRE(line == "Bonjour,");
	}
}

TEST_CASE("Text files can be read entirely", "[pe::file::TextFileReader]")
{
	pe::file::File f("./test2.txt");
	pe::file::TextFileReader reader;

	SECTION("Reading an entire file")
	{
		reader.openFile(f);
		std::string file(reader.readFile());
		reader.closeFile();

		REQUIRE(file == "Bonjour,\nje m'appelle Clément");

	}
}
