#include "main.hpp"
#include <PotatoEngine/File/File.hpp>

TEST_CASE("File objects can be created using a path", "[pe::file::File]")
{
	pe::file::File file("./test.txt");


	SECTION("The path can be fetched from the object")
	{
		REQUIRE(file.getPath() == "./test.txt");
	}	
}
