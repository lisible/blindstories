#include "main.hpp"
#include <PotatoEngine/File/File.hpp>
#include <PotatoEngine/File/TextFileWriter.hpp>
#include <PotatoEngine/File/TextFileReader.hpp>

TEST_CASE("Text files can be written", "[pe::File::TextFileWriter]")
{
	pe::file::TextFileWriter writer;	

	SECTION("Writing a some text into a file")
	{
		pe::file::File f("./test.txt");
		writer.openFile(f);
		writer.write("Bonjour, je m'appelle Clément");
		writer.closeFile();
	}

	SECTION("Writing a line into a file")
	{
		pe::file::File f("./test2.txt");
		writer.openFile(f);
		writer.writeLine("Bonjour,");
		writer.write("je m'appelle Clément");
		writer.closeFile();
	}

}
