#define CATCH_CONFIG_RUNNER
#include "main.hpp"

int main(int argc, char* const argv[])
{

	int result = Catch::Session().run(argc, argv);

#ifdef _WIN32
	system("pause");
#endif

	return result;
}