#ifndef PE_RESOURCES_RESOURCEDESCRIPTION_HPP
#define PE_RESOURCES_RESOURCEDESCRIPTION_HPP

#include "types.hpp"
#include "Resources/ResourceType.hpp"

#include <string>
#include <unordered_map>

namespace pe
{
namespace resources
{
	/**
 	 * Describes a resource
 	 */
	class ResourceDescription
	{
		private:
			// The name of the resource
			std::string resourceName;
			// The type of the resource
			ResourceType resourceType;

			// The parameters of the resource
			std::unordered_map<std::string, std::string> resourceParameters;
		public:
			ResourceDescription();
			~ResourceDescription();

			/**
 			 * Sets the name of the resource
 			 * @param name The name of the resource
 			 */
			PEvoid setResourceName(const std::string& name);
			/**
 			 * Returns the name of the resource
 			 * @retun The name of the resource
 			 */
			const std::string& getResourceName() const;

			/**
 			 * Sets the type of the resource
 			 * @param type The type of the resource
 			 */
			PEvoid setResourceType(ResourceType type);
			/**
 			 * Returns the type of the resource
 			 * @return The type of the resource
 			 */
			ResourceType getResourceType() const;

			/**
 			 * Adds a resource parameter
 			 * @param name The name of the resource
 			 * @param value The value of the paramter
 			 */
			PEvoid addResourceParameter(const std::string& name, 
										const std::string& value);
			/**
 			 * Returns the value of the parameter with the given name
 			 * @param parameter The name of the resource parameter
 			 * @return The corresponding value
 			 */
			const std::string& getResourceParameter(const std::string& parameter) const;
			/**
 			 * Returns true if the resource has the given parameter
 			 * @param parameter The name of the parameter 
 			 * @return True if the resource has the given parameter
 			 */
			PEbool hasResourceParameter(const std::string& parameter) const;
			/**
 			 * Returns true if the resource has the parameter 
 			 * and the parameter is "true"
 			 * @param parameter The parameter
 			 * @return true if the resource has the parameter and the parameter is "true"
 			 */
			PEbool isResourceParameterTrue(const std::string& parameter) const;


			/**
 			 * Returns the resource description for the corresponding resource
 			 * @param resourceName The name of the resource
 			 * @param resourceType The type of the resource
 			 */
			static ResourceDescription getResourceDescription(const std::string& resourceName,
															  ResourceType type);


			};
}
}

#endif
