#ifndef PE_RESOURCES_RESOURCE_HPP
#define PE_RESOURCES_RESOURCE_HPP

#include "types.hpp"

namespace pe
{
namespace resources
{
	/**
 	 * Template for resource holders
 	 */
	template <typename T>
	class Resource
	{
		private:
			T* resource;
		public:
			Resource(){};
			Resource(T* resource)
				: resource(resource)
			{
			}

			~Resource()
			{
			}

			T& getResource() 
			{
				return *resource;
			}

			PEvoid dispose()
			{
				delete resource;
			}
	};
}
}

#endif
