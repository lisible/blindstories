#ifndef PE_RESOURCES_RESOURCETYPE_HPP
#define PE_RESOURCES_RESOURCETYPE_HPP

#include "types.hpp"
#include <string>

namespace pe
{
namespace resources
{
	/**
 	 * Describes the types of resources
 	 */
	enum ResourceType
	{
		UNKNOWN,
		SHADER,
		TEXTURE
	};

	inline ResourceType resourceTypeFromString(const std::string& str)
	{
		if(str == "SHADER")
			return SHADER;
		if(str == "TEXTURE")
			return TEXTURE;
		return UNKNOWN;
	}
}
}

#endif
