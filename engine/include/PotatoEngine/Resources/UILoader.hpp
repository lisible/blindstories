#ifndef PE_RESOURCES_UILOADER_HPP
#define PE_RESOURCES_UILOADER_HPP

#include "types.hpp"
#include "UI/UserInterface.hpp"
#include "UI/GUIComponentFactory.hpp"
#include "Resources/Resource.hpp"

#include <tinyxml2.h>

namespace pe
{
namespace resources
{
    class ResourceSystem;
    /**
     * Used to load UIs
     */
    class UILoader
    {
    private:
        // The resource system using the loader
        ResourceSystem& resourceSystem;
        // The component factory to use
        pe::ui::GUIComponentFactory* componentFactory;

        PEvoid parseUIComponent(pe::ui::UserInterface* ui, pe::ui::GUIComponent* parent, tinyxml2::XMLNode* uiNode);

        /**
         * Parses the UI tree
         * @param uiRoot The root of the ui
         * @return The parsed UserInterface
         */
        pe::ui::UserInterface* parseUITree(tinyxml2::XMLNode* uiRoot);

        /**
          * Loads a user interface
          * @param name The name of the ui to load
          * @return A new UserInterface
          */
        pe::ui::UserInterface* loadUI(const std::string& name);
    public:
        UILoader(ResourceSystem& resourceSystem);
        ~UILoader();

        PEvoid setComponentFactory(pe::ui::GUIComponentFactory* factory);

        /**
          * Loads a ui as a resource
          * @param name The name of the resource to load
          */
        Resource<pe::ui::UserInterface> loadUIResource(const std::string& name);
    };
}
}

#endif
