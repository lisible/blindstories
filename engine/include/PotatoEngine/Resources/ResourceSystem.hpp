#ifndef PE_RESOURCES_RESOURCESYSTEM_HPP
#define PE_RESOURCES_RESOURCESYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"
#include "Resources/ResourceCache.hpp"
#include "Resources/ResourceIndex.hpp"

#include "TextureLoader.hpp"
#include "ShaderLoader.hpp"
#include "FontLoader.hpp"
#include "UILoader.hpp"

#include "Graphics/Shader.hpp"
#include "Graphics/Texture.hpp"
#include "Graphics/Font.hpp"
#include "UI/UserInterface.hpp"

namespace pe
{
namespace resources
{
	class ResourceSystem : public pe::system::System
	{
		private:
			// Resource index
			ResourceIndex resourceIndex;	

			// Loaders
			TextureLoader textureLoader;
			ShaderLoader shaderLoader;
			FontLoader fontLoader;
			UILoader uiLoader;

			// Shader cache
			ResourceCache<pe::graphics::Shader> shaderCache;
			ResourceCache<pe::graphics::Texture> textureCache;
			ResourceCache<pe::graphics::Font> fontCache;
			ResourceCache<pe::ui::UserInterface> uiCache;

			PEvoid loadResourceIndex();
			PEvoid loadShader(const std::string& name);
			PEvoid loadTexture(const std::string& name);
			PEvoid loadFont(const std::string& name);
			PEvoid loadUI(const std::string& name);
		protected:
			/**
 			 * Registers the default callback methods for events
 			 */
			virtual PEvoid registerDefaultHandlers() override;

		public:
			ResourceSystem();
			~ResourceSystem() override;

            PEvoid setUIComponentFactory(pe::ui::GUIComponentFactory* componentFactory);

			const std::string& getResourceFilePath(const std::string& identifier) const;

		private:
			// Callbacks
			/**
 			 * Loads a shader
 			 * @param data The name of the shader as a string pointer
 			 */
			PEvoid onLoadShaderRequest(PEvoid* data);
			/**
 			 * Returns a reference to a shader
 			 * @param data The name of the shader as a string pointer
 			 * @return A pointer to the shader
 			 */
			PEvoid* onGetShaderDRequest(PEvoid* data);

			/**
 			 * Loads a texture
 			 * @param data The name of the texture as a string pointer
 			 */
			PEvoid onLoadTextureRequest(PEvoid* data);
			/**
 			 * Returns a reference to a texture
 			 * @param data The name of the texture as a string pointer
 			 * @return A pointer to the texture
 			 */
			PEvoid* onGetTextureDRequest(PEvoid* data);
				
			/**
 			 * Loads a font
 			 * @param data The name of the font as a string pointer
 			 */
			PEvoid onLoadFontRequest(PEvoid* data);
			/**
 			 * Returns a reference to a font
 			 * @param data The name of the font as a string pointer
 			 * @return A pointer to the font
 			 */
			PEvoid* onGetFontDRequest(PEvoid* data);

			/**
			 * Loads a UI
			 * @param data The name of the font as a string pointer
			 */
			PEvoid onLoadUIRequest(PEvoid* data);
			/**
			* Returns a reference to a UI
			* @param data The name of the UI as a string pointer
			* @return A pointer to the UI
			*/
			PEvoid* onGetUIDRequest(PEvoid* data);
					

	};
}
}

#endif
