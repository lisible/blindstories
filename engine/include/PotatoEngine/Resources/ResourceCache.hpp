#ifndef PE_RESOURCES_RESOURCECACHE_HPP
#define PE_RESOURCES_RESOURCECACHE_HPP

#include "types.hpp"
#include "Resources/Resource.hpp"
#include <unordered_map>

namespace pe
{
namespace resources
{
	template <typename T>
	class ResourceCache
	{
		private:
			std::unordered_map<std::string, Resource<T>> resources;

		public:
			ResourceCache(){}
			~ResourceCache(){}

			/**
 			 * Adds a resource into the cache
 			 * Note: A resource with an existing name will replace the old one
 			 * @param name The name of the resource
 			 * @param resource The resource
 			 */
			PEvoid addResource(const std::string& name, Resource<T> resource)
			{
				if(resources.count(name) == 1)
				{
					return;
				}

				resources.insert(std::make_pair(name, resource));
			}

			PEvoid disposeResource(const std::string& name)
			{
				resources.at(name).dispose();
				resources.erase(resources.find(name));
			}

			PEvoid clearResources()
			{
				for(auto& r : resources)
				{
					r.second.dispose();
				}

				resources.clear();
			}

			/**
 			 * Returns the resource with the given name
 			 * @param name The name of the resource
 			 */
			T& getResource(const std::string& name) 
			{
				return resources.at(name).getResource();
			}

			/**
 			 * Returns true if the cache contains a resource with the given name
 			 * @param name The name of the resource to look for
 			 * @return true if the cache contains a resource with the given name
 			 */
			PEbool hasResource(const std::string& name)
			{
				return resources.find(name) != resources.end();
			}
	};
}
}

#endif
