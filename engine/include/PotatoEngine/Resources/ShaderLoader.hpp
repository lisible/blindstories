#ifndef PE_RESOURCES_SHADERLOADER_HPP
#define PE_RESOURCES_SHADERLOADER_HPP

#include "types.hpp"
#include "Resources/Resource.hpp"
#include "Graphics/Shader.hpp"
#include "Graphics/ShaderType.hpp"

#include <string>


namespace pe
{
namespace resources
{
	class ResourceSystem;

	/**
 	 * Used to load shaders
 	 */
	class ShaderLoader
	{
		private:
			// The resource system using the loader
			ResourceSystem& resourceSystem;

			/**
 			 * Loads a shader 
 			 * @param name The name of the shader to load
 			 * @return A new shader
 			 */
			pe::graphics::Shader* loadShader(const std::string& name);
			
			/**
 			 * Compiles a shader
 			 * @param name The name of the shader
 			 * @param type The type of the shader
 			 * @return The compiled shader handle
 			 */
			GLuint compileShader(const std::string& name,
								 pe::graphics::ShaderType type);
			/**
 			 * Returns the shader source
 			 * @param name The name of the shader
 			 * @param type The type of the shader
 			 */
			std::string getShaderSource(const std::string& name,
										pe::graphics::ShaderType type);
		public:
			ShaderLoader(ResourceSystem& resourceSystem);
			~ShaderLoader();


			
			/**
 			 * Loads a shader as a resource
 			 * @param name The name of the shader to load
 			 */
			Resource<pe::graphics::Shader> loadShaderResource(const std::string& name);
	};
}
}

#endif
