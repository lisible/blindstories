#ifndef PE_RESOURCES_FONTLOADER_HPP
#define PE_RESOURCES_FONTLOADER_HPP

#include "types.hpp"
#include "Graphics/Font.hpp"
#include "Resources/Resource.hpp"

#include "Resources/TextureLoader.hpp"

namespace pe
{
namespace resources
{
	class ResourceSystem;

	/**
 	 * Used to load fonts
 	 */
	class FontLoader
	{
		private:
			// The resource system using the loader
			ResourceSystem& resourceSystem;
			// The texture loader used to load the font texture
			TextureLoader& textureLoader;
			

			/**
 			 * Loads a font
 			 * @param name The name of the font to load
 			 * @return A new font
 			 */
			pe::graphics::Font* loadFont(const std::string& name);
		public:
			FontLoader(ResourceSystem& resourceSystem, TextureLoader& textureLoader);
			~FontLoader();

			/**
 			 * Loads a font as a resource
 			 * @param name The name of the resource to load
 			 */
			Resource<pe::graphics::Font> loadFontResource(const std::string& name);
	};
}
}

#endif
