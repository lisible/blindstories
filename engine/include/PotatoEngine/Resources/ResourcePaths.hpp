#ifndef PE_RESOURCES_RESOURCEPATHS_HPP
#define PE_RESOURCES_RESOURCEPATHS_HPP

#include "types.hpp"
#include <string>

//define RESOURCE_DIR "data/"
//define SHADER_DIR "shaders/"
//define TEXTURE_DIR "textures/"

namespace pe
{
namespace resources
{
	static const std::string RESOURCE_DIR("data/");
	
	static const std::string SHADER_DIR("shaders/");
	static const std::string TEXTURE_DIR("textures/");
}
}

#endif
