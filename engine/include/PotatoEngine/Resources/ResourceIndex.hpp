#ifndef PE_RESOURCES_RESOURCEINDEX_HPP
#define PE_RESOURCES_RESOURCEINDEX_HPP

#include "types.hpp"

#include <unordered_map>

namespace pe
{
namespace resources
{
	/**
 	 * Holds file paths of indexed resources
 	 */
	class ResourceIndex
	{
		private:
			// The resource files
			std::unordered_map<std::string, std::string> resourceFilePaths;

		public:
			ResourceIndex();
			~ResourceIndex();

			/**
 			 * Adds a resource file to the index
 			 * @param resourceIdentifier The identifier of the resource
 			 * @param filePath The file path to add
 			 */
			PEvoid addResourceFile(const std::string& resourceIdentifier,
								   const std::string& file);
			/**
 			 * Returns the resource file path for the given resource identifier
 			 * @param resourceIdentifier The identifier of the resource
 			 * @return The corresponding resource file path
 			 */
			const std::string& getResourceFilePath(const std::string& resourceIdentifier) const;
	};
}
}

#endif
