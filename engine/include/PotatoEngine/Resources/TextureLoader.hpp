#ifndef PE_RESOURCES_TEXTURELOADER_HPP
#define PE_RESOURCES_TEXTURELOADER_HPP

#include "types.hpp"
#include "Resources/Resource.hpp"
#include "Graphics/Texture.hpp"

#include <string>

namespace pe
{
namespace resources
{
	class ResourceSystem;

	/**
 	 * Used to load textures
 	 */
	class TextureLoader
	{
		private:
			// The resource system using the loader
			ResourceSystem& resourceSystem;

			/**
 			 * Loads a texture
 			 * @param name The name of the texture to load
 			 * @return A new texture
 			 */
			pe::graphics::Texture* loadTexture(const std::string& name);

		public:
			TextureLoader(ResourceSystem& resourceSystem);
			~TextureLoader();

			/**
 			 * Loads a texture as a resource
 			 * @param name The name of the resource to laod
 			 */
			Resource<pe::graphics::Texture> loadTextureResource(const std::string& name);

			/**
 			 * Loads a texture from a path
 			 * @param path The path of the texture to load
 			 */
			pe::graphics::Texture* loadTextureFromPath(const std::string& path);
	};
}
}

#endif
