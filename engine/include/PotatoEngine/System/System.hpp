#ifndef PE_SYSTEM_SYSTEM_HPP
#define PE_SYSTEM_SYSTEM_HPP

#include "types.hpp"
#include "Messaging/Messenger.hpp"

namespace pe
{
namespace system
{
	/**
 	 * Base class for systems
 	 */
	class System : public pe::messaging::Messenger
	{
		protected:
			/**
 			 * Registers the default callback methods for events
 			 */	
			virtual PEvoid registerDefaultHandlers() = 0;

		public:
			System();
			virtual ~System();

			
	};
}
}

#endif
