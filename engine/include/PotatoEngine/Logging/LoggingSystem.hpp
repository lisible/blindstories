#ifndef PE_LOGGING_LOGGINGSYSTEM_HPP
#define PE_LOGGING_LOGGINGSYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"

namespace pe
{
namespace logging
{
	class ILogger;

	/**
 	 * System used to log messages
 	 */
	class LoggingSystem : public pe::system::System
	{
		private:
			// The loggers of the system
			std::list<ILogger*> loggers;

		protected:
			/**
 			 * Registers the default callback methods
 			 */
			virtual PEvoid registerDefaultHandlers() override;

		public:
			LoggingSystem();
			~LoggingSystem() override;
			
			/**
 			 * Adds a logger to the logging system
 			 */
			PEvoid addLogger(ILogger* logger);
			
			/**
 			 * Sends a string to log to the loggers
 			 * @param logString The string to log
 			 */
			PEvoid log(const std::string& logString);
		
		private:	
			/**
 			 * Message handlers
 			 */
			PEvoid onLogRequest(PEvoid* data);
			PEvoid onApplicationStartup(PEvoid* data);
			PEvoid onWindowCreated(PEvoid* data);
			PEvoid onWindowShown(PEvoid* data);
			PEvoid onWindowHidden(PEvoid* data);
			PEvoid onInputReceived(PEvoid* data);
			PEvoid onActionPressed(PEvoid* data);
			PEvoid onActionReleased(PEvoid* data);
			PEvoid onTTSPronouncedRequest(PEvoid* data);
	};
}
}

			
#endif

