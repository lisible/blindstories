#ifndef PE_LOGGING_ILOGGER_HPP
#define PE_LOGGING_ILOGGER_HPP

#include "types.hpp"

#include <string>

namespace pe
{
namespace logging
{
	/**
 	 * Interface for loggers
 	 */
	class ILogger
	{
		public:
			virtual ~ILogger() {}
			
			/**
 			 * Logs a string
 			 * @param logString The string to log
 			 */
			virtual PEvoid log(const std::string& logString) = 0;
	};
}
}

#endif
