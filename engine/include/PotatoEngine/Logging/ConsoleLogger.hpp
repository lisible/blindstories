#ifndef PE_LOGGING_CONSOLELOGGER_HPP
#define PE_LOGGING_CONSOLELOGGER_HPP

#include "types.hpp"
#include "Logging/ILogger.hpp"

namespace pe
{
namespace logging
{
	/**
 	 * ILogger implementation for console output
 	 */
	class ConsoleLogger : public ILogger
	{
		public:
			ConsoleLogger();
			~ConsoleLogger() override;
			
			/**
 			 * Logs a string
 			 * @param logString The string to log
 			 */
			PEvoid log(const std::string& logString) override;
	};
}
}

#endif

