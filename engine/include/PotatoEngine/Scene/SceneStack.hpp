#ifndef PE_SCENE_SCENESTACK_HPP
#define PE_SCENE_SCENESTACK_HPP

#include "types.hpp"
#include "PotatoEngine/Scene/Scene.hpp"

#include <stack>

namespace pe
{
namespace scene
{
	/**
 	 * Class used to manipulate the scene stack
 	 */
	class SceneStack
	{
		private:
			// The scene stack
			std::stack<Scene*> stack;
		public:
			SceneStack();
			~SceneStack();
			
			/**
 			 * Pushes a scene on the scene stack
 			 * @param scene The scene to push
 			 */
			PEvoid pushScene(Scene* scene);
			/**
 			 * Pops a scene of the stack
 			 */
			PEvoid popScene();
			/**
 			 * Pops a scene and pushes a new one
 			 * @param scene The new scene
 			 */
			PEvoid changeScene(Scene* scene);

			/**
 			 * Returns a pointer to the top scene
 			 * @return A pointer to the top scene
 			 */
			Scene* currentScene() const;

			/**
 			 * Returns true if the stack is empty
 			 * @return True if the stack is empty
 			 */
			PEbool isEmpty() const;
	};
}
}

#endif
