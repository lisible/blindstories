#ifndef PE_SCENE_SCENE_HPP
#define PE_SCENE_SCENE_HPP

#include "types.hpp"
#include "UI/UserInterface.hpp"
#include "UI/GUIComponent.hpp"

#include <tinyxml2.h>
#include <functional>

namespace pe
{
namespace logic
{
	class LogicSystem;
}
namespace scene
{
	/**
 	 * Base class for scenes
 	 */
	class Scene
	{
		private:
			// The logic system using the scene
			pe::logic::LogicSystem* logicSystem;

		protected:
			/**
			 * Pops the scene
			 */
			PEvoid pop_scene();
			/**
			 * Pushes a scene on top of the current one
			 * @param sceneIdentifier The identifier of the scene to push
			 */
			PEvoid push_scene(const std::string& sceneIdentifier);
			/**
			 * Switches the scene
			 * @param sceneIdentifier The identifier of the new scene
			 */
			PEvoid change_scene(const std::string& sceneIdentifier);

            /**
             * Logs a message
             * @param message The message to log
             */
            PEvoid log_message(const std::string& message);


			/**
			 * Adds a component to the ui
			 * @param parentIdentifier The identifier of the parent ui component
			 * @param component The ui component to add
			 */
			PEvoid ui_add_component(const std::string& parentIdentifier, ui::GUIComponent* component);

			/**
			 * Changes an attribute of a component of the ui
			 * @param componentIdentifier The identifier of the component
			 * @param attribute The attribute to change
			 * @param value The new value of the attribute
			 */
			PEvoid ui_edit_attribute(const std::string& componentIdentifier,
									 const std::string& attribute,
									 const std::string& value);

			/**
			 * Asks the UI to focus the next component
			 */
			PEvoid ui_focus_next();
			/**
			 * Asks the UI to focus the previous component
			 */
			PEvoid ui_focus_previous();
			/**
			 * Posts/Activates the focused component
			 */
			PEvoid ui_post();

			/**
			 * Sends a text to speech request
			 * @param text The text to pronounce
			 */
			PEvoid tts_say(const std::string& text);

			/**
			 * Returns a data file
			 * @param file The file to get
			 * @return The data file
			 */
			tinyxml2::XMLDocument* open_data_file(const std::string& file);
			/**
			 * Closes a data file
			 * @param file The file
			 */
			PEvoid close_data_file(tinyxml2::XMLDocument *file);

			/**
			 * Registers a click callback for a GUI component
			 * @param componentIdentifier The identifier of the component
			 * @param callback The callback to run on click
			 */
			PEvoid register_click_callback(const std::string& componentIdentifier,
										   std::function<PEvoid(PEvoid)> callback);
			/**
			 * Registers a post callback for a GUI component
			 * @param componentIdentifier The identifier of the component
			 * @param callback The callback to run on post
			 */
			PEvoid register_post_callback(const std::string& componentIdentifier,
										  std::function<PEvoid(PEvoid)> callback);
		public:
			Scene();
			virtual ~Scene();

			/**
			 * Returns the identifier of the ui of the scene
			 * @return The identifier of the ui of the scene
			 */
			virtual std::string getUIIdentifier() const = 0;

			/**
			 * Sets the logic system of the scene
			 * @param logicSystem The logic system of the scene
			 */
			PEvoid setLogicSystem(pe::logic::LogicSystem* logicSystem);

			/**
 			 * On initialization
 			 */
			virtual PEvoid onInitialization() = 0;

			/**
 			 * Run on each tick
 			 */
			virtual PEvoid onTick() = 0;

			/**
			 * Handles the action pressed
			 * @param action The action
			 */
			virtual PEvoid onActionPressed(const std::string& action);
			/**
			 * Handles the typed text
			 * @param text The typed text
			 */
			virtual PEvoid onTextTyped(const std::string& text);
	};
}
}

#endif
