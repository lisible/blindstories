#ifndef PE_SCENE_ISCENEFACTORY_HPP
#define PE_SCENE_ISCENEFACTORY_HPP

#include "types.hpp"
#include "Scene/Scene.hpp"

namespace pe
{
namespace scene
{
    /**
     * Interface for scene factories
     */
    class ISceneFactory
    {
    public:
        virtual ~ISceneFactory(){}

        /**
         * Creates a scene corresponding to the identifier
         * @param sceneIdentifier The identifier of the scene
         * @return The corresponding scene
         */
        virtual Scene* createScene(const std::string& sceneIdentifier) = 0;
    };
}
}

#endif