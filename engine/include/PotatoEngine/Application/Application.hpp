#ifndef PE_APPLICATION_APPLICATION_HPP
#define PE_APPLICATION_APPLICATION_HPP

#include "types.hpp"
#include "Messaging/MessageBus.hpp"
#include "Messaging/Messenger.hpp"
#include "System/System.hpp"


#include <string>

namespace pe
{
namespace app
{
	/**
 	 * Base class for PotatoEngine applications
 	 */
	class Application : public pe::messaging::Messenger
	{
		private:
			// The title of the application
			std::string title;
			
			// The message bus of the application
			pe::messaging::MessageBus messageBus;
			
			// The list of registered systems
			std::list<pe::system::System*> systems;

			// Flag to know whether the app is running or not
			PEbool running;
		protected:
			/**
 			 * Initializes the application
 			 */
			virtual PEvoid init();

			/**
 			 * Cleans up the application 
 			 */
			virtual PEvoid cleanup();
			
		public:
		    /**
		     * Creates a new application
		     * @param applicationTitle The title of the application
		     */
			Application(const std::string& applicationTitle);
			virtual ~Application();

			/**
            * Runs the application
            * Initializes the systems,
            * Starts the main loop,
            * Calls the clean up method when the loop is exited
            */
            PEvoid run();

			/**
 			 * Returns the application title
 			 * @return title
 			 */
			const std::string& getTitle() const;

			/**
 			 * Registers a system to the application
 			 * @param system The system to register
 			 */
			PEvoid registerSystem(pe::system::System* system);

		// TODO fix that
		protected:
			// Callbacks
			PEvoid onApplicationStartup(PEvoid* data);
			PEvoid onApplicationStopRequest(PEvoid* data);
	};
}
}

#endif
