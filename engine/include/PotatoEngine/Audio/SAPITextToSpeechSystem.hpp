#ifndef PE_AUDIO_SAPITEXTTOSPEECHSYSTEM_HPP
#define PE_AUDIO_SAPITEXTTOSPEECHSYSTEM_HPP

#include "types.hpp"
#include "Audio/TextToSpeechSystem.hpp"

namespace pe
{
namespace audio
{
    /**
 	 * Text-to-speech system implementation for SAPI (windows)
 	 */
    class SAPITextToSpeechSystem : public pe::audio::TextToSpeechSystem
    {
    public:
        SAPITextToSpeechSystem();
        virtual ~SAPITextToSpeechSystem() override;

    protected:
        /**
          * Pronounces the given text string
          * @param text The text to pronounce
          */
        virtual PEvoid pronounce(const std::string& text) override;
    };
}
}

#endif