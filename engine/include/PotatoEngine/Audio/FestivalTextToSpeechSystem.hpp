#ifndef PE_AUDIO_FESTIVALTEXTTOSPEECHSYSTEM_HPP
#define PE_AUDIO_FESTIVALTEXTTOSPEECHSYSTEM_HPP

#include "types.hpp"
#include "Audio/TextToSpeechSystem.hpp"

#include <string>
#include <cstdlib>
#include <unistd.h>

namespace pe
{
namespace audio
{
	/**
 	 * Text-to-speech system implementation for festival (linux)
 	 */
	class FestivalTextToSpeechSystem : public pe::audio::TextToSpeechSystem
	{
		private:
			bool initialized;
			// pipe fd
			int fd[2];
			// festival pid
			pid_t pid;
		public:
			FestivalTextToSpeechSystem();
			virtual ~FestivalTextToSpeechSystem() override;

		protected:
			/**
 			 * Pronounces the given text string
 			 * @param text The text to pronounce
 			 */
			virtual PEvoid pronounce(const std::string& text) override;
	};
}
}


#endif
