#ifndef PE_AUDIO_TEXTTOSPEECHSYSTEM_HPP
#define PE_AUDIO_TEXTTOSPEECHSYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"

namespace pe
{
namespace audio
{
	/**
 	 * Base class for Text-to-speech systems
 	 */
	class TextToSpeechSystem : public pe::system::System
	{
		protected:
			/**
 			 * Registers the default callback methods for events
 			 */
			virtual PEvoid registerDefaultHandlers() override;

			/**
 			 * Pronounce the given text string
 			 * @param text The text to pronounce
 			 */
			virtual PEvoid pronounce(const std::string& text) = 0;

		public:
			virtual ~TextToSpeechSystem() override {};

		private:
			// Callbacks
			PEvoid onAudioPronounceRequest(PEvoid* data);
		
	};
}
}

#endif
