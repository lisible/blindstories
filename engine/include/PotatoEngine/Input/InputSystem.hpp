#ifndef PE_INPUT_INPUTSYSTEM_HPP
#define PE_INPUT_INPUTSYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"
#include "Input/KeyMap.hpp"
#include "Input/InputEvent.hpp"

namespace pe
{
namespace input
{
	/**
 	 * System used to translate hardware input (keyboard) into game inputs
 	 */
	class InputSystem : public pe::system::System
	{
		private:
			// The keymap used by the system
			KeyMap keyMap;	


			/**
 		 	 * Registers the default callback methods for events
 		 	 */
			PEvoid registerDefaultHandlers() override;

		public:
			InputSystem();
			~InputSystem() override;

			/**
 			 * Sets the key map used by the input system
 			 * @param keyMap the key map to use
 			 */
			PEvoid setKeyMap(const KeyMap& keyMap);	
		
		private:
			/**
 			 * Callbacks
 			 */
			PEvoid onInputReceived(PEvoid* data);
			PEvoid onKeyPressed(InputEvent* data);
			PEvoid onKeyReleased(InputEvent* data);
			PEvoid onMouseMoved(InputEvent* data);
			PEvoid onMouseButtonDown(InputEvent* data);
			PEvoid onMouseButtonUp(InputEvent* data);
			PEvoid onTextTyped(InputEvent* data);
	};
}
}

#endif
