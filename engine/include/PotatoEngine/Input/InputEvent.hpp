#ifndef PE_INPUT_INPUTEVENT_HPP
#define PE_INPUT_INPUTEVENT_HPP

#include "types.hpp"
#include "Input/Keyboard.hpp"
#include "Input/MouseButton.hpp"

namespace pe
{
namespace input
{
	/**
 	 * Stores the data associated with an input event
 	 */
	struct InputEvent
	{
		// Keyboard input data
		struct KeyboardEvent
		{
			Keyboard key;
		};

		struct MouseMotionEvent
		{
			PEint32 x;
			PEint32 y;
			PEint32 relX;
			PEint32 relY;
		};

		struct MouseClickEvent
		{
			PEint32 x;
			PEint32 y;
			MouseButton button;
		};

		struct TextEvent
		{
			PEchar text[32];
		};

		// The type of the event
		enum EventType
		{
			UNKNOWN,	
			KEY_PRESSED,
			KEY_RELEASED,
			MOUSE_MOVED,
			MOUSE_BUTTON_DOWN,
			MOUSE_BUTTON_UP,
			TEXT_TYPED
		} type;

		union {
			struct KeyboardEvent keyboard;
			struct MouseMotionEvent mouseMotion;
			struct MouseClickEvent mouseClick;
			struct TextEvent text;
		};
	};
}
}
#endif
