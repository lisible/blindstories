#ifndef PE_INPUT_KEYMAP_HPP
#define PE_INPUT_KEYMAP_HPP

#include "types.hpp"
#include "Input/Keyboard.hpp"
#include <unordered_map>

namespace pe
{
namespace input
{
	/**
 	 * Maps the key to action string indentifiers
 	 */
	class KeyMap
	{
		private:
			// The internal maps
			std::unordered_map<Keyboard, std::string> actions;
			std::unordered_map<std::string, Keyboard> keys;

		public:
			KeyMap();
			KeyMap(const KeyMap& keymap);
			~KeyMap();

			/**
 			 * Adds a mapping to the keymap
 			 * @param key The key
 			 * @param actionIdentifier The corresponding action
 			 */
			PEvoid addKey(Keyboard key, const std::string& actionIdentifier);
			/**
 			 * Removes a mapping
 			 * @param key The key of the mapping to remove
 			 */
			PEvoid removeKey(Keyboard key);

			/**
 			 * Returns the corresponding key for the given action
 			 * @param action The action
 			 * @return The key corresponding to the action
 			 */
			Keyboard getKeyForAction(const std::string& action);
			/**
 			 * Returns the action for the given key
 			 * @param key The key
 			 * @return The corresponding action
 			 */
			const std::string& getActionForKey(Keyboard key);

			/**
 			 * Returns true if the given key is mapped in the keymap
 			 * @param key The key
 			 * @return True if key is mapped in the keymap, false otherwise
 			 */
			PEbool hasKey(Keyboard key) const;
			
	};
}
}

#endif
