#ifndef PE_INPUT_MOUSEBUTTON_HPP
#define PE_INPUT_MOUSEBUTTON_HPP

namespace pe
{
namespace input
{
    /**
     * Enum for mouse buttons
     */
    enum class MouseButton
    {
        LEFT,
        RIGHT,
        UNKNOWN
    };
}
}

#endif