#ifndef PE_INPUT_KEYBOARD_HPP
#define PE_INPUT_KEYBOARD_HPP

#include "types.hpp"

#include <string>

namespace pe
{
namespace input
{
	enum Keyboard
	{
		KEY_UNKNOWN,
		KEY_A,
		KEY_B,
		KEY_C,
		KEY_D,
		KEY_E,
		KEY_F,
		KEY_G,
		KEY_H,
		KEY_I,
		KEY_J,
		KEY_K,
		KEY_L,
		KEY_M,
		KEY_N,
		KEY_O,
		KEY_P,
		KEY_Q,
		KEY_R,
		KEY_S,
		KEY_T,
		KEY_U,
		KEY_V,
		KEY_W,
		KEY_X,
		KEY_Y,
		KEY_Z,
		KEY_ESCAPE,
		KEY_ENTER,
		KEY_LCTRL,
		KEY_RCTRL,
		KEY_LSHIFT,
		KEY_RSHIFT,
		KEY_SPACE,
		KEY_UP,
		KEY_DOWN,
		KEY_LEFT,
		KEY_RIGHT,
		KEY_TAB,
		KEY_BACKSPACE,
	};

	/**
 	 * Converts a Keyboard enum value to a string
 	 * @param key The key to convert
 	 * @return The key as a string
 	 */
	std::string keyboardToString(Keyboard key);
	
	/**
 	 * Converts a string to a keyboard enum value
 	 * @param keyString the string
 	 * @return The corresponding keyboard enum value
 	 */
	Keyboard stringToKeyboard(const std::string& keyString);
}
}

#endif
