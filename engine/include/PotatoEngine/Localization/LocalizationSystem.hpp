#ifndef PE_LOCALIZATION_LOCALIZATIONSYSTEM_HPP
#define PE_LOCALIZATION_LOCALIZATIONSYSTEM_HPP

#include "System/System.hpp"

namespace pe
{
namespace localization
{
    class LocalizationSystem : public pe::system::System
    {
    protected:
        /**
          * Registers the default callback methods for events
          */
        virtual PEvoid registerDefaultHandlers() override;
    public:
        LocalizationSystem();
        ~LocalizationSystem() override;


    };
}
}

#endif