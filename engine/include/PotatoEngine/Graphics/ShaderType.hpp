#ifndef PE_GRAPHICS_SHADERTYPE_HPP
#define PE_GRAPHICS_SHADERTYPE_HPP

namespace pe
{
namespace graphics
{
	enum class ShaderType
	{
		UNKNOWN,
		VERTEX,
		FRAGMENT,
		GEOMETRY
	};
}
}

#endif
