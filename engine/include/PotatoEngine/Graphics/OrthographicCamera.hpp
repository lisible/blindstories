#ifndef PE_GRAPHICS_ORTHOGRAPHICCAMERA_HPP
#define PE_GRAPHICS_ORTHOGRAPHICCAMERA_HPP

#include "types.hpp"
#include <Maths/Maths.hpp>

namespace pe
{
namespace graphics
{
	class OrthographicCamera
	{
		private:
			pem::mat4 projectionMatrix;

		public:
			OrthographicCamera();
			OrthographicCamera(PEfloat left, PEfloat top,
								PEfloat right, PEfloat bottom,
								PEfloat near, PEfloat far);
			~OrthographicCamera();
			
			PEvoid setProjection(PEfloat left, PEfloat top,
								 PEfloat right, PEfloat bottom,
								 PEfloat near, PEfloat far);
			pem::mat4& getProjectionMatrix();

	};
}
}

#endif
