#ifndef PE_GRAPHICS_TEXTUREDQUAD_HPP
#define PE_GRAPHICS_TEXTUREDQUAD_HPP

#include "types.hpp"
#include "Graphics/Quad.hpp"

namespace pe
{
namespace graphics
{
	struct TexturedQuad
	{
		Quad quad;
		GLuint textureIdentifier;
	};
}
}

#endif
