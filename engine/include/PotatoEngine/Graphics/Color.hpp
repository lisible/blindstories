#ifndef PE_GRAPHICS_COLOR
#define PE_GRAPHICS_COLOR

#include "types.hpp"
#include "Maths/Maths.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Struct for do stuf f with c o lo or
 	 */
	struct Color
	{
		// Red component
		PEfloat red;
		// Green component
		PEfloat green;
		// Blue component
		PEfloat blue;
		// Alpha component
		PEfloat alpha;

		Color()
			: red(1.f)
			, green(1.f)
			, blue(1.f)
			, alpha(1.f)
		{}

		Color(PEfloat red, PEfloat green, PEfloat blue, PEfloat alpha = 1.0f)
			: red(red)
			, green(green)
			, blue(blue)
			, alpha(alpha)
		{
		}

		Color(const std::string& hexRepresentation)
		{

			std::string redStr = hexRepresentation.substr(0, 2);
			std::string greenStr = hexRepresentation.substr(2, 2);
			std::string blueStr = hexRepresentation.substr(4, 2);
			red = pe::maths::hex2dec<int>(redStr)/255.f;
			green = pe::maths::hex2dec<int>(greenStr)/255.f;
			blue = pe::maths::hex2dec<int>(blueStr)/255.f;


			if(hexRepresentation.length() == 8)
			{
				std::string alphaStr = hexRepresentation.substr(6, 2);
				alpha = pe::maths::hex2dec<int>(alphaStr)/255.f;
			}
			else
			{
				alpha = 1.f;
			}
		}
	};

	static const Color WHITE = Color(1.0f, 1.0f, 1.0f, 1.0f);
	static const Color RED = Color(1.f, 0.f, 0.f, 1.f);
	static const Color GREEN = Color(0.f, 1.f, 0.f, 1.f);
	static const Color BLUE = Color(0.f, 0.f, 1.f, 1.f);
}
}


#endif
