#ifndef PE_GRAPHICS_RENDERER2D_HPP
#define PE_GRAPHICS_RENDERER2D_HPP

#include "types.hpp"
#include "Graphics/LowLevelRenderer.hpp"
#include "Graphics/RectangleShape.hpp"
#include "Graphics/Sprite.hpp"
#include "Maths/Maths.hpp"

namespace pe
{
namespace graphics
{
	class GraphicSystem;

	/**
 	 * Used to render 2D shapes and sprites
 	 */
	class Renderer2D
	{
		private:
			// The low level renderer used by the Renderer2D
			LowLevelRenderer& lowLevelRenderer;

			GraphicSystem* system;
		public:
			Renderer2D(GraphicSystem* graphicSystem, LowLevelRenderer& lowLevelRenderer);
			~Renderer2D();

			/**
			 * Draws a point
			 * @param point The point to draw
			 * @param color The color of the point
			 * @param transform The transform matrix
			 */
			PEvoid drawPoint(const Point& point, const Color& color, const pem::mat4& transform);
			/**
			* Draws a line
			* @param line The line to draw
			* @param color The color of the line
			* @param transform The transform matrix
			*/
			PEvoid drawLine(const Line& line, const Color& color, const pem::mat4& transform);
			/**
			* Draws a rectangle
			* @param rectangle The rectangle to draw
			* @param color The color of the rectangle
			* @param transform The transform matrix
			*/
			PEvoid drawRectangle(const pem::Rectangle& rectangle, const Color& color, const pem::mat4& transform);
			/**
			* Draws an image
			* @param textureIdentifier The texture of the image
			* @param sourceRectangle The source rectangle
			* @param destinationRectangle The destination rectangle
			* @param color The color
			* @param transform The transofrm matrix
			*/
			PEvoid drawImage(const std::string& textureIdentifier, const pem::Rectangle& sourceRectangle,
						 const pem::Rectangle& destinationRectangle, const Color& color,
						 const pem::mat4& transform);
			/**
			* Draws a text
			* @param text The text to draw
			* @param fontIdentifier The identifier of the font to use
			* @param fontSize The size of the font
			* @param color The color of the text
			* @param transform The transform matrix
			*/
			PEvoid drawText(const std::string& text, const std::string& fontIdentifier, PEfloat x, PEfloat y,
							PEuint32 fontSize, const Color& color, const pem::mat4& transform);

			/**
			 * Draws a text area
			 * @param text The text
			 * @param fontName The name of the font to use
			 * @param width The width of the text
			 * @param height The height of the text
			 * @param color The color of the text
			 * @param transformMatrix The transform matrix to use
			 */
			PEvoid drawTextArea(const std::string& text,
								const std::string& fontName,
								PEuint32 width,
								PEuint32 height,
								Color color,
								const pem::mat4& transformMatrix);
	};
}
}

#endif
