#ifndef PE_GRAPHICS_LOWLEVELRENDERER_HPP
#define PE_GRAPHICS_LOWLEVELRENDERER_HPP

#include "types.hpp"
#include "Graphics/Vertex.hpp"
#include "Graphics/Line.hpp"
#include "Graphics/Quad.hpp"
#include "Graphics/TexturedQuad.hpp"
#include "Graphics/Shader.hpp"
#include "Graphics/LowLevelBatch.hpp"
#include "Graphics/OrthographicCamera.hpp"

#include <vector>

namespace pe
{
namespace graphics
{
	class LowLevelRenderer
	{
		private:
			// Camera to use
			OrthographicCamera camera;

			// Current shader identifier
			GLuint currentShader;

			// Batches to render
			std::vector<LowLevelBatch> batches;
		public:
			LowLevelRenderer();
			~LowLevelRenderer();

			/**
 			 * Creates the required buffers and stuff
 			 */
			PEvoid initialize();

			/**
 			 * Prepares the rendering by filling the buffers
 			 */
			PEvoid prepareRendering();
			
			/**
 			 * Renders
 			 */
			PEvoid render();

			/**
 			 * Cleans up the buffers
 			 */	
			PEvoid finishRendering();


			PEvoid setCamera(OrthographicCamera& camera);

			PEvoid useShader(const Shader& shader);

			PEvoid drawVertex(const Vertex& v, const pem::mat4& transformMatrix);
			PEvoid drawLine(const Vertex& a, const Vertex& b, const pem::mat4& transformMatrix);
			PEvoid drawQuad(const Quad& q, const pem::mat4& transformMatrix);
			PEvoid drawTexturedQuad(const TexturedQuad& q, const pem::mat4& transformMatrix);
	};
}
}

#endif
