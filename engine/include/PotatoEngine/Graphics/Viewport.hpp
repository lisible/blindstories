#ifndef PE_GRAPHICS_VIEWPORT_HPP
#define PE_GRAPHICS_VIEWPORT_HPP

#include "types.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Describes a viewport
 	 */
	class Viewport
	{
		private:
			static const PEuint32 DEFAULT_WIDTH;
			static const PEuint32 DEFAULT_HEIGHT;
	

			// The width of the viewport
			PEuint32 width;
			// The height of the viewport
			PEuint32 height;	

		public:
			Viewport();
			Viewport(PEuint32 viewportWidth, PEuint32 viewportHeight);
			~Viewport();

			/**
 			 * Sets the width of the viewport
 			 * @param width The new width of the viewport
 			 */
			PEvoid setWidth(PEuint32 viewportWidth);
			/**
 			 * Returns the width of the viewport
 			 * @return The width of the viewport
 			 */
			PEuint32 getWidth() const;

			/**
 			 * Sets the height of the viewport
 			 * @param height The new height of the viewport
 			 */
			PEvoid setHeight(PEuint32 viewportHeight);
			/**
 			 * Returns the height of the viewport
 			 * @return The height of the viewport
 			 */
			PEuint32 getHeight() const;


	};
}
}

#endif
