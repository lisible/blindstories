#ifndef PE_GRAPHICS_GUIRENDERER_HPP
#define PE_GRAPHICS_GUIRENDERER_HPP

#include "types.hpp"
#include "Graphics/Renderer2D.hpp"
#include "Graphics/LowLevelRenderer.hpp"
#include "UI/UserInterface.hpp"
#include "UI/GUIFrame.hpp"
#include "UI/GUIImage.hpp"
#include "UI/GUILabel.hpp"
#include "UI/GUITextArea.hpp"
#include "Graphics/Viewport.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Renderer for the GUI
 	 */
	class GUIRenderer
	{
		private:
			// The renderer used
			Renderer2D& renderer;
			// The graphic system
			GraphicSystem* system;

			// The viewport used to render
			Viewport viewport;
		public:
			GUIRenderer() = delete;
			GUIRenderer(GraphicSystem* system, Renderer2D& renderer2D);
			~GUIRenderer();

			/**
 			 * Resizes the viewport used by the renderer 2D
 			 * @param viewportWidth The new width of the viewport
 			 * @param viewportHeight The new height of the viewport
 			 */
			PEvoid resizeViewport(PEuint32 viewportWidth, PEuint32 viewportHeight);

			/**
 			 * Renders a user interface
 			 * @param ui The user interface to draw
 			 */
			PEvoid draw(pe::ui::UserInterface& ui);

		private:
			/**
 			 * Recursively draws components
 			 * @param component
 			 */
			PEvoid drawComponent(pe::ui::GUIComponent& component, const pem::mat4& transform);
	};
}
}

#endif
