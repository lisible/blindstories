#ifndef PE_GRAPHICS_SPRITE_HPP
#define PE_GRAPHICS_SPRITE_HPP

#include "types.hpp"
#include "Graphics/Transformable.hpp"
#include "Graphics/Texture.hpp"
#include "Maths/Maths.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Class representing a sprite
 	 */
	class Sprite : public Transformable
	{
		private:
			// The name of the texture used by the sprite
			std::string texture;
			// The texture rectangle
			pem::Rectangle textureRectangle;
			// The size of the sprite
			pem::vec2 size;
		public:
			Sprite();
			~Sprite();

			/**
 			 * Sets the texture of the sprite
 			 * @param texture The texture of the sprite
 			 */
			PEvoid setTexture(const std::string& texture);
			/**
 			 * Returns the texture
 			 * @return The texture
 			 */
			const std::string& getTexture() const;

			/**
 			 * Sets the texture rectangle of the sprite
 			 * @param x The x position in the texture
 			 * @param y The y position in the texture
 			 * @param width The width in the texture
 			 * @param height The height in the texture
 			 */
			PEvoid setTextureRectangle(PEfloat x, PEfloat y,
									   PEfloat width, PEfloat height);
			/**
 			 * Returns the texture rectangle
 			 * @return The texture rectangle
 			 */
			const pem::Rectangle& getTextureRectangle() const;

			/**
 			 * Sets the size of the sprite
 			 * @param width The width
 			 * @param height The height
 			 */
			PEvoid setSize(PEfloat width, PEfloat height);
			/**
 			 * Returns the size of the sprite
 			 * @return The size of the sprite
 			 */
			const pem::vec2& getSize() const;
	};
}
}

#endif
