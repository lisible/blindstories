#ifndef PE_GRAPHICS_LOWLEVELBATCH_HPP
#define PE_GRAPHICS_LOWLEVELBATCH_HPP

#include "types.hpp"
#include "Graphics/Vertex.hpp"

#include <vector>

namespace pe
{
namespace graphics
{
	/**
 	 * Types for low level batches
 	 */
	enum class LowLevelBatchType
	{
		 VERTEX,
		 LINE,
		 QUAD
	};

	/**
 	 * Represents a batch of vertices
 	 */
	struct LowLevelBatch
	{
		GLuint vao;
		GLuint vbo;
		GLuint ebo;
		GLuint texture;
		GLuint shader;
		pem::mat4 mvp;

		// The type of the batch
		LowLevelBatchType type;
		// The vertices of the batch
		std::vector<Vertex> vertices;

		LowLevelBatch()
			: texture(0)
			, shader(0)
		{}
	};
}
}

#endif
