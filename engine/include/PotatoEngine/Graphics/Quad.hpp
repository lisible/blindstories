#ifndef PE_GRAPHICS_QUAD_HPP
#define PE_GRAPHICS_QUAD_HPP

#include "types.hpp"
#include "Graphics/Vertex.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Struct for quads
 	 */
	struct Quad
	{
		Vertex topLeft;
		Vertex topRight;
		Vertex bottomRight;
		Vertex bottomLeft;

		Quad()
		{}
	};
}
}

#endif
