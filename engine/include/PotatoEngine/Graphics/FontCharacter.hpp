#ifndef PE_GRAPHICS_FONTCHARACTER_HPP
#define PE_GRAPHICS_FONTCHARACTER_HPP

#include "types.hpp"
#include "Maths/Maths.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Contains data about font characters
 	 */
	class FontCharacter
	{
		private:
			// The code of the character
			PEchar code;

			// The position of the character
			pem::vec2u position;			
			// The size of the character in pixel
			pem::vec2u size;
			// The offset of the character
			pem::vec2u offset;
			// The space after the character
			PEuint32 xAdvance;	

		public:
			FontCharacter(PEchar code,
						  PEuint32 x, PEuint32 y,
						  PEuint32 width, PEuint32 height,
						  PEuint32 xOffset, PEuint32 yOffset,
						  PEuint32 xAdvance);
			~FontCharacter();

			/**
 			 * Returns the code of the character
 			 * @return The code of the character
 			 */
			PEchar getCode() const;

			/**
 			 * The position of the character
 			 * @return The position of the character
 			 */
			const pem::vec2u& getPosition() const;
			/**
 			 * The size of the character
 			 * @return The size of the character
 			 */
			const pem::vec2u& getSize() const;
			/**
 			 * The offset of the character
 			 * @return The offset of the character
 			 */
			const pem::vec2u& getOffset() const;
			/**
 			 * Returns the space after the character
 			 * @return The space after the character
 			 */
			PEuint32 getXAdvance() const;
	};
}
}

#endif
