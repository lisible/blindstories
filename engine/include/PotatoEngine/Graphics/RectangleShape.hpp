#ifndef PE_GRAPHICS_RECTANGLESHAPE_HPP
#define PE_GRAPHICS_RECTANGLESHAPE_HPP

#include "types.hpp"
#include "Maths/Vector2.hpp"
#include "Graphics/Color.hpp"
#include "Graphics/Transformable.hpp"

namespace pe
{
namespace graphics
{
	class RectangleShape : public Transformable
	{
		private:
			// The color of the shape
			Color color;

			// The size of the shape
			pem::vec2 size;
		public:
			RectangleShape();
			~RectangleShape();

			/**
 			 * Returns the color of the rectangle shape
 			 * @return The color of the rectangle shape
 			 */
			Color getColor() const;
			/**
 			 * Sets the color of the shape
 			 * @param color The new color of the shape
 			 */
			PEvoid setColor(const Color& color);

			/**
 			 * Sets the size of the shape
 			 * @param width The width of the shape
 			 * @param height The height of the shape
 			 */
			PEvoid setSize(PEfloat width, PEfloat height);
			/**
 			 * Returns the size of the shape
 			 * @return The size of the shape
 			 */
			const pem::vec2& getSize() const;
	};
}
}

#endif
