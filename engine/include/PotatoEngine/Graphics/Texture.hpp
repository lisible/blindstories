#ifndef PE_GRAPHICS_TEXTURE_HPP
#define PE_GRAPHICS_TEXTURE_HPP

#include "types.hpp"
#include "Maths/Maths.hpp"
#include <GL/glew.h>

namespace pe
{
namespace graphics
{
	/**
 	 * Represents a texture
 	 */
	class Texture
	{
		private:
			// The identifier of the texture
			GLuint textureIdentifier;

			// The size of the texture
			pem::vec2i textureSize;

		public:
			Texture(){};
			Texture(GLuint identifier, int width, int height);
			~Texture();

			/**
 			 * Returns the identifier of the texture
 			 * @return The identifier of the texture
 			 */
			GLuint getIdentifier() const;

			/**
 			 * Returns the size of the texture
 			 * @return The size of the texture
 			 */
			const pem::vec2i& getSize() const;
	};
}
}

#endif
