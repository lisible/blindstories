#ifndef PE_GRAPHICS_GRAPHICSYSTEM_HPP
#define PE_GRAPHICS_GRAPHICSYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"
#include "Graphics/LowLevelRenderer.hpp"
#include "Graphics/Renderer2D.hpp"
#include "Graphics/GUIRenderer.hpp"
#include "Graphics/OrthographicCamera.hpp"

#include "Graphics/Font.hpp"

namespace pe
{
namespace graphics
{
	class GraphicSystem : public pe::system::System
	{
		protected:
			/**
 			 * Registers the default callback methods for events
 			 */
			virtual PEvoid registerDefaultHandlers() override;
		private:
			// Used to render low level stuff (vertices, lines, quads)
			LowLevelRenderer lowLevelRenderer;
			// Used to render 2D stuff
			Renderer2D renderer2D;
			// Used to render graphical interface
			GUIRenderer guiRenderer;

		public:
			GraphicSystem();
			~GraphicSystem();

			PEvoid setCamera(OrthographicCamera& camera);

			/**
 			 * Asks the resource system for a texture
 			 * @param resourceName The name of the texture to get
 			 * @return The texture
 			 */
			Texture* getTexture(const std::string& resourceName);
			/**
 			 * Asks the resource system for a font
 			 * @param resourceName The name of the font to get
 			 * @return The font
 			 */
			Font* getFont(const std::string& resourceName);
			/**
 			 * Asks the reousrce system for a shader
 			 * @param resourceName The name of the shader to get
 			 * @return The shader
 			 */
			Shader* getShader(const std::string& resourceName);

		private:
			// Callbacks
			PEvoid onRenderUIRequest(PEvoid* data);
			PEvoid onInitializationRequest(PEvoid* data);
			PEvoid onRenderRequest(PEvoid* data);

	};
}
}

#endif
