#ifndef PE_GRAPHICS_LINE_HPP
#define PE_GRAPHICS_LINE_HPP

#include "types.hpp"
#include "Graphics/Point.hpp"
#include "Graphics/Color.hpp"
#include "Graphics/Transformable.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Struct for lines
 	 */
	struct Line
	{
		// First point of the line
		Point firstPoint;
		// Second point of the line
		Point secondPoint;

		Line()
		{
		}
		Line(const Point& firstPoint, const Point& secondPoint)
			: firstPoint(firstPoint)
			, secondPoint(secondPoint)
		{
		}
		Line(PEfloat x1, PEfloat y1, PEfloat x2, PEfloat y2)
			: firstPoint(x1, y1)
			, secondPoint(x2, y2)
		{
		}
	};
}
}

#endif
