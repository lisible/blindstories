#ifndef PE_GRAPHICS_FONT_HPP
#define PE_GRAPHICS_FONT_HPP

#include "types.hpp"
#include "Graphics/FontCharacter.hpp"
#include "Graphics/Texture.hpp"

#include <unordered_map>

namespace pe
{
namespace graphics
{
	class Font
	{
		private:
			// The texture containing the glyphs
			Texture* texture;

			// The font characters
			std::unordered_map<PEchar, FontCharacter> fontCharacters;

			// The line height of the font
			PEuint32 lineHeight;

		public:
			Font(Texture* texture);
			~Font();

			/**
 			 * Returns the texture 
 			 * @return The texture 
 			 */
			Texture* getTexture() const;

			/**
			 * Sets the line height of the font
			 * @param lineHeight The line height of the font
			 */
			PEvoid setLineHeight(PEuint32 lineHeight);
			/**
			 * Returns the line height of the font
			 * @return The line height of the font
			 */
			PEuint32 getLineHeight() const;

			/**
 			 * Adds a character to the font
 			 * @param fontCharacter The character to add
 			 */
			PEvoid addCharacter(const FontCharacter& fontCharacter);
			/**
 			 * Returns the corresponding font character object
 			 * @param character The character
 			 * @return The corresponding font character
 			 */
			const FontCharacter& getFontCharacter(PEchar character) const;
	};
}
}


#endif
