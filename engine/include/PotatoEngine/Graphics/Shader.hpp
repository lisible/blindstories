#ifndef PE_GRAPHICS_SHADER_HPP
#define PE_GRAPHICS_SHADER_HPP

#include "types.hpp"
#include <GL/glew.h>

namespace pe
{
namespace graphics
{
	/**
 	 * Represents a shader program
 	 */
	class Shader
	{
		private:
			// The identifier of the shader
			GLuint shaderIdentifier;
		public:
			Shader(){};
			Shader(GLuint identifier);
			~Shader();

			/**
 			 * Returns the identifier of the shader
 			 * @return The identifier of the shader
 			 */
			GLuint getIdentifier() const;
	};
}
}

#endif
