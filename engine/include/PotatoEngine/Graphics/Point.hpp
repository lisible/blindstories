#ifndef PE_GRAPHICS_POINT_HPP
#define PE_GRAPHICS_POINT_HPP

#include "types.hpp"
#include "Graphics/Color.hpp"
#include "Graphics/Transformable.hpp"

namespace pe
{
namespace graphics
{
	struct Point
	{
		PEfloat x;
		PEfloat y;

		Point()
			: x(0.f)
			, y(0.f)
		{
		}

		Point(PEfloat x, PEfloat y)
			: x(x)
			, y(y)
		{
		}
	};
}
}

#endif
