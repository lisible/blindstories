#ifndef PE_GRAPHICS_VERTEX_HPP
#define PE_GRAPHICS_VERTEX_HPP

#include "types.hpp"
#include "Maths/Maths.hpp"
#include "Graphics/Color.hpp"
#include "GL/glew.h"

// TODO delete
#include <iostream>

namespace pe
{
namespace graphics
{
	/**
 	 * Struct containing vertex data
 	 */
	struct Vertex
	{
		// Position of the vertex
		pem::Vector3<GLfloat> position;
		// Color of the vertex
		Color color;
		// Texture cordinates of the vertex
		pem::Vector2<GLfloat> textureCoordinates;	

		Vertex()
		{}

		Vertex(GLfloat x, GLfloat y, GLfloat z = 0.0f, Color color = WHITE)
			: position(x, y, z)	
			, color(color)
			, textureCoordinates(0.0f, 0.0f)
		{
		}

		Vertex transform(const pem::mat4& transformMatrix) const
		{
			Vertex transformedVertex;

			pem::vec4 vertexPosition(position.x, position.y, position.z, 1.f);
			
			pem::vec4 transformedVertexPosition = vertexPosition*transformMatrix;
			transformedVertex.position.x = transformedVertexPosition.x;
			transformedVertex.position.y = transformedVertexPosition.y;
			transformedVertex.position.z = transformedVertexPosition.z;

			transformedVertex.color = color;
			transformedVertex.textureCoordinates = textureCoordinates;

			return transformedVertex;
		}
	};
}
}

#endif
