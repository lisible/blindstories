#ifndef PE_GRAPHICS_TRANSFORMABLE_HPP
#define PE_GRAPHICS_TRANSFORMABLE_HPP

#include "types.hpp"
#include "Maths/Maths.hpp"
#include "Graphics/Viewport.hpp"

namespace pe
{
namespace graphics
{
	/**
 	 * Base class for transformable objects
 	 */
	class Transformable
	{
		private:
			pem::vec2 position;
			pem::vec2 scale;
			pem::vec4 angle;
			pem::vec2 origin;

			mutable pem::mat4 transformMatrix;
			mutable pem::mat4 inverseTransformMatrix;
			
			PEbool needTransformMatrixUpdate;
			PEbool needInverseTransformMatrixUpdate;	
		public:
			Transformable();
			virtual ~Transformable();

			PEvoid setPosition(PEfloat x, PEfloat y);
			PEvoid move(PEfloat x, PEfloat y);
			const pem::vec2& getPosition() const;

			PEvoid setScale(PEfloat scaleX, PEfloat scaleY);
			PEvoid relScale(PEfloat relScaleX, PEfloat relScaleY);
			const pem::vec2& getScale() const;

			PEvoid setAngle(PEfloat angle);
			PEvoid rotate(PEfloat angle);
			PEfloat getAngle() const;

			PEvoid setOrigin(PEfloat x, PEfloat y);
			const pem::vec2& getOrigin() const;

			pem::mat4& getTransformMatrix();
			pem::mat4& getInverseTransformMatrix();
	};
}
}

#endif
