#ifndef PE_LOGIC_LOGICSYSTEM_HPP
#define PE_LOGIC_LOGICSYSTEM_HPP


#include "types.hpp"
#include "System/System.hpp"
#include "Scene/SceneStack.hpp"
#include "Scene/ISceneFactory.hpp"

namespace pe
{
namespace logic
{
	class LogicSystem : public pe::system::System
	{
		private:
			// The scene stack used by the logic system
			pe::scene::SceneStack sceneStack;

			// The scene factory to use to create the scenes
			pe::scene::ISceneFactory* sceneFactory;

		protected:
			/**
 			 * Registers the default callback methods for events
 			 */
			PEvoid registerDefaultHandlers() override;

		public:
			LogicSystem();
			~LogicSystem() override;

			/**
			 * Sets the scene factory to use
			 * @param sceneFactory The scene factory to use
			 */
			PEvoid setSceneFactory(pe::scene::ISceneFactory* sceneFactory);

			/**
			 * Pops a scene of the scene stack
			 */
			PEvoid popScene();
			/**
			 * Pushes a scene onto the scene stack
			 * @param identifier The identifier of the scene to push
			 */
			PEvoid pushScene(const std::string& identifier);
			/**
			 * Changes the current scene on top of the stack
			 * @param identfier The identifier of the scene to change to
			 */
			PEvoid changeScene(const std::string& identfier);

			/**
			 * Logs a message
			 * @param message The message to log
			 */
			PEvoid logMessage(const std::string& message);


			/**
			 * Adds a component to the ui
			 * @param parentIdentifier The identifier of the parent ui component
			 * @param component The ui component to add
			 */
			PEvoid uiAddComponent(const std::string& parentIdentifier, ui::GUIComponent* component);
            /**
             * Changes an attribute of a component of the ui
             * @param componentIdentifier The identifier of the component
             * @param attribute The attribute to change
             * @param value The new value of the attribute
             */
            PEvoid changeUIAttribute(const std::string& componentIdentifier,
                                 const std::string& attribute,
                                 const std::string& value);
            /**
            * Asks the UI to focus the next component
            */
            PEvoid uiFocusNext();
            /**
            * Asks the UI to focus the previous component
            */
            PEvoid uiFocusPrevious();
			/**
			 * Posts/Activates the focused component
			 */
			PEvoid uiPost();

			/**
			 * Sends a text to speech request
			 * @param text The text to pronounce
			 */
			PEvoid ttsSay(const std::string& text);



			/**
			 * Registers a UI click callback
			 * @param componentIdentifier The identifier of the component
			 * @param callback The callback to run
			 */
			PEvoid registerUIClickCallback(const std::string& componentIdentifier, std::function<PEvoid(PEvoid)> callback);
			/**
			 * Registers a UI post callback
			 * @param componentIdentifier The identifier of the component
			 * @param callback The callback to run
			 */
			PEvoid registerUIPostCallback(const std::string& componentIdentifier, std::function<PEvoid(PEvoid)> callback);

			/**
 			 * Runs a tick
 			 */
			PEvoid tick();
			/**
 			 * Renders the current scene
 			 */
			PEvoid render();

		private:
			// Callbacks
			PEvoid onPushSceneRequest(PEvoid* scene);
			PEvoid onPopSceneRequest();
			PEvoid onActionPressed(PEvoid* action);
			PEvoid onTextTyped(PEvoid* text);
	};
}
}

#endif
