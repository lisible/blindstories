#ifndef PE_TIME_TIMER_HPP
#define PE_TIME_TIMER_HPP

#include "types.hpp"
#include <chrono>

namespace pe
{
namespace time
{
    /**
     * Class for timer
     */
    class Timer
    {
    private:
        // The time when the timer was started
        std::chrono::time_point<std::chrono::high_resolution_clock> startTime;

    public:
        Timer();
        ~Timer();

        /**
         * Starts the timer
         */
        PEvoid start();

        /**
         * Returns the number of milliseconds since the timer was started
         * @return The number of milliseconds since the timer was started
         */
        std::chrono::milliseconds::rep getTime();
        /**
         * Restarts the timer
         * @return The number of milliseconds that were spent since it started
         */
        std::chrono::milliseconds::rep restart();
    };
}
}

#endif