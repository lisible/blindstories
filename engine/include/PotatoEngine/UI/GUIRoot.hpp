#ifndef PE_UI_GUIROOT_HPP
#define PE_UI_GUIROOT_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"

namespace pe
{
namespace ui
{
	class GUIRoot : public GUIComponent
	{
		public:
			GUIRoot();
			~GUIRoot() override;			

			PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform) override;
	};
}
}

#endif
