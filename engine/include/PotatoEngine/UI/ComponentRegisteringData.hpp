#ifndef PE_UI_COMPONENTREGISTERINGDATA_HPP
#define PE_UI_COMPONENTREGISTERINGDATA_HPP

#include "UI/GUIComponent.hpp"
#include <string>

namespace pe
{
namespace ui
{
    /**
     * Used to send ui adding data
     */
    struct ComponentRegisteringData
    {
        // The identifier of the parent
        std::string parentIdentifier;
        // The component to add
        ui::GUIComponent* component;
    };
}
}

#endif