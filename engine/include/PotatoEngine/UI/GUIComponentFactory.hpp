#ifndef PE_UI_GUICOMPONENTFACTORY_HPP
#define PE_UI_GUICOMPONENTFACTORY_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"
#include <tinyxml2.h>

namespace pe
{
namespace ui
{
    /**
     * Used to create GUI Components, can be overriden to be compatible with additional components
     */
    class GUIComponentFactory
    {
    private:
        GUIComponent* createRootFromXML(tinyxml2::XMLElement* element);
        GUIComponent* createFrameFromXML(tinyxml2::XMLElement* element);
        GUIComponent* createLabelFromXML(tinyxml2::XMLElement* element);
        GUIComponent* createTextAreaFromXML(tinyxml2::XMLElement* element);
        GUIComponent* createImageFromXML(tinyxml2::XMLElement* element);
        GUIComponent* createButtonFromXML(tinyxml2::XMLElement* element);
    public:
        GUIComponentFactory();
        ~GUIComponentFactory();

        /**
         * Converts an XML Node to a new GUIComponent*
         * @param node The XML Node to parse
         * @return The corresponding GUIComponent*
         */
        virtual GUIComponent* createComponentFromXML(tinyxml2::XMLNode* node);
    };
}
}

#endif