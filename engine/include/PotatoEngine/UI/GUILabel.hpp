#ifndef PE_UI_GUILABEL_HPP
#define PE_UI_GUILABEL_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"
#include "Graphics/Color.hpp"

namespace pe
{
namespace ui
{

	class GUILabel : public GUIComponent
	{
		private:
			// The text of the label
			std::string text;
			// The font name of the label
			std::string fontName;
			// The text color
			pe::graphics::Color color;
			// The text color
			pe::graphics::Color defaultColor;
			// The text color on hover
			pe::graphics::Color hoverColor;

		public:
			GUILabel(const std::string& identifier,
					 const std::string& fontName,
					 const std::string& text = "label");
			~GUILabel();

			/**
 			 * Sets the text of the label
 			 * @param text The text of the label
 			 */
			PEvoid setText(const std::string& text);
			/**
 			 * Returns the text of the label
 			 * @return The text of the label
 			 */
			const std::string& getText() const;

			/**
 			 * Returns the name of the font
 			 * @return The name of the font
 			 */
			const std::string& getFontName() const;

			/**
			  * Sets the color of the label
			  * @param color The color of the label
			  */
			PEvoid setColor(pe::graphics::Color color);
			/**
			  * Returns the color of the label
			  * @return The color of the label
			  */
			pe::graphics::Color getColor() const;

			/**
			* Sets the default color of the label
			* @param color The color of the label
			*/
			PEvoid setDefaultColor(pe::graphics::Color color);
			/**
			* Returns the color of the label
			* @return The color of the label
			*/
			pe::graphics::Color getDefaultColor() const;

			/**
			* Sets the hover color of the label
			* @param color The color of the label
			*/
			PEvoid setHoverColor(pe::graphics::Color color);
			/**
			* Returns the color of the label
			* @return The color of the label
			*/
			pe::graphics::Color getHoverColor() const;

			/**
 			 * Renders the frame using the given renderer
 			 * @param renderer The renderer
 			 */
			PEvoid render(pe::graphics::Renderer2D& renderer,
						  const pem::mat4& transform) override;

			/**
			 * Changes an attribute of the component
			 * @param attribute The attribute to change
			 * @param value The new value of the attribute
			 */
			virtual PEvoid changeAttribute(const std::string& attribute, const std::string& value) override;

			virtual PEvoid onFocus() override;
			virtual PEvoid onFocusExit() override;
			virtual PEvoid onHover() override;
			virtual PEvoid onHoverExit() override;
	};

}
}

#endif
