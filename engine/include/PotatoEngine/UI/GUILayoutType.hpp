#ifndef PE_UI_GUILAYOUTTYPE_HPP
#define PE_UI_GUILAYOUTTYPE_HPP

#include "types.hpp"

namespace pe
{
namespace ui
{
	/**
 	 * Enum for layout types
 	 */
	enum GUILayoutType
	{
		VERTICAL,
		HORIZONTAL
	};
}
}

#endif
