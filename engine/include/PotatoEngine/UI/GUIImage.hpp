#ifndef PE_UI_GUIIMAGE_HPP
#define PE_UI_GUIIMAGE_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"
#include "Graphics/Sprite.hpp"

namespace pe
{
namespace ui
{
	class GUIImage : public GUIComponent
	{
		private:
			// The sprite of the GUI Image TODO remove ?
			pe::graphics::Sprite sprite;

		public:
			GUIImage(const std::string& identifier);
			virtual ~GUIImage();

			/**
 			 * Renders the frame using the given renderer
 			 * @param renderer The renderer
 			 */
			PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& tranform) override;

			/**
 			 * Sets the name of the texture of the image
 			 * @param texture The name of the texture
 			 */
			PEvoid setTexture(const std::string& textureName);
			/**
 			 * Returns the name of the texture used by the image
 			 * @return The name of the texture used by the image
 			 */
			const std::string& getTexture() const;

			/**
 			 * Sets the size of the component
 			 * @param width The width of the component
 			 * @param height The height of the component
 			 */
			PEvoid setSize(PEfloat width, PEfloat height) override;
			
			/**
 			 * Sets the texture rectangle of the image
 			 * @param x The x texture coordinate
 			 * @param y The y texture coordinate
 			 * @param width The width
 			 * @param height The height
 			 */
			PEvoid setTextureRectangle(PEfloat x, PEfloat y, PEfloat width, PEfloat height);

			/**
 			 * Returns the internal sprite of the image
 			 * @return The sprite
 			 */
			const pe::graphics::Sprite& getSprite() const;

			/**
 			 * Returns the texture rectangle
 			 * @return The texture rectangle
 			 */
			const pem::Rectangle& getTextureRectangle() const;
	};
}
}

#endif
