#ifndef PE_UI_CALLBACKDATA_HPP
#define PE_UI_CALLBACKDATA_HPP

#include "types.hpp"
#include <functional>

namespace pe
{
namespace ui
{
    /**
     * Used for ui callback registering messages
     */
    struct CallbackData
    {
        // The component
        std::string componentIdentifier;
        // The callback
        std::function<PEvoid(PEvoid)> callbackFunction;
    };
}
}

#endif