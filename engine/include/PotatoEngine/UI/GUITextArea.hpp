#ifndef PE_UI_GUITEXTAREA_HPP
#define PE_UI_GUITEXTAREA_HPP

#include "Graphics/Color.hpp"
#include "UI/GUIComponent.hpp"
#include <string>

namespace pe
{
namespace ui
{
    class GUITextArea : public GUIComponent
    {
    private:
        // The text
        std::string text;
        // The font of the text
        std::string font;
        // The color of the text
        pe::graphics::Color color;

    public:
        GUITextArea(const std::string& identifier,
                    const std::string& fontName,
                    const std::string& text = "TextArea");
        ~GUITextArea();

        /**
         * Sets the text of the text area
         * @param text The text of the text area
         */
        PEvoid setText(const std::string& text);
        /**
          * Returns the text of the text area
          * @return The text of the text area
          */
        const std::string& getText() const;

        /**
          * Returns the name of the text area
          * @return The name of the text area
          */
        const std::string& getFontName() const;

        /**
          * Sets the color of the text area
          * @param color The color of the text area
          */
        PEvoid setColor(pe::graphics::Color color);
        /**
          * Returns the color of the text area
          * @return The color of the text area
          */
        pe::graphics::Color getColor() const;

        /**
         * Changes an attribute of the component
         * @param attribute The attribute to change
         * @param value The new value of the attribute
         */
        virtual PEvoid changeAttribute(const std::string& attribute, const std::string& value) override;

        PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform) override;
    };
}
}
#endif