#ifndef PE_UI_GUISYSTEM_HPP
#define PE_UI_GUISYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"
#include "UI/UserInterface.hpp"

#include <functional>

namespace pe
{
namespace ui
{
    class GUISystem : public pe::system::System
    {
    private:
        UserInterface* ui;

        std::unordered_map<std::string, std::function<PEvoid(PEvoid)>> clickCallbacks;
        std::unordered_map<std::string, std::function<PEvoid(PEvoid)>> postCallbacks;

    protected:
        /**
          * Registers the default callback methods for events
          */
        virtual PEvoid registerDefaultHandlers() override;

    public:
        GUISystem();
        ~GUISystem() override;

        /**
         * Requests for the TTS system
         * @param text The text to speech
         */
        PEvoid requestTextToSpeech(const std::string& text);

    private:
        /**
         * Changes the current UI to the UI with the given identifier
         * @param identifier The identifier of the ui to change to
         */
        PEvoid changeUI(const std::string& identifier);
        /**
         * Requests the rendering of the UI by the graphic system
         */
        PEvoid requestRendering();



        // Callbacks
        PEvoid onAddUIComponentRequest(PEvoid* data);
        PEvoid onChangeUIRequest(PEvoid* data);
        PEvoid onRenderRequest(PEvoid* data);
        PEvoid onMouseMoved(PEvoid* data);
        PEvoid onMouseButtonDown(PEvoid* data);
        PEvoid onMouseButtonUp(PEvoid* data);
        PEvoid onRegisterClickCallback(PEvoid* data);
        PEvoid onRegisterPostCallback(PEvoid* data);
        PEvoid onChangeUIAttributeRequest(PEvoid* data);
        PEvoid onFocusNextRequest(PEvoid* data);
        PEvoid onFocusPreviousRequest(PEvoid* data);
        PEvoid onPostRequest(PEvoid* data);
    };
}
}

#endif