#ifndef PE_UI_ANCHOR_HPP
#define PE_UI_ANCHOR_HPP

#include <string>
#include <algorithm>

namespace pe
{
namespace ui
{
    /**
     * Anchors for GUI components
     */
    enum class Anchor
    {
        NORTH_WEST,
        NORTH,
        NORTH_EAST,
        EAST,
        SOUTH_EAST,
        SOUTH,
        SOUTH_WEST,
        WEST
    };

    inline Anchor stringToAnchor(const std::string& str)
    {
        std::string anchorString = str;
        std::transform(anchorString.begin(), anchorString.end(), anchorString.begin(), ::toupper);

        if(anchorString == "NORTH_WEST")
            return Anchor::NORTH_WEST;
        if(anchorString == "NORTH")
            return Anchor::NORTH;
        if(anchorString == "NORTH_EAST")
            return Anchor::NORTH_EAST;
        if(anchorString == "EAST")
            return Anchor::EAST;
        if(anchorString == "SOUTH_EAST")
            return Anchor::SOUTH_EAST;
        if(anchorString == "SOUTH")
            return Anchor::SOUTH;
        if(anchorString == "SOUTH_WEST")
            return Anchor::SOUTH_WEST;
        if(anchorString == "WEST")
            return Anchor::WEST;

        return Anchor::NORTH_WEST;
    }
}
}

#endif