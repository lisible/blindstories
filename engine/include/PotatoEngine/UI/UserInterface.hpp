#ifndef PE_UI_USERINTERFACE_HPP
#define PE_UI_USERINTERFACE_HPP

#include "types.hpp"
#include "UI/GUIRoot.hpp"
#include "UI/GUIComponent.hpp"

#include <vector>

namespace pe
{
namespace ui
{
	class GUISystem;

	/**
 	 * Represents a user interface
 	 */
	class UserInterface
	{
		private:
			// The GUI system
			GUISystem* system;

			// The root of the GUI
			GUIRoot root;

			// The list of components of the ui
			std::vector<GUIComponent*> components;
			std::vector<GUIComponent*> dynamicComponents;



		public:
			UserInterface();
			~UserInterface();

			/**
			 * Sets the system of the UI
			 * @param system The system
			 */
			PEvoid setSystem(GUISystem* system);
			/**
			 * Returns the system of the UI
			 * @return The system of the ui
			 */
			GUISystem* getSystem() const;

			/**
 			 * Returns the root of the GUI
 			 * @return The root of the GUI
 			 */
			GUIComponent* getRoot();

			/**
			 * Registers the component into the ui
			 * @param component The component to register
			 */
			PEvoid registerComponent(GUIComponent* component);
			PEvoid registerDynamicComponent(GUIComponent* component);
			/**
			 * Returns the list of components of the ui
			 * @return The list of components of the ui
			 */
			std::vector<GUIComponent*>& getComponentList();

			/**
			 * Clears the dynamically added components of the ui
			 */
			PEvoid clearDynamicComponents();
			/**
			 * Clears the UI
			 */
			PEvoid clear();
	};
}
}

#endif
