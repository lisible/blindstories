#ifndef PE_UI_GUIFRAME_HPP
#define PE_UI_GUIFRAME_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"
#include "Graphics/Color.hpp"

namespace pe
{
namespace ui
{
	class GUIFrame : public GUIComponent
	{
		private:
			// Color of the frame
			pe::graphics::Color color;
		public:
			GUIFrame(const std::string& identifier);
			virtual ~GUIFrame();

			/**
 			 * Renders the frame using the given renderer
 			 * @param renderer The renderer
 			 */
			PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& tranform) override;
			
			PEvoid setColor(const pe::graphics::Color& color);
			const pe::graphics::Color& getColor() const;
	};
}
}

#endif
