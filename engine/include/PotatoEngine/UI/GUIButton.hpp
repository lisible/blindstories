#ifndef PE_UI_GUIBUTTON_HPP
#define PE_UI_GUIBUTTON_HPP

#include "types.hpp"
#include "UI/GUIComponent.hpp"
#include "UI/GUILabel.hpp"
#include "UI/GUIFrame.hpp"
#include "Graphics/Color.hpp"



namespace pe
{
namespace ui
{
    /**
     * Class for buttons
     */
    class GUIButton : public GUIComponent
    {
    private:
        // The label of the button
        GUILabel buttonLabel;
        // The frame of the button
        GUIFrame buttonFrame;

        // The default color of the button
        pe::graphics::Color defaultColor;
        // The color of the button when the cursor is over the button
        pe::graphics::Color hoverColor;
        // The color of the button when the button is pressed
        pe::graphics::Color downColor;

        PEvoid updateLabelPosition();
    public:
        GUIButton(const std::string& identifier, const std::string& font);
        ~GUIButton() override;

        PEvoid setText(const std::string& text);
        PEvoid setSize(PEfloat width, PEfloat height) override;

        /**
         * Sets the default color of the button
         * @param color The default color of the button
         */
        PEvoid setDefaultColor(const pe::graphics::Color& color);
        /**
         * Sets the hover color of the button
         * @param color The default color of the button
         */
        PEvoid setHoverColor(const pe::graphics::Color& color);
        /**
         * Sets the down color of the button
         * @param color The default color of the button
         */
        PEvoid setDownColor(const pe::graphics::Color& color);

        /**
         * Renders the button using the given renderer
         * @param renderer The renderer
         */
        PEvoid render(pe::graphics::Renderer2D& renderer, const pem::mat4& tranform) override;

        virtual PEvoid onHover() override;
        virtual PEvoid onHoverExit() override;
        virtual PEvoid onPress() override;
        virtual PEvoid onPressExit() override;
        virtual PEvoid onFocus() override;
        virtual PEvoid onFocusExit() override;
    };
}
}

#endif