#ifndef PE_UI_GUICOMPONENT_HPP
#define PE_UI_GUICOMPONENT_HPP

#include "types.hpp"
#include "UI/GUILayoutType.hpp"
#include "UI/Anchor.hpp"
#include "Graphics/Transformable.hpp"

#include <map>

namespace pe
{
namespace graphics
{
	class Renderer2D;
}
namespace ui
{
	class UserInterface;

	/**
 	 * Base class for GUI Components
 	 */
	class GUIComponent : public pe::graphics::Transformable
	{
		private:
			// The identifier of the component
			std::string identifier;
			// The ui of the component
			UserInterface* ui;
			// The size of the component
			pem::vec2 size;
			// The anchor of the component
			Anchor anchor;

			// The children of the component
			std::map<std::string, GUIComponent*> children;

			// The parent of the component
			GUIComponent* parent;
			// The next component
			std::string nextComponent;
			// The previous component
			std::string previousComponent;

			// TTS Speech on focus
			std::string ttsOnFocus;



			// Flag to know whether or not the component has focus
			PEbool hasFocus;
			// Flag to know whether or not the component is hovered
			PEbool hovered;
			// Flag to know whether or not the component is being clicked
			PEbool pressed;


			pem::mat4 getCombinedInvTransformMatrix();

		public:
			GUIComponent(const std::string& identifier);
			virtual ~GUIComponent() override;

			/**
			 * Sets the UI of the component
			 * @param ui The ui of the component
			 */
			PEvoid setUI(UserInterface* ui);
			/**
			 * Returns the UI of the component
			 * @return The ui of the component
			 */
			UserInterface* getUI() const;

			/**
 			 * Adds a child component to the component
 			 * @param child The child to add
 			 */
			PEvoid addChild(GUIComponent* child);
			/**
 			 * Returns the successor with the corresponding identifier
 			 * @param identifier The identifier of the successor to look for
 			 * @return The corresponding successor or nullptr if it doesn't exist
 			 */	
			GUIComponent* getSuccessor(const std::string& identifier) const;

			/**
 			 * Sets the parent of the component
 			 * @param parent The parent of the component
 			 */
			PEvoid setParent(GUIComponent* parent);
			/**
 			 * Returns the parent of the component
 			 * @return The parent of the component (or nullptr)
 			 */
			GUIComponent* getParent() const;
			
			/**
 			 * Clears (by deleting them) all the successors of the component
 			 */
			PEvoid clearSuccessors();

			/**
 			 * Returns the identifier of the component
 			 * @return The identifier of the component
 			 */
			const std::string& getIdentifier() const;


			/**
 			 * Returns the children of the component
 			 * @return The children of the component
 			 */
			std::map<std::string, GUIComponent*>& getChildren();

			/**
			 * Removes a child of the component
			 * @param childIdentifier The identifier of the child to remove
			 */
			PEvoid removeChild(const std::string& childIdentifier);

			/**
 			 * Sets the size of the component
 			 * @param width The width of the component
 			 * @param height The height of the component
 			 */
			virtual PEvoid setSize(PEfloat width, PEfloat height);
			/**
 			 * Returns the size of the component
 			 * @return The size of the component
 			 */
			const pem::vec2& getSize() const;

			/**
			 * Sets the anchor of the component
			 * @param anchor The anchor of the component
			 */
			PEvoid setAnchor(Anchor anchor);
			/**
			 * Returns the anchor of the component
			 * @return The anchor of the component
			 */
			Anchor getAnchor() const;

			/**
			 * Sets the next component in the GUI
			 * @param next The next component in the GUI
			 */
			PEvoid setNextComponent(const std::string& next);
			/**
			 * Returns the next component in the GUI
			 * @return The next component
			 */
			const std::string& getNextComponent() const;

			/**
			 * Sets the previous component in the GUI
			 * @param previous The previous component in the GUI
			 */
			PEvoid setPreviousComponent(const std::string& previous);
			/**
			 * Returns the previous component in the GUI
			 * @return The previous component
			 */
			const std::string& getPreviousComponent() const;

			/**
			 * Sets the TTS speech on focus
			 * @param ttsOnFocus The tts speech
			 */
			PEvoid setTTSOnFocus(const std::string& ttsOnFocus);
			/**
			 * Returns the TTS speech on focus
			 * @return The TTS speech on focus
			 */
			const std::string& getTTSOnFocus() const;

			/**
			 * Returns true if the given coordinates are in bounds after transformation
			 * @param x The horizontal coordinate
			 * @param y The vertical coordinate
			 * @return true if the given coordinates are in bounds after transformation
			 */
			PEbool isInBounds(PEfloat x, PEfloat y);

			/**
			 * Changes an attribute of the component
			 * @param attribute The attribute to change
			 * @param value The new value of the attribute
			 */
			virtual PEvoid changeAttribute(const std::string& attribute, const std::string& value);
			
		
			/**
 			 * Renders the component using the given renderer
 			 * @param renderer
 			 */
			virtual PEvoid render(pe::graphics::Renderer2D& renderer,
								  const pem::mat4& transform) = 0;

			/**
			 * Returns true if the component has focus
			 * @return true if the component has focus
			 */
			PEbool isFocused() const;
			/**
			 * Give the focus to the component
			 */
			virtual PEvoid onFocus();
			/**
			 * Unfocuses the component
			 */
			virtual PEvoid onFocusExit();

			/**
			 * Returns true if the component is hovered
			 * @return true if the component is hovered
			 */
			PEbool isHovered() const;
			/**
			 * Returns true if the component is pressed
			 * @return true if the component is pressed
			 */
			PEbool isPressed() const;

			/**
			 * Resets the state of the component
			 */
			PEvoid resetState();

			virtual PEvoid onHover();
			virtual PEvoid onHoverExit();
			virtual PEvoid onPress();
			virtual PEvoid onPressExit();
	};
}
}

#endif
