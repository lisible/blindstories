#ifndef PE_MESSAGING_MESSAGEBUS_HPP
#define PE_MESSAGING_MESSAGEBUS_HPP

#include "types.hpp"
#include "Messaging/Message.hpp"

#include <functional>
#include <queue>
#include <list>

namespace pe 
{
namespace messaging
{
	class Messenger;	
	
	/**
 	 * Bus used to send messages across registered messengers
 	 */	
	class MessageBus 
	{
		private:
			// The message queue
			std::queue<Message> messageQueue;
			// The list of registered messengers
			std::list<Messenger*> messengers;
		public:
			MessageBus();	
			~MessageBus();

			/**
 			 * Registers a messenger to the message bus
 			 * @param messenger The messenger to register
 			 */
			PEvoid registerMessenger(Messenger* messenger);
		
			/**
 			 * Sends a message on the bus
 			 * @param message The message to send
 			 */
			PEvoid sendMessage(Message& message);
			/**
 			 * Sends a direct message, a direct message can be handled by only one
 			 * messenger, it returns a generic pointer
 			 * @param message The message to send
 			 * @return The response data, nullptr in case of error
 			 */
			PEvoid* sendDirectMessage(Message& message);
			
			/**
 			 * Publishes the messages of the queue
 			 */	
			PEvoid publishMessages();

	};
}
}

#endif
