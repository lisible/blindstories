#ifndef PE_MESSAGING_MESSAGE_HPP
#define PE_MESSAGING_MESSAGE_HPP

#include "types.hpp"
#include <functional>

namespace pe
{
namespace messaging
{
	/**
 	 * Message sent through a message bus
 	 */
	class Message
	{
		private:
			// The identifier of the message	
			PEMsgId identifier;
			// The data of the message
			PEvoid* data;
			// The function to run after the message is sent
			std::function<PEvoid(PEvoid*)> sentCallback;

		public:
			/**
 			 * Creates a message
 			 * @param identifier The identifier of the message
 			 * @param data The data of the message
 			 * @param sentCallback The method to run after sending the message
 			 */
			Message(PEMsgId identifier,
					PEvoid* data = nullptr, 
					std::function<PEvoid(PEvoid*)> sentCallback = std::function<PEvoid(PEvoid*)>());
			~Message();

			/**
 			 * Returns the identifier of the message
 			 * @return The identifier of the message
 			 */
			PEMsgId getIdentifier() const;
			/**
 			 * Returns the data of the message
 			 * @return THe data of the message
 			 */
			PEvoid* getData();
			/**
 			 * Runs the sent callback of the message
 			 */
			PEvoid dispose();

	};
}
}

#endif
