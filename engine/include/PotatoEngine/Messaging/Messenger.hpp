#ifndef PE_MESSAGING_MESSENGER_HPP
#define PE_MESSAGING_MESSENGER_HPP

#include "types.hpp"
#include "Messaging/MessageBus.hpp"
#include "Messaging/Message.hpp"

#include <unordered_map>

namespace pe
{
namespace messaging
{
	/**
 	 * Base class for messengers
 	 */	
	class Messenger
	{
		private:
			// The message bus used by the messenger	
			MessageBus* messageBus;
			// The callbacks to messages
			std::unordered_map<PEMsgId, std::function<PEvoid(PEvoid*)>> callbacks;
			std::unordered_map<PEMsgId, std::function<PEvoid*(PEvoid*)>> directCallbacks;
		public:
			Messenger();
			virtual ~Messenger();

			/**
 			 * Sends a message on the bus
 			 * @param message The message to send 
 			 */
			PEvoid sendMessage(Message message);
			/**
 			 * Sends a direct message on the bus
 			 * @param message The message to send
 			 * @return The response
 			 */	
			PEvoid* sendDirectMessage(Message message);
	
			/**
 			 * Sets the message bus of the messenger
 			 * @param bus The message bus
 			 */	
			PEvoid setMessageBus(MessageBus& bus);	

			/**
 			 * Registers a new callback method for the messenger
 			 * @param identifier The message to react to
 			 * @param callbackMethod Method to react to the message
 			 */
			PEvoid registerCallback(PEMsgId identifier, 
									std::function<PEvoid(PEvoid*)> callbackMethod);
			/**
 			 * Registers a new direct callback method for the messenger
 			 * @param identifier The message to react to
 			 * @param callbackMethod Method to react to the message
 			 */
			PEvoid registerDirectCallback(PEMsgId identifier,
									std::function<PEvoid*(PEvoid*)> callbackMethod);
			
			/**
 			 * Handles a message
 			 * @param message The message to handle
 			 */
			PEvoid handleMessage(pe::messaging::Message& message);

			/**
 			 * Handles a direct message
 			 * @param message The message to handle
 			 * @return The response data, nullptr if it cannot handle the message
 			 */
			PEvoid* handleDirectMessage(pe::messaging::Message& message);

			/**
 			 * Returns true if the messenger can handle a message with the given 
 			 * identifier, used for direct messages
 			 * @param messageIdentifier The identifier to test
 			 * @return true if the messenger can handle the message
 			 */
			PEbool canHandleDirectMessage(PEMsgId messageIdentifier) const;
	};
}
}
#endif
