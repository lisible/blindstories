#ifndef PE_MESSAGING_MESSAGING_HPP
#define PE_MESSAGING_MESSAGING_HPP

#include "Messaging/Message.hpp"
#include "Messaging/MessageBus.hpp"
#include "Messaging/Messages.hpp"
#include "Messaging/Messenger.hpp"
#include "SimpleMessageData.hpp"

namespace pemsg = pe::messaging;

#endif
