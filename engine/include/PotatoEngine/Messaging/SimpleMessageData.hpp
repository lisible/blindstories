#ifndef PE_MESSAGING_SIMPLEMESSAGEDATA_HPP
#define PE_MESSAGING_SIMPLEMESSAGEDATA_HPP

#include "types.hpp"

#include <string>

namespace pe
{
namespace messaging
{
	/**
 	 * struct to send simple message data
 	 */
	struct SimpleMessageData
	{
		PEuint32 integerValue1;
		PEuint32 integerValue2;
		PEuint32 integerValue3;
		std::string stringValue1;
		std::string stringValue2;
		std::string stringValue3;		
	};
}
}
#endif
