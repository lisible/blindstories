#ifndef PE_WINDOWING_SDLWINDOW_HPP
#define PE_WINDOWING_SDLWINDOW_HPP

#include "types.hpp"
#include <Windowing/IWindow.hpp>

#include <SDL2/SDL.h>

namespace pe
{
namespace windowing
{
	/**
 	 * SDL implementation for IWindow
 	 */
	class SDLWindow : public pe::windowing::IWindow
	{
		private:
			// Internal SDL_Window	
			SDL_Window* sdlWindow;
			// OpenGL context
			SDL_GLContext oglContext;

			// Window title cache variable
			std::string windowTitle;
			// Window size cache variables
			PEuint32 windowWidth;
			PEuint32 windowHeight;

		public:
			SDLWindow();
			~SDLWindow() override;

			/**
 			 * Creates the window
 			 * @param title The title of the window
 			 * @param width The width of the window
 			 * @param height The height of the window
 			 */
			virtual PEvoid create(const std::string& title, PEuint32 width, PEuint32 height) override;

			/**
 			 * Shows the window
 			 */
			virtual PEvoid show() override;
			/**
 			 * Hides the window
 			 */
			virtual PEvoid hide() override;

			/**
 			 * Renders the window content
 			 */
			virtual PEvoid render() override;
			/**
 			 * Clears the window content
 			 */
			virtual PEvoid clear() override;
			
			/**
 			 * Sets the title of the window
 			 * @param title The title of the window
 			 */
			virtual PEvoid setTitle(const std::string& title) override;
			/**
 			 * Returns the title of the window
 			 * @return The title of the window
 			 */	
			virtual const std::string& getTitle() const override;

			/**
 			 * Sets the width of the window
 			 * @param width The new width of the window
 			 */
			virtual PEvoid setWidth(PEuint32 width) override;
			/**
 			 * Returns the width of the window
 			 * @return The width of the window
 			 */
			virtual PEuint32 getWidth() const override;

			/**
 			 * Sets the height of the window
 			 * @param height The height of the window
 			 */
			virtual PEvoid setHeight(PEuint32 height) override;
			/**
 			 * Returns the height of the window
 			 * @return The height of the window
 			 */
			virtual PEuint32 getHeight() const override;
		
			/**
 			 * Polls events
 			 * @param event The event to poll
 			 */	
			virtual PEbool pollEvent(pe::input::InputEvent* event) override;
	};
}
}

#endif
