#ifndef PE_WINDOWING_WINDOWINGSYSTEM_HPP
#define PE_WINDOWING_WINDOWINGSYSTEM_HPP

#include "types.hpp"
#include "System/System.hpp"
#include "Windowing/IWindow.hpp"

namespace pe
{
namespace windowing
{
	/**
 	 * Base class for window systems
 	 */
	class WindowingSystem : public pe::system::System
	{
		private:
			// The window of the windowing system
			IWindow* window;
		protected:
			virtual PEvoid registerDefaultHandlers() override;
		
			/**
 			 * Creates the window
 			 * @param title The title of the window
 			 * @param width The width of the window
 			 * @param height The height of the window
 			 */	
			PEvoid createWindow(const std::string& title, PEuint32 width, PEuint32 height);
			/**
 			 * Shows the window
 			 */	
			PEvoid showWindow();
			/**
 			 * Hides the window
 			 */
			PEvoid hideWindow();
			/**
 			 * Displays window content
 			 */
			PEvoid displayWindow();
			/**
 			 * Clears window content
 			 */
			PEvoid clearWindow();
		public:
			WindowingSystem(IWindow* window);
			virtual ~WindowingSystem() override;

			/**
 			 * Initializes the windowing system
 			 */
			virtual PEvoid init() = 0;

			/**
 			 * Shuts down the windowing system
 			 */
			virtual PEvoid shutdown() = 0;

		protected:
			// Handlers
			virtual PEvoid onWindowCreationRequest(PEvoid* data);
			virtual PEvoid onWindowShowRequest(PEvoid* data);
			virtual PEvoid onWindowHideRequest(PEvoid* data);
			virtual PEvoid onWindowClearRequest(PEvoid* data);
			virtual PEvoid onWindowDisplayRequest(PEvoid* data);
			virtual PEvoid onUpdateRequest(PEvoid* data);

	};
}
}

#endif
