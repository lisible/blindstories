#ifndef PE_WINDOWING_IWINDOW_HPP
#define PE_WINDOWING_IWINDOW_HPP

#include "types.hpp"
#include "Input/InputEvent.hpp"

#include <string>

namespace pe
{
namespace windowing
{
	/**
 	 * Interface for windows
 	 */
	class IWindow
	{
		public:
			virtual ~IWindow(){}

			/**
 			 * Creates the window
 			 * @param title The title of the window
 			 * @param width The width of the window
 			 * @param height The height of the window
 			 */
			virtual PEvoid create(const std::string& title, PEuint32 width, PEuint32 height) = 0;

			/**
 			 * Shows the window
 			 */
			virtual PEvoid show() = 0;
			/**
 			 * Hides the window
 			 */
			virtual PEvoid hide() = 0;

			/**
 			 * Renders the window content
 			 */
			virtual PEvoid render() = 0;
			/**
 			 * Clears the window content
 			 */
			virtual PEvoid clear() = 0;
			
			/**
 			 * Sets the title of the window
 			 * @param title The title of the window
 			 */
			virtual PEvoid setTitle(const std::string& title) = 0;
			/**
 			 * Returns the title of the window
 			 * @return The title of the window
 			 */	
			virtual const std::string& getTitle() const = 0;

			/**
 			 * Sets the width of the window
 			 * @param width The new width of the window
 			 */
			virtual PEvoid setWidth(PEuint32 width) = 0;
			/**
 			 * Returns the width of the window
 			 * @return The width of the window
 			 */
			virtual PEuint32 getWidth() const = 0;

			/**
 			 * Sets the height of the window
 			 * @param height The height of the window
 			 */
			virtual PEvoid setHeight(PEuint32 height) = 0;
			/**
 			 * Returns the height of the window
 			 * @return The height of the window
 			 */
			virtual PEuint32 getHeight() const = 0;

			/**
 			 * Polls events
 			 * @param event The event to poll
 			 */
			virtual PEbool pollEvent(pe::input::InputEvent* event) = 0;
	};
}
}

#endif
