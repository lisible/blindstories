#ifndef PE_WINDOWING_SDLWINDOWINGSYSTEM_HPP
#define PE_WINDOWING_SDLWINDOWINGSYSTEM_HPP

#include "types.hpp"
#include "Windowing/WindowingSystem.hpp"
#include "Windowing/SDLWindow.hpp"

namespace pe
{
namespace windowing
{
	/**
 	 * SDL implementation of WindowingSystem
 	 */
	class SDLWindowingSystem : public pe::windowing::WindowingSystem
	{
		public:
			SDLWindowingSystem();
			~SDLWindowingSystem() override;

			/**
 			 * Initializes the windowing system
 			 */
			PEvoid init() override;

			/**
 			 * Shuts down the windowing system
 			 */
			PEvoid shutdown() override;

	};
}
}

#endif
