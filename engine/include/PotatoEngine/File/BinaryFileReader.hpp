#ifndef PE_FILE_BINARYFILE_HPP
#define PE_FILE_BINARYFILE_HPP

#include "types.hpp"
#include "File/File.hpp"

#include <vector>
#include <fstream>

namespace pe
{
namespace file
{
	/**
 	 * Class used to read binary file
 	 */
	class BinaryFileReader
	{
		private:
			// The stream used to read the file
			std::ifstream stream;

		public:
			BinaryFileReader();
			~BinaryFileReader();

			/**
 			 * Opens the file to read
 			 * @param file The file to read
 			 */
			PEvoid openFile(File& file);
			/**
 			 * Closes the file
 			 */
			PEvoid closeFile();
	
			/**
 			 * Reads bytes in a file
 			 * @param length The number of bytes to read
 			 * @return The read bytes
 			 */
			std::vector<char> readBytes(PEuint64 length);	
			/**
 			 * Reads a full file
 			 * @return The read bytes
 			 */
			std::vector<char> readFile();
	};
}
}

#endif
