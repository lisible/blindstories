#ifndef PE_FILE_TEXTFILEWRITER_HPP
#define PE_FILE_TEXTFILEWRITER_HPP

#include "types.hpp"

#include <File/File.hpp>

#include <fstream>

namespace pe
{
namespace file
{
	/**
 	 * Class used to write text to file
 	 */
	class TextFileWriter
	{
		private:
			// The file stream used
			std::ofstream stream;	

		public:
			TextFileWriter();
			~TextFileWriter();
			
			/**
 			 * Opens a file
 			 * @param file The file to open
 			 */
			PEvoid openFile(const File& file);
			/**
 			 * Closes a file
 			 */
			PEvoid closeFile();

			/**
 			 * Writes a string into a file
 			 * @param string The string to write
 			 */
			PEvoid write(const std::string& string);
			/**
 			 * Writes a line into a file
 			 * @param line The line to write
 			 */
			PEvoid writeLine(const std::string& line);	

	};
}
}

#endif
