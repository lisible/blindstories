#ifndef PE_FILE_FILE_HPP
#define PE_FILE_FILE_HPP

#include "types.hpp"

#include <string>

namespace pe
{
namespace file
{
	/**
 	 * Represents a file in the filesystem
 	 */
	class File
	{
		private:
			// The path of the file as a string
			std::string pathString;
		public:
			// Don't need the default constructor
			File() = delete;

			/**
 			 * Creates a file with the given path
 			 * @param pathString The path of the file as a string
 			 */
			File(const std::string& pathString);
			~File();

			/**
 			 * Returns the path of the file
 			 */
			const std::string& getPath() const;
	};
}
}

#endif
