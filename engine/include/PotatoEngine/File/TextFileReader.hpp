#ifndef PE_FILE_TEXTFILEREADER_HPP
#define PE_FILE_TEXTFILEREADER_HPP

#include "types.hpp"

#include "File/File.hpp"


#include <string>
#include <fstream>

namespace pe
{
namespace file
{
	/**
 	 * Class used to read text files
 	 */
	class TextFileReader
	{
		private:
			// The stream used to read the file
			std::ifstream stream;

		public:
			TextFileReader();
			~TextFileReader();

			/**
 			 * Opens the given file
 			 * @param file The file to write into
 			 */
			PEvoid openFile(File& file);
			/**
 			 * Closes the file
 			 */
			PEvoid closeFile();

			/**
 			 * Reads a string from the file
 			 * @param length The length of the string to read
 			 * @return The read string
 			 */
			std::string readString(PEuint64 length);
			/**
 			 * Reads a line from the file
 			 * @return The read line
 			 */
			PEsize_t readLine(std::string& str);
			/**
 			 * Reads an entire file into a string
 			 * @return The file as a string
 			 */
			std::string readFile();
	};

}
}

#endif
