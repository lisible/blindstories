#ifndef PE_FILE_RESOURCEFILEREADER_HPP
#define PE_FILE_RESOURCEFILEREADER_HPP

#include "types.hpp"
#include "File/File.hpp"
#include "Resources/ResourceDescription.hpp"

#include "File/TextFileReader.hpp"

namespace pe
{
namespace file
{
	/**
 	 * Used to read resource description files
 	 */
	class ResourceFileReader
	{
		private:
			TextFileReader fileReader;

			PEvoid readHeader(pe::resources::ResourceDescription& resourceDescription);
			PEvoid readParameters(pe::resources::ResourceDescription& resourceDescription);
		public:
			ResourceFileReader();
			~ResourceFileReader();

			PEvoid openFile(File& file);
			PEvoid closeFile();

			pe::resources::ResourceDescription readResourceDescription();
	};
}
}

#endif
