#ifndef PE_MATHS_MATHS_HPP
#define PE_MATHS_MATHS_HPP

#include "Maths/Base.hpp"

#include "Maths/Vector2.hpp"
#include "Maths/Vector3.hpp"
#include "Maths/Vector4.hpp"
#include "Maths/Matrix4.hpp"

#include "Maths/Rectangle.hpp"

namespace pem = pe::maths;

#endif
