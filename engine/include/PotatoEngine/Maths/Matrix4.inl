/**
 * Sets the values of a matrix
 * @param valuesArray The values of the matrix
 */
template <typename T>
PEvoid Matrix4<T>::setValues(T* valuesArray)
{
	for(PEsize_t i = 0; i < 4*4; i++)
		values[i] = valuesArray[i];
}

/**
 * Fills a matrix with a value
 * @param value The value to fill the matrix with
 */
template <typename T>
PEvoid Matrix4<T>::fill(T value)
{
	for(PEsize_t i = 0; i < 4*4; i++)
		values[i] = value;
}

template <typename T>
Matrix4<T> Matrix4<T>::translationMatrix(const Vector3<T>& translation)
{
	return translationMatrix(translation.x, translation.y, translation.z);
}

template <typename T>
Matrix4<T> Matrix4<T>::translationMatrix(T x, T y, T z)
{
	Matrix4<T> translationMatrix(1);
	translationMatrix.set(3, 0, x);
	translationMatrix.set(3, 1, y);
	translationMatrix.set(3, 2, z);

	return translationMatrix;
}

template <typename T>
Matrix4<T> Matrix4<T>::rotationMatrix(PEfloat angleDegrees, const Vector3<T>& axis,
									const Vector3<T>& center)
{
	// @TODO fix this
	PEfloat angleRadians = angleDegrees*(3.14159265358979323846/180);
	Matrix4<T> rotationMatrixX(1), rotationMatrixY(1), rotationMatrixZ(1);
	if (axis.x != 0)
	{
		rotationMatrixX.set(1, 1, std::cos(angleRadians));
		rotationMatrixX.set(2, 1, std::sin(angleRadians));
		rotationMatrixX.set(1, 2, -std::sin(angleRadians));
		rotationMatrixX.set(2, 2, std::cos(angleRadians));
	}

	if (axis.y != 0)
	{
		rotationMatrixY.set(0, 0, std::cos(angleRadians));
		rotationMatrixY.set(0, 2, std::sin(angleRadians));
		rotationMatrixY.set(2, 0, -std::sin(angleRadians));
		rotationMatrixY.set(2, 2, std::cos(angleRadians));
	}

	if (axis.z != 0)
	{
		rotationMatrixZ.set(0, 0, std::cos(angleRadians));
		rotationMatrixZ.set(0, 1, std::sin(angleRadians));
		rotationMatrixZ.set(1, 0, -std::sin(angleRadians));
		rotationMatrixZ.set(1, 1, std::cos(angleRadians));
	}


	return translationMatrix(-center) * rotationMatrixX * rotationMatrixY * rotationMatrixZ * translationMatrix(center);	
}

template <typename T>
Matrix4<T> Matrix4<T>::scaleMatrix(const Vector3<T>& scale)
{
	return scaleMatrix(scale.x, scale.y, scale.z);
}

template <typename T>
Matrix4<T> Matrix4<T>::scaleMatrix(T scaleX, T scaleY, T scaleZ)
{
	Matrix4<T> scaleMatrix(1);
	scaleMatrix.set(0, 0, scaleX);
	scaleMatrix.set(1, 1, scaleY);
	scaleMatrix.set(2, 2, scaleZ);

	return scaleMatrix;
}

/**
 * Multiplies the matrix with the one passed in parameter
 * @param m The matrix to multiply with
 */
template <typename T>
void Matrix4<T>::multiply(const Matrix4<T>& m)
{
	Matrix4 res = multiply(*this, m);
	m.setValues(res.value_ptr());
}
/**
 * Multiplies a matrix with another
 * @param m1 The first matrix
 * @param m2 The second matrix
 * @return The matrix m1*m2
 */

template <typename T>
Matrix4<T> Matrix4<T>::multiply(const Matrix4<T>& m1, const Matrix4<T>& m2)
{
	Matrix4<T> res;

	for (PEsize_t i = 0; i < 4; i++)
	{
		for (PEsize_t j = 0; j < 4; j++)
		{
			T sum(0);
			for (PEsize_t k = 0; k < 4; k++)
			{
				sum += m1.values[k + j * 4] * m2.values[i + k * 4];
			}
			res.values[i + j * 4] = sum;
		}
	}

	return res;
}
/**
 * Multiplies the current matrix with a constant
 * @param a The constant to multiply with
 */
template <typename T>
void Matrix4<T>::multiply(T a)
{
	for(PEsize_t i = 0; i < 4*4; i++)
	{
		values[i] = a*values[i];
	}
}

template<typename T>
Vector4<T> Matrix4<T>::multiply(const Matrix4<T>& m, const Vector4<T>& v)
{
	Vector4<T> res;
	
	res.x = m.get(0, 0)*v.x + m.get(1, 0)*v.y + m.get(2,0)*v.z + m.get(3,0)*v.w;
	res.y = m.get(0, 1)*v.x + m.get(1, 1)*v.y + m.get(2,1)*v.z + m.get(3,1)*v.w;
	res.z = m.get(0, 2)*v.x + m.get(1, 2)*v.y + m.get(2,2)*v.z + m.get(3,2)*v.w;
	res.w = m.get(0, 3)*v.x + m.get(1, 3)*v.y + m.get(2,3)*v.z + m.get(3,3)*v.w;

	return res;
}

template<typename T>
Matrix4<T> Matrix4<T>::invert() 
{
	Matrix4 invOut;
	double inv[16], det;
	int i;

	inv[0] = values[5] * values[10] * values[15] -
		values[5] * values[11] * values[14] -
		values[9] * values[6] * values[15] +
		values[9] * values[7] * values[14] +
		values[13] * values[6] * values[11] -
		values[13] * values[7] * values[10];

	inv[4] = -values[4] * values[10] * values[15] +
		values[4] * values[11] * values[14] +
		values[8] * values[6] * values[15] -
		values[8] * values[7] * values[14] -
		values[12] * values[6] * values[11] +
		values[12] * values[7] * values[10];

	inv[8] = values[4] * values[9] * values[15] -
		values[4] * values[11] * values[13] -
		values[8] * values[5] * values[15] +
		values[8] * values[7] * values[13] +
		values[12] * values[5] * values[11] -
		values[12] * values[7] * values[9];

	inv[12] = -values[4] * values[9] * values[14] +
		values[4] * values[10] * values[13] +
		values[8] * values[5] * values[14] -
		values[8] * values[6] * values[13] -
		values[12] * values[5] * values[10] +
		values[12] * values[6] * values[9];

	inv[1] = -values[1] * values[10] * values[15] +
		values[1] * values[11] * values[14] +
		values[9] * values[2] * values[15] -
		values[9] * values[3] * values[14] -
		values[13] * values[2] * values[11] +
		values[13] * values[3] * values[10];

	inv[5] = values[0] * values[10] * values[15] -
		values[0] * values[11] * values[14] -
		values[8] * values[2] * values[15] +
		values[8] * values[3] * values[14] +
		values[12] * values[2] * values[11] -
		values[12] * values[3] * values[10];

	inv[9] = -values[0] * values[9] * values[15] +
		values[0] * values[11] * values[13] +
		values[8] * values[1] * values[15] -
		values[8] * values[3] * values[13] -
		values[12] * values[1] * values[11] +
		values[12] * values[3] * values[9];

	inv[13] = values[0] * values[9] * values[14] -
		values[0] * values[10] * values[13] -
		values[8] * values[1] * values[14] +
		values[8] * values[2] * values[13] +
		values[12] * values[1] * values[10] -
		values[12] * values[2] * values[9];

	inv[2] = values[1] * values[6] * values[15] -
		values[1] * values[7] * values[14] -
		values[5] * values[2] * values[15] +
		values[5] * values[3] * values[14] +
		values[13] * values[2] * values[7] -
		values[13] * values[3] * values[6];

	inv[6] = -values[0] * values[6] * values[15] +
		values[0] * values[7] * values[14] +
		values[4] * values[2] * values[15] -
		values[4] * values[3] * values[14] -
		values[12] * values[2] * values[7] +
		values[12] * values[3] * values[6];

	inv[10] = values[0] * values[5] * values[15] -
		values[0] * values[7] * values[13] -
		values[4] * values[1] * values[15] +
		values[4] * values[3] * values[13] +
		values[12] * values[1] * values[7] -
		values[12] * values[3] * values[5];

	inv[14] = -values[0] * values[5] * values[14] +
		values[0] * values[6] * values[13] +
		values[4] * values[1] * values[14] -
		values[4] * values[2] * values[13] -
		values[12] * values[1] * values[6] +
		values[12] * values[2] * values[5];

	inv[3] = -values[1] * values[6] * values[11] +
		values[1] * values[7] * values[10] +
		values[5] * values[2] * values[11] -
		values[5] * values[3] * values[10] -
		values[9] * values[2] * values[7] +
		values[9] * values[3] * values[6];

	inv[7] = values[0] * values[6] * values[11] -
		values[0] * values[7] * values[10] -
		values[4] * values[2] * values[11] +
		values[4] * values[3] * values[10] +
		values[8] * values[2] * values[7] -
		values[8] * values[3] * values[6];

	inv[11] = -values[0] * values[5] * values[11] +
		values[0] * values[7] * values[9] +
		values[4] * values[1] * values[11] -
		values[4] * values[3] * values[9] -
		values[8] * values[1] * values[7] +
		values[8] * values[3] * values[5];

	inv[15] = values[0] * values[5] * values[10] -
		values[0] * values[6] * values[9] -
		values[4] * values[1] * values[10] +
		values[4] * values[2] * values[9] +
		values[8] * values[1] * values[6] -
		values[8] * values[2] * values[5];

	det = values[0] * inv[0] + values[1] * inv[4] + values[2] * inv[8] + values[3] * inv[12];

	if (det == 0)
		return identity();

	det = 1.0 / det;

	for (i = 0; i < 16; i++)
		invOut.values[i] = inv[i] * det;

	return invOut;
}
/**
 * Multiplies a matrix with a constant
 * @param m The matrix to multiply
 * @param a The constant to multiply the matrix with
 * @return The matrix m*a
 */
template <typename T>
Matrix4<T> Matrix4<T>::multiply(const Matrix4<T>& m, T a)
{
	Matrix4<T> res;

	for(PEsize_t i = 0; i < 4*4; i++)
	{
		res.values[i] = m.values[i] * a;
	}

	return res;
}

/**
 * Divides the current matrix with a constant
 * @param a The constant to divide with
 */
template <typename T>
void Matrix4<T>::divide(T a)
{
	Matrix4<T>::divide(*this, a);
}

/**
 * Divides a matrix with a constant
 * @param m The matrix to divide
 * @param a The constant to divide the matrix with
 * @return The matrix m/a
 */
template <typename T>
Matrix4<T> Matrix4<T>::divide(const Matrix4<T>& m, T a)
{
	Matrix4<T> res;

	for(PEsize_t i = 0; i < 4*4; i++)
		res.values[i] = m.values[i] / a;

	return res;
}

/**
 * Adds a matrix to the current matrix
 * @param m The matrix to add
 */
template <typename T>
void Matrix4<T>::add(const Matrix4<T>& m)
{
	for(PEsize_t i = 0; i < 4*4; i++)
	{
		values[i] = values[i] + m.values[i];
	}
}
/**
 * Returns the addition of two matrices
 * @param m1 The first matrix
 * @param m2 The second matrix
 * @return The matrix m1 + m2
 */
template <typename T>
Matrix4<T> Matrix4<T>::add(const Matrix4<T>& m1, const Matrix4<T>& m2)
{
	Matrix4<T> res;

	for(PEsize_t i = 0; i < 4*4; i++)
	{
		res.values[i] = m1.values[i] + m2.values[i];
	}

	return res;
}

/**
 * Subtract a matrix from the current matrix
 * @param m The matrix to subtract
 */
template <typename T>
void Matrix4<T>::subtract(const Matrix4<T>& m)
{
	for(PEsize_t i = 0; i < 4*4; i++)
	{
		values[i] = values[i] - m.values[i];
	}
}
/**
 * Returns the subtraction of two matrices
 * @param m1 The first matrix
 * @param m2 The second matrix
 * @return The matrix m1 - m2
 */
template <typename T>
Matrix4<T> Matrix4<T>::subtract(const Matrix4<T>& m1, const Matrix4<T>& m2)
{
	Matrix4<T> res;

	for(PEsize_t i = 0; i < 4*4; i++)
	{
		res.values[i] = m1.values[i] - m2.values[i];
	}

	return res;
}

/**
 * Sets a value of the matrix
 * @param x The x coordinate of the value (left is 0)
 * @param y The y coordinate of the value (top is 0)
 * @param value The value
 */
template <typename T>
void Matrix4<T>::set(PEsize_t x, PEsize_t y, T value)
{
	values[x + 4*y] = value;
}
/**
 * Returns a value of a matrix
 * @param x The x coordinate of the value in the matrix
 * @param y The y coordinate of the value in the matrix
 * @return The value at the given coordinates
 */
template <typename T>
T Matrix4<T>::get(PEsize_t x, PEsize_t y) const
{
	return values[x + 4*y];
}

/**
 * Returns a column of a matrix as a Vector4
 * @param index The column index
 * @return The column at the given index as a Vector4
 */
template <typename T>
Vector4<T> Matrix4<T>::getColumn(PEsize_t index) const
{
	return columns[index];
}

/**
 * Returns the identity matrix
 * @return The identity matrix
 */
template <typename T>
Matrix4<T> Matrix4<T>::identity()
{
	return Matrix4<T>(1);
}

/**
 * Returns a pointer to the first value of the matrix
 * @return A pointer to the first value of the matrix
 */
template <typename T>
const T* Matrix4<T>::value_ptr()
{
	return &values[0];
}

/**
 * Returns a copy of the current matrix
 * @return A copy of the current matrix
 */
template <typename T>
Matrix4<T> Matrix4<T>::copy() const
{
	Matrix4<T> res;
	std::copy(std::begin(values), std::end(values), std::begin(res.values));

	return res;
}

/**
 * Returns the matrix as a String (for debugging purpose)
 * @return The matrix as a String
 */
template <typename T>
std::string Matrix4<T>::toString()
{
	std::string res = "";
	for (PEsize_t i = 0; i < 4; i++)
	{
		res += "(";
		for (PEsize_t j = 0; j < 4; j++)
		{
			res += std::to_string(values[j + i * 4]) + ", ";
		}
		res += ")\n";
	}

	return res;
}

// Matrix4 += Matrix4
template <typename T>
Matrix4<T>& operator+=(Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	lhs = lhs + rhs;
	return lhs;
}

// -= Matrix4
template<typename T>
Matrix4<T>& operator-=(Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	lhs = lhs - rhs;
	return lhs;
}

// *= T
template<typename T>
Matrix4<T>& operator*=(Matrix4<T>& lhs, const T& rhs)
{
	lhs = lhs*rhs;
	return lhs;
}

// /= T
template<typename T>
Matrix4<T>& operator/=(Matrix4<T>& lhs, const T& rhs)
{
	lhs = lhs/rhs;
	return lhs;
}

// Matrix4 + Matrix4
template<typename T>
Matrix4<T> operator+(const Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	return pem::mat4::add(lhs, rhs);
}

// Matrix4 - Matrix4
template<typename T>
Matrix4<T> operator-(const Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	return pem::mat4::subtract(lhs, rhs);
}

// Matrix4 * Matrix4
template<typename T>
Matrix4<T> operator*(const Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	return pem::mat4::multiply(lhs, rhs);
}

// Matrix4 * T
template<typename T>
Matrix4<T> operator*(const Matrix4<T>& lhs, const T& rhs)
{
	return pem::mat4::multiply(lhs, rhs);
}

// T * Matrix4
template<typename T>
Matrix4<T> operator*(const T& lhs, const Matrix4<T>& rhs)
{
	return pem::mat4::multiply(rhs, lhs);
}

// Vector4 * Matrix4
template<typename T>
Vector4<T> operator*(const Vector4<T>& lhs, const Matrix4<T>& rhs)
{
	return pem::mat4::multiply(rhs, lhs);
}
// Matrix4 * vector4
template<typename T>
Vector4<T> operator*(const Matrix4<T>& lhs, const Vector4<T>& rhs)
{
	return pem::mat4::multiply(lhs, rhs);
}

// Matrix4 / T
template<typename T>
Matrix4<T> operator/(const Matrix4<T>& lhs, const T& rhs)
{
	return pem::mat4::divide(lhs, rhs);
}

// Matrix4 == Matrix4
template<typename T>
bool operator==(const Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	for(PEsize_t i = 0; i < 4*4; i++)
	{
		if(lhs.values[i] != rhs.values[i])
			return false;
	}

	return true;
}

// Matrix4 != Matrix4
template<typename T>
bool operator!=(const Matrix4<T>& lhs, const Matrix4<T>& rhs)
{
	for(PEsize_t i = 0; i < 4*4; i++)
	{
		if(lhs.values[i] != rhs.values[i])
			return true;
	}

	return false;
}

template<typename T>
Matrix4<T> orthographicProjection(T left, T top, T right, T bottom, T near, T far)
{
	Matrix4<T> ortho(1);
	ortho.set(0, 0, 2/(right-left));
	ortho.set(1, 1, 2/(top-bottom));
	ortho.set(2, 2, -2/(far-near));
	ortho.set(3, 0, -(right+left)/(right-left));
	ortho.set(3, 1, -(top+bottom)/(top-bottom));
	ortho.set(3, 2, -(far+near)/(far-near));

	return ortho;
}
