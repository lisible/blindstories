// Vector3++
template<typename T>
inline Vector3<T> operator++(Vector3<T>& rhs, int)
{
	Vector3<T> ret = rhs;
	ret.x++;
	ret.y++;
	ret.z++;

	return ret;
}

// ++Vector3
template<typename T>
inline Vector3<T>& operator++(Vector3<T>& rhs)
{
	++rhs.x;
	++rhs.y;
	++rhs.z;

	return rhs;
}

// Vector3--
template<typename T>
inline Vector3<T> operator--(Vector3<T>& rhs, int)
{
	Vector3<T> ret = rhs;
	ret.x--;
	ret.y--;
	ret.z--;

	return ret;
}

// --Vector3
template<typename T>
inline Vector3<T>& operator--(Vector3<T>& rhs)
{
	--rhs.x;
	--rhs.y;
	--rhs.z;

	return rhs;
}


// += Vector3
template <typename T>
inline Vector3<T>& operator+=(Vector3<T>& lhs, const Vector3<T>& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;

	return lhs;
}

// += T
template <typename T>
inline Vector3<T>& operator+=(Vector3<T>& lhs, const T& rhs)
{
	lhs.x += rhs;
	lhs.y += rhs;
	lhs.z += rhs;

	return lhs;
}

// -= Vector3
template<typename T>
inline Vector3<T>& operator-=(Vector3<T>& lhs, const Vector3<T>& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;

	return lhs;
}

// -= T
template <typename T>
inline Vector3<T>& operator-=(Vector3<T>& lhs, const T& rhs)
{
	lhs.x -= rhs;
	lhs.y -= rhs;
	lhs.z -= rhs;

	return lhs;
}

// *= T
template<typename T>
inline Vector3<T>& operator*=(Vector3<T>& lhs, const T& rhs)
{
	lhs.x *= rhs;
	lhs.y *= rhs;
	lhs.z *= rhs;

	return lhs;
}

// /= T
template<typename T>
inline Vector3<T>& operator/=(Vector3<T>& lhs, const T& rhs)
{
	lhs.x /= rhs;
	lhs.y /= rhs;
	lhs.z /= rhs;

	return lhs;
}

// -Vector3
template<typename T>
inline Vector3<T> operator-(const Vector3<T>& in)
{
	Vector3<T> ret;
	ret.x = -in.x;
	ret.y = -in.y;
	ret.z = -in.z;

	return ret;
}

// Vector3 + Vector3
template<typename T>
inline Vector3<T> operator+(const Vector3<T>& lhs, const Vector3<T>& rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x + rhs.x;
	ret.y = lhs.y + rhs.y;
	ret.z = lhs.z + rhs.z;

	return ret;
}

// Vector3 + T
template<typename T>
inline Vector3<T> operator+(const Vector3<T>& lhs, const T& rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x + rhs;
	ret.y = lhs.y + rhs;
	ret.z = lhs.z + rhs;

	return ret;
}

// Vector3 - Vector3
template<typename T>
inline Vector3<T> operator-(const Vector3<T>& lhs, const Vector3<T>& rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x - rhs.x;
	ret.y = lhs.y - rhs.y;
	ret.z = lhs.z - rhs.z;

	return ret;
}

// Vector3 - T
template<typename T>
inline Vector3<T> operator-(const Vector3<T>& lhs, T rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x - rhs;
	ret.y = lhs.y - rhs;
	ret.z = lhs.z - rhs;

	return ret;
}

// Vector3 * T
template<typename T>
inline Vector3<T> operator*(const Vector3<T>& lhs, const T& rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x * rhs;
	ret.y = lhs.y * rhs;
	ret.z = lhs.z * rhs;

	return ret;
}

// T * Vector3
template<typename T>
inline Vector3<T> operator*(const T& lhs, const Vector3<T>& rhs)
{
	Vector3<T> ret;
	ret.x = lhs * rhs.x;
	ret.y = lhs * rhs.y;
	ret.z = lhs * rhs.z;

	return ret;
}

// Vector3 / T
template<typename T>
inline Vector3<T> operator/(const Vector3<T>& lhs, const T& rhs)
{
	Vector3<T> ret;
	ret.x = lhs.x / rhs;
	ret.y = lhs.y / rhs;
	ret.z = lhs.z / rhs;

	return ret;
}

// Vector3 == Vector3
template<typename T>
inline bool operator==(const Vector3<T>& lhs, const Vector3<T>& rhs)
{
	return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z);
}

// Vector3 != Vector3
template<typename T>
inline bool operator!=(const Vector3<T>& lhs, const Vector3<T>& rhs)
{
	return (lhs.x != rhs.x) || (lhs.y != rhs.y) || (lhs.z != rhs.z);
}
