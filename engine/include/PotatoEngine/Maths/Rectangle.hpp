#ifndef PE_MATHS_RECTANGLE_HPP
#define PE_MATHS_RECTANGLE_HPP

#include "types.hpp"
#include <string>

namespace pe
{
namespace maths
{
	/**
 	 * Represents a rectangle
 	 */
	struct Rectangle
	{
		// Top left x coordinate
		PEfloat x;
		// Top left y coordinate
		PEfloat y;
		PEfloat width;
		PEfloat height;

		Rectangle()
			: x(0.f)
			, y(0.f)
			, width(1.f)
			, height(1.f)
		{
		}

		Rectangle(PEfloat x, PEfloat y, PEfloat width, PEfloat height)
			: x(x)
			, y(y)
	  		, width(width)
	  		, height(height)		
		{
		}
		
		std::string toString() const
		{
			std::string str("(");
			str += std::to_string(x) + ", ";
			str += std::to_string(y) + ", ";
			str += std::to_string(width) + ", ";
			str += std::to_string(height);
			str += ")";

			return str;
		}
	};
}
}

#endif
