#ifndef PE_MATHS_VECTOR2_HPP
#define PE_MATHS_VECTOR2_HPP

#include "types.hpp"
#include <string>

namespace pe
{
namespace maths
{
	/**
 	 * Template for 2D vectors
 	 */
	template<typename T>
	struct Vector2
	{
		/// The x component of the Vector
		T x;
		/// The y component of the Vector
		T y;

		Vector2()
			: x(0)
			, y(0)
		{}
		Vector2(T x, T y)
			: x(x)
			, y(y)
		{}

		std::string toString() const
		{
			return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
		}

		const PEvoid* value_ptr() const
		{
			return &x;
		}
	};

	// Vector2++
	template<typename T>
	Vector2<T> operator++(Vector2<T>& rhs, int);

	// ++Vector2
	template<typename T>
	Vector2<T>& operator++(Vector2<T>& rhs);

	// Vector2--
	template<typename T>
	Vector2<T> operator--(Vector2<T>& rhs, int);

	// --Vector2
	template<typename T>
	Vector2<T>& operator--(Vector2<T>& rhs);

	// Vecotr2 += Vector2
	template <typename T>
	Vector2<T>& operator+=(const Vector2<T>& lhs, const Vector2<T>& rhs);

	// Vector2 += T
	template <typename T>
	Vector2<T>& operator+=(const Vector2<T>& lhs, const T& rhs);

	// -= Vector2
	template<typename T>
	Vector2<T>& operator-=(const Vector2<T>& lhs, const Vector2<T>& rhs);

	// -= T
	template <typename T>
	Vector2<T>& operator-=(const Vector2<T>& lhs, const T& rhs);

	// *= T
	template<typename T>
	Vector2<T>& operator*=(const Vector2<T>& lhs, const T& rhs);

	// /= T
	template<typename T>
	Vector2<T>& operator/=(const Vector2<T>& lhs, const T& rhs);

	// -Vector2
	template<typename T>
	Vector2<T> operator-(const Vector2<T>& in);

	// Vector2 + Vector2
	template<typename T>
	Vector2<T> operator+(const Vector2<T>& lhs, const Vector2<T>& rhs);

	// Vector2 + T
	template<typename T>
	Vector2<T> operator+(const Vector2<T>& lhs, const T& rhs);

	// Vector2 - Vector2
	template<typename T>
	Vector2<T> operator-(const Vector2<T>& lhs, const Vector2<T>& rhs);

	// Vector2 - T
	template<typename T>
	Vector2<T> operator-(const Vector2<T>& lhs, T rhs);

	// Vector2 * T
	template<typename T>
	Vector2<T> operator*(const Vector2<T>& lhs, const T& rhs);

	// T * Vector2
	template<typename T>
	Vector2<T> operator*(const T& lhs, const Vector2<T>& rhs);

	// Vector2 / T
	template<typename T>
	Vector2<T> operator/(const Vector2<T>& lhs, const T& rhs);

	// Vector2 == Vector2
	template<typename T>
	bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs);

	// Vector2 != Vector2
	template<typename T>
	bool operator!=(const Vector2<T>& lhs, const Vector2<T>& rhs);

	using vec2 = Vector2<PEfloat>;
	using vec2i = Vector2<PEint32>;
	using vec2u = Vector2<PEuint32>;
	using vec2d = Vector2<PEdouble>;

	#include "Maths/Vector2.inl"
}
}

#endif
