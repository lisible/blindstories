#ifndef PE_MATHS_VECTOR4_HPP
#define PE_MATHS_VECTOR4_HPP

#include "types.hpp"
#include <string>

namespace pe
{
namespace maths
{
	/**
 	 * 4D vector
 	 */
	template <typename T>
	struct Vector4
	{
		/// The x component of the Vector
		T x;
		/// The y component of the Vector
		T y;
		/// The z component of the Vector
		T z;
		/// The w component of the Vector
		T w;

		Vector4()
				: x(0)
				, y(0)
				, z(0)
				, w(0)
		{}
		Vector4(T x, T y, T z, T w)
				: x(x)
				, y(y)
				, z(z)
				, w(w)
		{}

		std::string  toString() const
		{
			return "(" + std::to_string(x) + 
					", " + std::to_string(y) + 
					", " + std::to_string(z) + 
					", " + std::to_string(w) + ")";
		}

		const PEvoid* value_ptr() const
		{
			return &x;
		}
	};

	// Vector4++
	template<typename T>
	Vector4<T> operator++(Vector4<T>& rhs, int);

	// ++Vector4
	template<typename T>
	Vector4<T>& operator++(Vector4<T>& rhs);

	// Vector4--
	template<typename T>
	Vector4<T> operator--(Vector4<T>& rhs, int);

	// --Vector4
	template<typename T>
	Vector4<T>& operator--(Vector4<T>& rhs);

	// Vecotr4 += Vector4
	template <typename T>
	Vector4<T>& operator+=(const Vector4<T>& lhs, const Vector4<T>& rhs);

	// Vector4 += T
	template <typename T>
	Vector4<T>& operator+=(const Vector4<T>& lhs, const T& rhs);

	// -= Vector4
	template<typename T>
	Vector4<T>& operator-=(const Vector4<T>& lhs, const Vector4<T>& rhs);

	// -= T
	template <typename T>
	Vector4<T>& operator-=(const Vector4<T>& lhs, const T& rhs);

	// *= T
	template<typename T>
	Vector4<T>& operator*=(const Vector4<T>& lhs, const T& rhs);

	// /= T
	template<typename T>
	Vector4<T>& operator/=(const Vector4<T>& lhs, const T& rhs);

	// -Vector4
	template<typename T>
	Vector4<T> operator-(const Vector4<T>& in);

	// Vector4 + Vector4
	template<typename T>
	Vector4<T> operator+(const Vector4<T>& lhs, const Vector4<T>& rhs);

	// Vector4 + T
	template<typename T>
	Vector4<T> operator+(const Vector4<T>& lhs, const T& rhs);

	// Vector4 - Vector4
	template<typename T>
	Vector4<T> operator-(const Vector4<T>& lhs, const Vector4<T>& rhs);

	// Vector4 - T
	template<typename T>
	Vector4<T> operator-(const Vector4<T>& lhs, T rhs);

	// Vector4 * T
	template<typename T>
	Vector4<T> operator*(const Vector4<T>& lhs, const T& rhs);

	// T * Vector4
	template<typename T>
	Vector4<T> operator*(const T& lhs, const Vector4<T>& rhs);

	// Vector4 / T
	template<typename T>
	Vector4<T> operator/(const Vector4<T>& lhs, const T& rhs);

	// Vector4 == Vector4
	template<typename T>
	bool operator==(const Vector4<T>& lhs, const Vector4<T>& rhs);

	// Vector4 != Vector4
	template<typename T>
	bool operator!=(const Vector4<T>& lhs, const Vector4<T>& rhs);

	using vec4 = Vector4<PEfloat>;
	using vec4i = Vector4<PEint32>;
	using vec4u = Vector4<PEuint32>;
	using vec4d = Vector4<PEdouble>;

	#include "Maths/Vector4.inl"
}
}
#endif
