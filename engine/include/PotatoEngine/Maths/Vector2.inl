// Vector2++
template<typename T>
inline Vector2<T> operator++(Vector2<T>& rhs, int)
{
	Vector2<T> ret = rhs;
	ret.x++;
	ret.y++;

	return ret;
}

// ++Vector2
template<typename T>
inline Vector2<T>& operator++(Vector2<T>& rhs)
{
	++rhs.x;
	++rhs.y;

	return rhs;
}

// Vector2--
template<typename T>
inline Vector2<T> operator--(Vector2<T>& rhs, int)
{
	Vector2<T> ret = rhs;
	ret.x--;
	ret.y--;

	return ret;
}

// --Vector2
template<typename T>
inline Vector2<T>& operator--(Vector2<T>& rhs)
{
	--rhs.x;
	--rhs.y;

	return rhs;
}


// += Vector2
template <typename T>
inline Vector2<T>& operator+=(Vector2<T>& lhs, const Vector2<T>& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;

	return lhs;
}

// += T
template <typename T>
inline Vector2<T>& operator+=(Vector2<T>& lhs, const T& rhs)
{
	lhs.x += rhs;
	lhs.y += rhs;

	return lhs;
}

// -= Vector2
template<typename T>
inline Vector2<T>& operator-=(Vector2<T>& lhs, const Vector2<T>& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;

	return lhs;
}

// -= T
template <typename T>
inline Vector2<T>& operator-=(Vector2<T>& lhs, const T& rhs)
{
	lhs.x -= rhs;
	lhs.y -= rhs;

	return lhs;
}

// *= T
template<typename T>
inline Vector2<T>& operator*=(Vector2<T>& lhs, const T& rhs)
{
	lhs.x *= rhs;
	lhs.y *= rhs;

	return lhs;
}

// /= T
template<typename T>
inline Vector2<T>& operator/=(Vector2<T>& lhs, const T& rhs)
{
	lhs.x /= rhs;
	lhs.y /= rhs;

	return lhs;
}

// -Vector2
template<typename T>
inline Vector2<T> operator-(const Vector2<T>& in)
{
	Vector2<T> ret;
	ret.x = -in.x;
	ret.y = -in.y;

	return ret;
}

// Vector2 + Vector2
template<typename T>
inline Vector2<T> operator+(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x + rhs.x;
	ret.y = lhs.y + rhs.y;

	return ret;
}

// Vector2 + T
template<typename T>
inline Vector2<T> operator+(const Vector2<T>& lhs, const T& rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x + rhs;
	ret.y = lhs.y + rhs;

	return ret;
}

// Vector2 - Vector2
template<typename T>
inline Vector2<T> operator-(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x - rhs.x;
	ret.y = lhs.y - rhs.y;

	return ret;
}

// Vector2 - T
template<typename T>
inline Vector2<T> operator-(const Vector2<T>& lhs, T rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x - rhs;
	ret.y = lhs.y - rhs;

	return ret;
}

// Vector2 * T
template<typename T>
inline Vector2<T> operator*(const Vector2<T>& lhs, const T& rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x * rhs;
	ret.y = lhs.y * rhs;

	return ret;
}

// T * Vector2
template<typename T>
inline Vector2<T> operator*(const T& lhs, const Vector2<T>& rhs)
{
	Vector2<T> ret;
	ret.x = lhs * rhs.x;
	ret.y = lhs * rhs.y;

	return ret;
}

// Vector2 / T
template<typename T>
inline Vector2<T> operator/(const Vector2<T>& lhs, const T& rhs)
{
	Vector2<T> ret;
	ret.x = lhs.x / rhs;
	ret.y = lhs.y / rhs;

	return ret;
}

// Vector2 == Vector2
template<typename T>
inline bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
	return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

// Vector2 != Vector2
template<typename T>
inline bool operator!=(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
	return (lhs.x != rhs.x) || (lhs.y != rhs.y);
}
