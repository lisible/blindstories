#ifndef PE_MATHS_BASE_HPP
#define PE_MATHS_BASE_HPP

#include <sstream>
#include <iomanip>

namespace pe
{
namespace maths
{
    template <typename T>
    T hex2dec(const std::string& hexValue)
    {
        T result;
        std::stringstream ss;
        ss << std::hex << hexValue;
        ss >> result;
        return result;
    }
}
}

#endif