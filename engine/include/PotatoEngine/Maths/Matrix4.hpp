#ifndef PE_MATHS_MATRIX4_HPP
#define PE_MATHS_MATRIX4_HPP

#include "types.hpp"
#include "Maths/Vector3.hpp"
#include "Maths/Vector4.hpp"

#include <cmath>

namespace pe
{
namespace maths
{
	/**
 	 * 4x4 Matrix
 	 */
	template <typename T>
	struct Matrix4
	{
		/// The matrix values
		union
		{
			T values[4*4];
			Vector4<T> columns[4];
		};

		/**
		 * Creates an empty matrix
		 */
		Matrix4()
		{
			fill(0);
		}

		/**
		 * Creates a matrix with the given values
		 * @param valuesArray The values of the matrix
		 */
		Matrix4(T* valuesArray)
		{
			setValues(valuesArray);
		}

		/**
		 * Creates a matrix with a given diagonal value
		 * @param diagonal The value on the diagonal
		 */
		Matrix4(T diagonal)
		{
			fill(0);
			for(PEsize_t i = 0; i < 4; i++)
				values[i + i * 4] = diagonal;
		}

		/**
		 * Sets the values of a matrix
		 * @param valuesArray The values of the matrix
		 */
		PEvoid setValues(T* valuesArray);

		/**
		 * Fills a matrix with a value
		 * @param value The value to fill the matrix with
		 */
		PEvoid fill(T value);

		/**
 		 * Returns the inverse matrix
 		 * @return The inverse matrix
 		 */
		Matrix4<T> invert();

		/**
 		 * Returns a translation matrix
 		 * @param translation a vector describing the translation
 		 * @return The corresponding translation matrix
 		 */
		static Matrix4<T> translationMatrix(const Vector3<T>& translation);
		/**
 		 * Returns a translation matrix
 		 * @param x The translation on the x axis
 		 * @param y The translation on the y axis
 		 * @param z The translation on the z axis
 		 * @return The corresponding translation matrix
 		 */
		static Matrix4<T> translationMatrix(T x, T y, T z);

		/**
 		 * Returns a rotation matrix
 		 * @param angleDegrees The angle in degrees
 		 * @param axis The axis
 		 * @param center The center of rotation
 		 * @return The rotation matrix
 		 */
		static Matrix4<T> rotationMatrix(PEfloat angleDegrees, const Vector3<T>& axis, const Vector3<T>& center = Vector3<T>(0, 0, 0));

		/**
 		 * Returns a scale matrix
 		 * @param scale A vector describing the scaling
 		 * @return The corresponding scale matrix
 		 */
		static Matrix4<T> scaleMatrix(const Vector3<T>& scale);
		static Matrix4<T> scaleMatrix(T scaleX, T scaleY, T scaleZ);

		/**
		 * Multiplies the matrix with the one passed in parameter
		 * @param m The matrix to multiply with
		 */
		void multiply(const Matrix4<T>& m);
		/**
		 * Multiplies a matrix with another
		 * @param m1 The first matrix
		 * @param m2 The second matrix
		 * @return The matrix m1*m2
		 */
		static Matrix4<T> multiply(const Matrix4<T>& m1, const Matrix4<T>& m2);
		/**
		 * Multiplies the current matrix with a constant
		 * @param a The constant to multiply with
		 */
		void multiply(T a);
		/**
		 * Multiplies a matrix with a constant
		 * @param m The matrix to multiply
		 * @param a The constant to multiply the matrix with
		 * @return The matrix m*a
		 */
		static Matrix4<T> multiply(const Matrix4<T>& m, T a);

		/**
 		 * Multiplies a matrix with a vector
 		 * @param m The matrix
 		 * @param v The vector
 		 * @return The vector m*v
 		 */
		static Vector4<T> multiply(const Matrix4<T>& m, const Vector4<T>& v);

		/**
		 * Divides the current matrix with a constant
		 * @param a The constant to divide with
		 */
		void divide(T a);

		/**
		 * Divides a matrix with a constant
		 * @param m The matrix to divide
		 * @param a The constant to divide the matrix with
		 * @return The matrix m/a
		 */
		static Matrix4<T> divide(const Matrix4<T>& m, T a);

		/**
		 * Adds a matrix to the current matrix
		 * @param m The matrix to add
		 */
		void add(const Matrix4<T>& m);
		/**
		 * Returns the addition of two matrices
		 * @param m1 The first matrix
		 * @param m2 The second matrix
		 * @return The matrix m1 + m2
		 */
		static Matrix4<T> add(const Matrix4<T>& m1, const Matrix4<T>& m2);

		/**
		 * Subtract a matrix from the current matrix
		 * @param m The matrix to subtract
		 */
		void subtract(const Matrix4<T>& m);
		/**
		 * Returns the subtraction of two matrices
		 * @param m1 The first matrix
		 * @param m2 The second matrix
		 * @return The matrix m1 - m2
		 */
		static Matrix4<T> subtract(const Matrix4<T>& m1, const Matrix4<T>& m2);

		/**
		 * Sets a value of the matrix
		 * @param x The x coordinate of the value (left is 0)
		 * @param y The y coordinate of the value (top is 0)
		 * @param value The value
		 */
		void set(PEsize_t x, PEsize_t y, T value);
		/**
		 * Returns a value of a matrix
		 * @param x The x coordinate of the value in the matrix
		 * @param y The y coordinate of the value in the matrix
		 * @return The value at the given coordinates
		 */
		T get(PEsize_t x, PEsize_t y) const;

		/**
		 * Returns a column of a matrix as a Vector4
		 * @param index The column index
		 * @return The column at the given index as a Vector4
		 */
		Vector4<T> getColumn(PEsize_t index) const;

		/**
		 * Returns the identity matrix
		 * @return The identity matrix
		 */
		static Matrix4<T> identity();

		/**
		 * Returns a pointer to the first value of the matrix
		 * @return A pointer to the first value of the matrix
		 */
		const T* value_ptr();

		/**
		 * Returns a copy of the current matrix
		 * @return A copy of the current matrix
		 */
		Matrix4 copy() const;

		/**
		 * Returns the matrix as a String (for debugging purpose)
		 * @return The matrix as a String
		 */
		std::string toString();
	};

	// Matrix4 += Matrix4
	template <typename T>
	Matrix4<T>& operator+=(Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// -= Matrix4
	template<typename T>
	Matrix4<T>& operator-=(Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// *= T
	template<typename T>
	Matrix4<T>& operator*=(Matrix4<T>& lhs, const T& rhs);

	// /= T
	template<typename T>
	Matrix4<T>& operator/=(Matrix4<T>& lhs, const T& rhs);

	// Matrix4 + Matrix4
	template<typename T>
	Matrix4<T> operator+(const Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// Matrix4 - Matrix4
	template<typename T>
	Matrix4<T> operator-(const Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// Matrix4 * Matrix4
	template<typename T>
	Matrix4<T> operator*(const Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// Matrix4 * T
	template<typename T>
	Matrix4<T> operator*(const Matrix4<T>& lhs, const T& rhs);

	// T * Matrix4
	template<typename T>
	Matrix4<T> operator*(const T& lhs, const Matrix4<T>& rhs);

	// Matrix4 * Vector4<T>
	template<typename T>
	Vector4<T> operator*(const Vector4<T>& lhs, const Matrix4<T>& rhs);

	// Matrix4 * Vector4<T>
	template<typename T>
	Vector4<T> operator*(const Matrix4<T>& lhs, const Vector4<T>& rhs);

	// Matrix4 / T
	template<typename T>
	Matrix4<T> operator/(const Matrix4<T>& lhs, const T& rhs);

	// Matrix4 == Matrix4
	template<typename T>
	bool operator==(const Matrix4<T>& lhs, const Matrix4<T>& rhs);

	// Matrix4 != Matrix4
	template<typename T>
	bool operator!=(const Matrix4<T>& lhs, const Matrix4<T>& rhs);

	template<typename T>
	Matrix4<T> orthographicProjection(T left, T top, T right, T bottom, T near, T far);

	using mat4 = Matrix4<PEfloat>;
	using mat4i = Matrix4<PEint32>;
	using mat4u = Matrix4<PEuint32>;
	using mat4d = Matrix4<PEdouble>;

	namespace pem = pe::maths;
	#include "Maths/Matrix4.inl"
}
}

#endif
