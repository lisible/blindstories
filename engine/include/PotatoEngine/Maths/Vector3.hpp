#ifndef PE_MATHS_VECTOR3_HPP
#define PE_MATHS_VECTOR3_HPP

#include "types.hpp"
#include <string>

namespace pe
{
namespace maths
{
	/**
 	 * 3D vector
 	 */
	template <typename T>
	struct Vector3
	{
		/// The x component of the Vector
        T x;
        /// The y component of the Vector
        T y;
        /// The z component of the Vector
        T z;

        Vector3()
                : x(0)
                , y(0)
                , z(0)
        {}
        Vector3(T x, T y, T z)
                : x(x)
                , y(y)
                , z(z)
        {}

		std::string toString() const
        {
            return "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ")";
        }

        const PEvoid* value_ptr() const
        {
            return &x;
        }
	};

	// Vector3++
    template<typename T>
    Vector3<T> operator++(Vector3<T>& rhs, int);

    // ++Vector3
    template<typename T>
    Vector3<T>& operator++(Vector3<T>& rhs);

    // Vector3--
    template<typename T>
    Vector3<T> operator--(Vector3<T>& rhs, int);

    // --Vector3
    template<typename T>
    Vector3<T>& operator--(Vector3<T>& rhs);

    // Vecotr3 += Vector3
    template <typename T>
    Vector3<T>& operator+=(const Vector3<T>& lhs, const Vector3<T>& rhs);

    // Vector3 += T
    template <typename T>
    Vector3<T>& operator+=(const Vector3<T>& lhs, const T& rhs);

    // -= Vector3
    template<typename T>
    Vector3<T>& operator-=(const Vector3<T>& lhs, const Vector3<T>& rhs);

    // -= T
    template <typename T>
    Vector3<T>& operator-=(const Vector3<T>& lhs, const T& rhs);

    // *= T
    template<typename T>
    Vector3<T>& operator*=(const Vector3<T>& lhs, const T& rhs);

    // /= T
    template<typename T>
    Vector3<T>& operator/=(const Vector3<T>& lhs, const T& rhs);

    // -Vector3
    template<typename T>
    Vector3<T> operator-(const Vector3<T>& in);

    // Vector3 + Vector3
    template<typename T>
    Vector3<T> operator+(const Vector3<T>& lhs, const Vector3<T>& rhs);

    // Vector3 + T
    template<typename T>
    Vector3<T> operator+(const Vector3<T>& lhs, const T& rhs);

    // Vector3 - Vector3
    template<typename T>
    Vector3<T> operator-(const Vector3<T>& lhs, const Vector3<T>& rhs);

    // Vector3 - T
    template<typename T>
    Vector3<T> operator-(const Vector3<T>& lhs, T rhs);

    // Vector3 * T
    template<typename T>
    Vector3<T> operator*(const Vector3<T>& lhs, const T& rhs);

    // T * Vector3
    template<typename T>
    Vector3<T> operator*(const T& lhs, const Vector3<T>& rhs);

    // Vector3 / T
    template<typename T>
    Vector3<T> operator/(const Vector3<T>& lhs, const T& rhs);

    // Vector3 == Vector3
    template<typename T>
    bool operator==(const Vector3<T>& lhs, const Vector3<T>& rhs);

    // Vector3 != Vector3
    template<typename T>
    bool operator!=(const Vector3<T>& lhs, const Vector3<T>& rhs);

    using vec3 = Vector3<PEfloat>;
    using vec3i = Vector3<PEint32>;
    using vec3u = Vector3<PEuint32>;
    using vec3d = Vector3<PEdouble>;

	#include "Maths/Vector3.inl"
}
}

#endif
