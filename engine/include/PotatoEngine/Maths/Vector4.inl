// Vector4++
template<typename T>
inline Vector4<T> operator++(Vector4<T>& rhs, int)
{
	Vector4<T> ret = rhs;
	ret.x++;
	ret.y++;
	ret.z++;
	ret.w++;

	return ret;
}

// ++Vector4
template<typename T>
inline Vector4<T>& operator++(Vector4<T>& rhs)
{
	++rhs.x;
	++rhs.y;
	++rhs.z;
	++rhs.w;

	return rhs;
}

// Vector4--
template<typename T>
inline Vector4<T> operator--(Vector4<T>& rhs, int)
{
	Vector4<T> ret = rhs;
	ret.x--;
	ret.y--;
	ret.z--;
	ret.w--;

	return ret;
}

// --Vector4
template<typename T>
inline Vector4<T>& operator--(Vector4<T>& rhs)
{
	--rhs.x;
	--rhs.y;
	--rhs.z;
	--rhs.w;

	return rhs;
}


// += Vector4
template <typename T>
inline Vector4<T>& operator+=(Vector4<T>& lhs, const Vector4<T>& rhs)
{
	lhs.x += rhs.x;
	lhs.y += rhs.y;
	lhs.z += rhs.z;
	lhs.w += rhs.w;

	return lhs;
}

// += T
template <typename T>
inline Vector4<T>& operator+=(Vector4<T>& lhs, const T& rhs)
{
	lhs.x += rhs;
	lhs.y += rhs;
	lhs.z += rhs;
	lhs.w += rhs;

	return lhs;
}

// -= Vector4
template<typename T>
inline Vector4<T>& operator-=(Vector4<T>& lhs, const Vector4<T>& rhs)
{
	lhs.x -= rhs.x;
	lhs.y -= rhs.y;
	lhs.z -= rhs.z;
	lhs.w -= rhs.w;

	return lhs;
}

// -= T
template <typename T>
inline Vector4<T>& operator-=(Vector4<T>& lhs, const T& rhs)
{
	lhs.x -= rhs;
	lhs.y -= rhs;
	lhs.z -= rhs;
	lhs.w -= rhs;

	return lhs;
}

// *= T
template<typename T>
inline Vector4<T>& operator*=(Vector4<T>& lhs, const T& rhs)
{
	lhs.x *= rhs;
	lhs.y *= rhs;
	lhs.z *= rhs;
	lhs.w *= rhs;

	return lhs;
}

// /= T
template<typename T>
inline Vector4<T>& operator/=(Vector4<T>& lhs, const T& rhs)
{
	lhs.x /= rhs;
	lhs.y /= rhs;
	lhs.z /= rhs;
	lhs.w /= rhs;

	return lhs;
}

// -Vector4
template<typename T>
inline Vector4<T> operator-(const Vector4<T>& in)
{
	Vector4<T> ret;
	ret.x = -in.x;
	ret.y = -in.y;
	ret.z = -in.z;
	ret.w = -in.w;

	return ret;
}

// Vector4 + Vector4
template<typename T>
inline Vector4<T> operator+(const Vector4<T>& lhs, const Vector4<T>& rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x + rhs.x;
	ret.y = lhs.y + rhs.y;
	ret.z = lhs.z + rhs.z;
	ret.w = lhs.w + rhs.w;

	return ret;
}

// Vector4 + T
template<typename T>
inline Vector4<T> operator+(const Vector4<T>& lhs, const T& rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x + rhs;
	ret.y = lhs.y + rhs;
	ret.z = lhs.z + rhs;
	ret.w = lhs.w + rhs;

	return ret;
}

// Vector4 - Vector4
template<typename T>
inline Vector4<T> operator-(const Vector4<T>& lhs, const Vector4<T>& rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x - rhs.x;
	ret.y = lhs.y - rhs.y;
	ret.z = lhs.z - rhs.z;
	ret.w = lhs.w - rhs.w;

	return ret;
}

// Vector4 - T
template<typename T>
inline Vector4<T> operator-(const Vector4<T>& lhs, T rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x - rhs;
	ret.y = lhs.y - rhs;
	ret.z = lhs.z - rhs;
	ret.w = lhs.w - rhs;

	return ret;
}

// Vector4 * T
template<typename T>
inline Vector4<T> operator*(const Vector4<T>& lhs, const T& rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x * rhs;
	ret.y = lhs.y * rhs;
	ret.z = lhs.z * rhs;
	ret.w = lhs.w * rhs;

	return ret;
}

// T * Vector4
template<typename T>
inline Vector4<T> operator*(const T& lhs, const Vector4<T>& rhs)
{
	Vector4<T> ret;
	ret.x = lhs * rhs.x;
	ret.y = lhs * rhs.y;
	ret.z = lhs * rhs.z;
	ret.w = lhs * rhs.w;

	return ret;
}

// Vector4 / T
template<typename T>
inline Vector4<T> operator/(const Vector4<T>& lhs, const T& rhs)
{
	Vector4<T> ret;
	ret.x = lhs.x / rhs;
	ret.y = lhs.y / rhs;
	ret.z = lhs.z / rhs;
	ret.w = lhs.w / rhs;

	return ret;
}

// Vector4 == Vector4
template<typename T>
inline bool operator==(const Vector4<T>& lhs, const Vector4<T>& rhs)
{
	return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z) && (lhs.w == rhs.w);
}

// Vector4 != Vector4
template<typename T>
inline bool operator!=(const Vector4<T>& lhs, const Vector4<T>& rhs)
{
	return (lhs.x != rhs.x) || (lhs.y != rhs.y) || (lhs.z != rhs.z) || (lhs.w != rhs.w);
}
