#ifndef PE_UTILS_STRING_HPP
#define PE_UTILS_STRING_HPP

#include "types.hpp"
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>

namespace pe
{
namespace utils
{

	template<typename Out>
		void split(const std::string &s, char delim, Out result) {
			std::stringstream ss;
			ss.str(s);
			std::string item;
			while (std::getline(ss, item, delim)) {
				if(!item.empty())
    				*(result++) = item;
			}
		}

	inline std::vector<std::string> split(const std::string &s, char delim) {
		std::vector<std::string> elems;
		split(s, delim, std::back_inserter(elems));
		return elems;
	}

	inline void toLowercase(std::string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	}
}
}

#endif
