#ifndef PE_UTILS_MATHS_HPP
#define PE_UTILS_MATHS_HPP

namespace pe
{
namespace utils
{
    inline double map(double value, double inMin, double inMax, double outMin, double outMax)
    {
        int input_range = inMax - inMin;
        int output_range = outMax - outMin;

        return (value-inMin)*output_range / input_range + outMin;
    }
}
}

#endif