#ifndef PE_TYPES_HPP
#define PE_TYPES_HPP

#include <cstdint>
#include <cstddef>

using PEint8 = std::int8_t;
using PEint16 = std::int16_t;
using PEint32 = std::int32_t;
using PEint64 = std::int64_t;

using PEuint8 = std::uint8_t;
using PEuint16 = std::uint16_t;
using PEuint32 = std::uint32_t;
using PEuint64 = std::uint64_t;

using PEfloat = float;
using PEdouble = double;
using PEchar = char;
using PEbyte = PEuint8;
using PEbool = bool;
using PEvoid = void;
using PEsize_t = std::size_t;

using PEMsgId = PEuint32;



#endif

