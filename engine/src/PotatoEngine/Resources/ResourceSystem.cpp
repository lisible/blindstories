#include "Resources/ResourceSystem.hpp"
#include "Resources/ShaderLoader.hpp"
#include "Resources/TextureLoader.hpp"
#include "Messaging/Messages.hpp"

#include <tinyxml2.h>


//TODO remove
#include <iostream>
namespace pe
{
namespace resources
{
	ResourceSystem::ResourceSystem()
		: textureLoader(*this)
		, shaderLoader(*this)
		, fontLoader(*this, textureLoader)
		, uiLoader(*this)
	{
		registerDefaultHandlers();	
		loadResourceIndex();
	}

	ResourceSystem::~ResourceSystem()
	{
		shaderCache.clearResources();	
	}

    PEvoid ResourceSystem::setUIComponentFactory(pe::ui::GUIComponentFactory* componentFactory)
    {
        uiLoader.setComponentFactory(componentFactory);
    }

	PEvoid ResourceSystem::loadShader(const std::string& name)
	{
		Resource<pe::graphics::Shader> shaderResource = shaderLoader.loadShaderResource(name);
		shaderCache.addResource(name, shaderResource);
	}

	PEvoid ResourceSystem::loadTexture(const std::string& name)
	{
		Resource<pe::graphics::Texture> textureResource = textureLoader.loadTextureResource(name);
		textureCache.addResource(name, textureResource);
	}

	PEvoid ResourceSystem::loadFont(const std::string& name)
	{
		Resource<pe::graphics::Font> fontResource = fontLoader.loadFontResource(name);
		fontCache.addResource(name, fontResource);
	}

	PEvoid ResourceSystem::loadUI(const std::string& name)
	{
		Resource<pe::ui::UserInterface> uiResource = uiLoader.loadUIResource(name);
		uiCache.addResource(name, uiResource);
	}

	PEvoid ResourceSystem::onLoadShaderRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);
		loadShader(*name);	
	}

	PEvoid* ResourceSystem::onGetShaderDRequest(PEvoid* data)
	{
		std::string* shaderName = static_cast<std::string*>(data);	
		// If the shader is not loaded into the cache, load it	
		if(!shaderCache.hasResource(*shaderName))
			loadShader(*shaderName);

		return &shaderCache.getResource(*shaderName);
	}

	PEvoid ResourceSystem::onLoadTextureRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);
		loadTexture(*name);
	}

	PEvoid* ResourceSystem::onGetTextureDRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);
		if(!textureCache.hasResource(*name))
			loadTexture(*name);

		return &textureCache.getResource(*name);
	}

	PEvoid ResourceSystem::onLoadFontRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);
		loadFont(*name);
	}

	PEvoid* ResourceSystem::onGetFontDRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);

		if(!fontCache.hasResource(*name))
			loadFont(*name);


		if(!fontCache.hasResource(*name))
			std::cout << "pb" << std::endl;
		
		return &fontCache.getResource(*name);
	}

	PEvoid ResourceSystem::onLoadUIRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);
		loadUI(*name);
	}

	PEvoid* ResourceSystem::onGetUIDRequest(PEvoid* data)
	{
		std::string* name = static_cast<std::string*>(data);

		if(!uiCache.hasResource(*name))
			loadUI(*name);


		if(!uiCache.hasResource(*name))
			std::cout << "pb" << std::endl;

		return &uiCache.getResource(*name);
	}

	PEvoid ResourceSystem::loadResourceIndex()
	{
		using namespace tinyxml2;
		XMLDocument doc;
		doc.LoadFile("data/resource_index.xml");
		auto element = doc.FirstChildElement("resourceIndex")->FirstChildElement("resourceFile");
		while(element)
		{
			const char* identifier = element->Attribute("identifier");	
			const char* filePath = element->Attribute("file");

			resourceIndex.addResourceFile(std::string(identifier), std::string(filePath));

			element = element->NextSiblingElement();
		}	
	}

	const std::string& ResourceSystem::getResourceFilePath(const std::string& identifier) const
	{
		return resourceIndex.getResourceFilePath(identifier);
	}

	PEvoid ResourceSystem::registerDefaultHandlers()
	{
		registerCallback(pe::messaging::MSG_RESOURCES_LOADSHADER_REQUEST,
						[&](PEvoid* data){onLoadShaderRequest(data);});
		registerDirectCallback(pe::messaging::MSG_RESOURCES_GETSHADER_DREQUEST,
						[&](PEvoid* data){return onGetShaderDRequest(data);});

		registerCallback(pe::messaging::MSG_RESOURCES_LOADTEXTURE_REQUEST,
						[&](PEvoid* data){onLoadTextureRequest(data);});
		registerDirectCallback(pe::messaging::MSG_RESOURCES_GETTEXTURE_DREQUEST,
						[&](PEvoid* data){return onGetTextureDRequest(data);});

		registerCallback(pe::messaging::MSG_RESOURCES_LOADFONT_REQUEST,
						[&](PEvoid* data){onLoadFontRequest(data);});
		registerDirectCallback(pe::messaging::MSG_RESOURCES_GETFONT_DREQUEST,
						[&](PEvoid* data){return onGetFontDRequest(data);});

		registerCallback(pe::messaging::MSG_RESOURCES_LOADUI_REQUEST,
						 [&](PEvoid* data){onLoadUIRequest(data);});
		registerDirectCallback(pe::messaging::MSG_RESOURCES_GETUI_DREQUEST,
							   [&](PEvoid* data){return onGetUIDRequest(data);});
	}
}
}
