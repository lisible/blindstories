#include "Resources/ResourceIndex.hpp"

namespace pe 
{
namespace resources
{
	ResourceIndex::ResourceIndex()
	{
	}

	ResourceIndex::~ResourceIndex()
	{
	}

	PEvoid ResourceIndex::addResourceFile(const std::string& resourceIdentifier,
										  const std::string& file)
	{
		resourceFilePaths.insert(std::make_pair(resourceIdentifier, file));
	}

	const std::string& ResourceIndex::getResourceFilePath(const std::string& resourceIdentifier) const
	{
		return resourceFilePaths.at(resourceIdentifier);
	}
}
}
