#include "Resources/ShaderLoader.hpp"

#include "File/File.hpp"
#include "File/TextFileReader.hpp"
#include "File/ResourceFileReader.hpp"
#include "Resources/ResourcePaths.hpp"
#include "Resources/ResourceSystem.hpp"

#include <GL/glew.h>
#include <tinyxml2.h>
#include <iostream>
#include <vector>

namespace pe
{
namespace resources
{
	ShaderLoader::ShaderLoader(ResourceSystem& resourceSystem)
		: resourceSystem(resourceSystem)
	{
	}

	ShaderLoader::~ShaderLoader()
	{
	}

	pe::graphics::Shader* ShaderLoader::loadShader(const std::string& name)
	{
		std::string resourceFilePath = "data/" + resourceSystem.getResourceFilePath(name);
		tinyxml2::XMLDocument doc;
		doc.LoadFile(resourceFilePath.c_str());

		auto root = doc.FirstChildElement("resource");
		std::string identifier(root->FirstChildElement("identifier")->GetText());
		if(identifier != name)
		{
			// Error
			return nullptr;
		}

		std::string type(root->FirstChildElement("type")->GetText());
		if(type != "shader")
		{
			// Error
			return nullptr;
		}

		auto shaderRoot = root->FirstChildElement("shader");

		
		GLuint vertexShader(0), fragmentShader(0), geometryShader(0);

		auto vertexShaderFile = shaderRoot->FirstChildElement("vertexShaderFile");
		PEbool hasVertexShader = vertexShaderFile != NULL;
		if(vertexShaderFile)
		{
			std::string filePath(vertexShaderFile->GetText());
			filePath = "data/" + filePath;
			vertexShader = compileShader(filePath, pe::graphics::ShaderType::VERTEX);		
		}

		auto fragmentShaderFile = shaderRoot->FirstChildElement("fragmentShaderFile");
		PEbool hasFragmentShader = fragmentShaderFile != NULL;
		if(fragmentShaderFile)
		{
			std::string filePath(fragmentShaderFile->GetText());
			filePath = "data/" + filePath;
			fragmentShader = compileShader(filePath, pe::graphics::ShaderType::FRAGMENT);
		}
		
		auto geometryShaderFile = shaderRoot->FirstChildElement("geometryShaderFile");
		PEbool hasGeometryShader = geometryShaderFile != NULL;
		if(geometryShaderFile)
		{
			std::string filePath(geometryShaderFile->GetText());
			filePath = "data/" + filePath;
			geometryShader = compileShader(filePath, pe::graphics::ShaderType::GEOMETRY);
		}

		// Creating the shader program
		GLuint shaderProgram = glCreateProgram();
		if(hasVertexShader)
			glAttachShader(shaderProgram, vertexShader);
		if(hasFragmentShader)
			glAttachShader(shaderProgram, fragmentShader);
		if(hasGeometryShader)
			glAttachShader(shaderProgram, geometryShader);

		// Linking the program
		glLinkProgram(shaderProgram);

		GLint linked = 0;
		glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);
		if(linked == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(shaderProgram, maxLength, &maxLength, &infoLog[0]);

			glDeleteProgram(shaderProgram);

			std::string infoLogString(infoLog.begin(), infoLog.end());
			std::cerr << infoLogString << std::endl;

			return nullptr;
		}
	
		if(hasVertexShader)
		{
			glDetachShader(shaderProgram, vertexShader);
			glDeleteShader(vertexShader);
		}
		if(hasFragmentShader)
		{
			glDetachShader(shaderProgram, fragmentShader);
			glDeleteShader(fragmentShader);
		}
		if(hasGeometryShader)
		{
			glDetachShader(shaderProgram, geometryShader);
			glDeleteShader(geometryShader);
		}
	
		return new pe::graphics::Shader(shaderProgram);	
	}

	GLuint ShaderLoader::compileShader(const std::string& path,
									   pe::graphics::ShaderType type)
	{
		std::string shaderSource = getShaderSource(path, type);
		
		GLuint shader;
		if(type == pe::graphics::ShaderType::VERTEX)	
			shader = glCreateShader(GL_VERTEX_SHADER);
		else if(type == pe::graphics::ShaderType::FRAGMENT)
			shader = glCreateShader(GL_FRAGMENT_SHADER);
		else
			return 0;
		
		// Setting the shader source
		const char* shaderCode = shaderSource.c_str();
		glShaderSource(shader, 1, &shaderCode, NULL);
		// Compiling the shader
		glCompileShader(shader);

		// Checking errors
		GLint compiled = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
		if(compiled == GL_FALSE)
		{
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetShaderInfoLog(shader, maxLength, &maxLength, &infoLog[0]);

			glDeleteShader(shader);
			
			std::string infoLogString(infoLog.begin(), infoLog.end());
			std::cerr << infoLogString << std::endl;
		
			// TODO error	
		}
		
		return shader;
		
	}

	std::string ShaderLoader::getShaderSource(const std::string& path,
											  pe::graphics::ShaderType type)
	{
		std::string shaderSource;


		pe::file::File file(path);
		pe::file::TextFileReader fileReader;
		fileReader.openFile(file);
		shaderSource = fileReader.readFile();
		fileReader.closeFile();

		return shaderSource;
	}

	Resource<pe::graphics::Shader> ShaderLoader::loadShaderResource(const std::string& name)
	{
		return Resource<pe::graphics::Shader>(loadShader(name));
	}
}
}
