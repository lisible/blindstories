#include "Resources/FontLoader.hpp"
#include "Resources/ResourceSystem.hpp"
#include "Graphics/Texture.hpp"

#include <tinyxml2.h>

// TODO remove
#include <iostream>
namespace pe
{
namespace resources
{
	FontLoader::FontLoader(ResourceSystem& resourceSystem, TextureLoader& textureLoader)
		: resourceSystem(resourceSystem)
		, textureLoader(textureLoader)
	{
	}

	FontLoader::~FontLoader()
	{
	}

	Resource<pe::graphics::Font> FontLoader::loadFontResource(const std::string& name)
	{
		return Resource<pe::graphics::Font>(loadFont(name));
	}

	pe::graphics::Font* FontLoader::loadFont(const std::string& name)
	{
		std::string resourceFilePath = "data/" + resourceSystem.getResourceFilePath(name);

		tinyxml2::XMLDocument doc;
		doc.LoadFile(resourceFilePath.c_str());
		auto root = doc.FirstChildElement("resource");

		// Checking if the identifier is correct
		std::string identifier(root->FirstChildElement("identifier")->GetText());
		if(identifier != name)
		{
			// Error	
		}

		// Checking if the type is correct
		std::string type(root->FirstChildElement("type")->GetText());
		if(type != "font")
		{
			// Error
		}
		
	
		auto fontRoot = root->FirstChildElement("font");	
		std::string texturePath(fontRoot->FirstChildElement("texture")->GetText());
		texturePath = "data/" + texturePath;
		pe::graphics::Texture* fontTexture = textureLoader.loadTextureFromPath(texturePath);
		
		pe::graphics::Font* font = new pe::graphics::Font(fontTexture);
		

		std::string fontFilePath(fontRoot->FirstChildElement("file")->GetText());
		fontFilePath = "data/" + fontFilePath;	

		tinyxml2::XMLDocument fntDoc;
		fntDoc.LoadFile(fontFilePath.c_str());
		auto fntRoot = fntDoc.FirstChildElement("font");
		auto common = fntRoot->FirstChildElement("common");

		PEuint32 lineHeight = common->IntAttribute("lineHeight");
		font->setLineHeight(lineHeight);
		
		auto charElt = fntRoot->FirstChildElement("chars")->FirstChildElement("char");
		while(charElt != NULL)
		{
			PEchar id = charElt->IntAttribute("id");
			PEuint32 x = charElt->IntAttribute("x");
			PEuint32 y = charElt->IntAttribute("y");
			PEuint32 width = charElt->IntAttribute("width");
			PEuint32 height = charElt->IntAttribute("height");
			PEuint32 xOffset = charElt->IntAttribute("xoffset");
			PEuint32 yOffset = charElt->IntAttribute("yoffset");
			PEuint32 xAdvance = charElt->IntAttribute("xadvance");

			font->addCharacter(pe::graphics::FontCharacter(id,
														   x, y,
														   width, height,
														   xOffset, yOffset,
														   xAdvance));

			charElt = charElt->NextSiblingElement();
		}

		return font;
	}
}
}
