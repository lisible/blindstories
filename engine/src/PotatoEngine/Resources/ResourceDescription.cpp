#include "Resources/ResourceDescription.hpp"
#include "File/ResourceFileReader.hpp"

// TODO remove
#include <iostream>
namespace pe
{
namespace resources
{
	ResourceDescription::ResourceDescription()
		: resourceName("unknown resource")
		, resourceType(ResourceType::UNKNOWN)
	{
	}

	ResourceDescription::~ResourceDescription()
	{
	}

	PEvoid ResourceDescription::setResourceName(const std::string& name)
	{
		resourceName = name;
	}
	const std::string& ResourceDescription::getResourceName() const
	{
		return resourceName;
	}

	PEvoid ResourceDescription::setResourceType(ResourceType type)
	{
		resourceType = type;
	}
	ResourceType ResourceDescription::getResourceType() const
	{
		return resourceType;
	}

	PEvoid ResourceDescription::addResourceParameter(const std::string& name,
													 const std::string& value)
	{
		resourceParameters.insert(std::make_pair(name, value));
	}
	const std::string& ResourceDescription::getResourceParameter(const std::string& parameter) const
	{
		return resourceParameters.at(parameter);
	}
	PEbool ResourceDescription::hasResourceParameter(const std::string& parameter) const
	{
		return resourceParameters.count(parameter) != 0;
	}
	PEbool ResourceDescription::isResourceParameterTrue(const std::string& parameter) const
	{
		if(!hasResourceParameter(parameter))
			return false;

		return getResourceParameter(parameter) == "true";
	}

	ResourceDescription ResourceDescription::getResourceDescription(const std::string& resourceName,
																   ResourceType type)
	{
		ResourceDescription d;


		return d;
	}
}
}
