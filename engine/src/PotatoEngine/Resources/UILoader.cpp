#include "Resources/UILoader.hpp"
#include "Resources/ResourceSystem.hpp"

#include <tinyxml2.h>
#include <stack>
#include <cstring>

// TODO delete
#include <iostream>
namespace pe
{
namespace resources
{
    UILoader::UILoader(ResourceSystem& resourceSystem)
        : resourceSystem(resourceSystem)
        , componentFactory(nullptr)
    {
    }
    UILoader::~UILoader()
    {
    }

    PEvoid UILoader::setComponentFactory(pe::ui::GUIComponentFactory* factory)
    {
        componentFactory = factory;
    }

    Resource<pe::ui::UserInterface> UILoader::loadUIResource(const std::string& name)
    {
        return Resource<pe::ui::UserInterface>(loadUI(name));
    }

    pe::ui::UserInterface* UILoader::loadUI(const std::string& name)
    {
        std::string resourceFilePath = "data/" + resourceSystem.getResourceFilePath(name);

        tinyxml2::XMLDocument doc;
        doc.LoadFile(resourceFilePath.c_str());
        auto root = doc.FirstChildElement("resource");

        // Checking if the identifier is correct
        std::string identifier(root->FirstChildElement("identifier")->GetText());
        if(identifier != name)
        {
            // Error
        }

        // Checking if the type is correct
        std::string type(root->FirstChildElement("type")->GetText());
        if(type != "ui")
        {
            // Error
        }

        // Parsing the ui (depth first search)
        auto uiRoot = root->FirstChildElement("ui")->FirstChild();
        return parseUITree(uiRoot);
    }

    pe::ui::UserInterface* UILoader::parseUITree(tinyxml2::XMLNode* uiRoot)
    {
        using namespace tinyxml2;

        pe::ui::UserInterface* ui = new pe::ui::UserInterface();

        auto child = uiRoot->FirstChild();
        while(child != nullptr)
        {
            parseUIComponent(ui, ui->getRoot(), child);
            child = child->NextSibling();
        }

        return ui;
    }

    PEvoid UILoader::parseUIComponent(pe::ui::UserInterface* ui, pe::ui::GUIComponent* parent, tinyxml2::XMLNode* uiNode)
    {
        using namespace tinyxml2;
        pe::ui::GUIComponent* component = componentFactory->createComponentFromXML(uiNode);
        if(component == nullptr)
            return;

        auto child = uiNode->FirstChild();
        while(child != nullptr)
        {
            parseUIComponent(ui, component, child);
            child = child->NextSibling();
        }

        parent->addChild(component);
        ui->registerComponent(component);
    }
}
}