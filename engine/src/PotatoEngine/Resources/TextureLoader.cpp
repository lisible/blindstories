#include "Resources/TextureLoader.hpp"
#include "File/BinaryFileReader.hpp"
#include "File/ResourceFileReader.hpp"
#include "Utils/String.hpp"
#include "Resources/ResourcePaths.hpp"
#include "Resources/ResourceSystem.hpp"

#include <SDL2/SDL_image.h>
#include <tinyxml2.h>
#include <stdio.h>
#include <iostream>

namespace pe
{
namespace resources
{
	TextureLoader::TextureLoader(ResourceSystem& resourceSystem)
		: resourceSystem(resourceSystem)
	{
	}

	TextureLoader::~TextureLoader()
	{
	}

	Resource<pe::graphics::Texture> TextureLoader::loadTextureResource(const std::string& name)
	{
		return Resource<pe::graphics::Texture>(loadTexture(name));
	}

	pe::graphics::Texture* TextureLoader::loadTexture(const std::string& name)
	{
		std::string resourceFilePath = "data/" + resourceSystem.getResourceFilePath(name);

		tinyxml2::XMLDocument doc;
		doc.LoadFile(resourceFilePath.c_str());
		auto root = doc.FirstChildElement("resource");

		// Checking if the identifier is correct
		std::string identifier(root->FirstChildElement("identifier")->GetText());
		if(identifier != name)
		{
			// Error	
		}

		// Checking if the type is correct
		std::string type(root->FirstChildElement("type")->GetText());
		if(type != "texture")
		{
			// Error
		}

		auto textureRoot = root->FirstChildElement("texture");

		// Fetching the texture format
		std::string format(textureRoot->FirstChildElement("format")->GetText());

		// Fetching the texture file path
		std::string textureFilePath(textureRoot->FirstChildElement("file")->GetText());
	
		return loadTextureFromPath("data/" + textureFilePath); 
	}

	pe::graphics::Texture* TextureLoader::loadTextureFromPath(const std::string& path)
	{

		std::cout << "Loading texture from path " << path << std::endl;
		pe::file::BinaryFileReader binaryFileReader;
		pe::file::File textureFile(path);
		binaryFileReader.openFile(textureFile);
		auto data = binaryFileReader.readFile();
		binaryFileReader.closeFile();	
				
		// SDL Image stuff
		SDL_RWops *rw = SDL_RWFromMem(data.data(), data.size()*sizeof(char));	
		SDL_Surface* temp = IMG_Load_RW(rw, 1);
		if(temp == NULL)
			printf("IMG_Load_RW: %s\n", IMG_GetError());

		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		int mode = GL_RGB;
		if(temp->format->BytesPerPixel == 4)
			mode = GL_RGBA;

		glTexImage2D(GL_TEXTURE_2D, 0, mode, temp->w, temp->h, 
					 0, mode, GL_UNSIGNED_BYTE, temp->pixels);

		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		int w, h;
		int mipLevel(0);
		glGetTexLevelParameteriv(GL_TEXTURE_2D, mipLevel, GL_TEXTURE_WIDTH, &w);
		glGetTexLevelParameteriv(GL_TEXTURE_2D, mipLevel, GL_TEXTURE_HEIGHT, &h);

		return new pe::graphics::Texture(texture, w, h);	
	}
}
}
