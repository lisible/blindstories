#include "Messaging/Messenger.hpp"

// TODO delete
#include <iostream>
namespace pe
{
namespace messaging
{
	Messenger::Messenger()
		: messageBus(nullptr)
	{}

	Messenger::~Messenger()
	{}

	PEvoid Messenger::sendMessage(Message message)
	{
		// @TODO assert messageBus != nullptr
		messageBus->sendMessage(message);
	}

	PEvoid* Messenger::sendDirectMessage(Message message)
	{
		return messageBus->sendDirectMessage(message);
	}

	PEvoid Messenger::setMessageBus(MessageBus& bus)
	{
		messageBus = &bus;
	}

	PEvoid Messenger::registerCallback(PEMsgId identifier, 
									   std::function<PEvoid(PEvoid*)> callbackMethod)
	{
		// @TODO assert there is not already a callback for this type of message	
		callbacks.insert(std::make_pair(identifier, callbackMethod));
	}

	PEvoid Messenger::registerDirectCallback(PEMsgId identifier,
										std::function<PEvoid*(PEvoid*)> callbackMethod)
	{
		directCallbacks.insert(std::make_pair(identifier, callbackMethod));
	}

	PEvoid Messenger::handleMessage(pe::messaging::Message& message)
	{
		// If no callback was set for this message, just ignore it
		if(callbacks.count(message.getIdentifier()) != 0)
			callbacks[message.getIdentifier()](message.getData());
	}

	PEvoid* Messenger::handleDirectMessage(pe::messaging::Message& message)
	{
		if(directCallbacks.count(message.getIdentifier()) != 0)
			return directCallbacks[message.getIdentifier()](message.getData());

		return nullptr;
	}

	PEbool Messenger::canHandleDirectMessage(PEMsgId messageIdentifier) const
	{
		return directCallbacks.find(messageIdentifier) != directCallbacks.end();
	}
}
}
