#include "Messaging/MessageBus.hpp"
#include "Messaging/Messenger.hpp"

// Delete
#include <iostream>
namespace pe
{
namespace messaging
{
	MessageBus::MessageBus()
	{}

	MessageBus::~MessageBus()
	{}

	PEvoid MessageBus::registerMessenger(Messenger* messenger)
	{
		// @TODO assert messenger != nullptr
		// Adding the new messenger to the messenger list
		messengers.push_back(messenger);
		// Sets the message bus of the new messenger to this one
		messenger->setMessageBus(*this);
	}

	PEvoid MessageBus::sendMessage(Message& message)
	{
		messageQueue.push(message);
	}

	PEvoid* MessageBus::sendDirectMessage(Message& message)
	{
		for(auto& messenger : messengers)
		{
			if(messenger->canHandleDirectMessage(message.getIdentifier()))
			{
				return messenger->handleDirectMessage(message);
			}
		}

		return nullptr;
	}

	PEvoid MessageBus::publishMessages()
	{
		// While the queue isn't empty, pops the messages and handle them
		while(!messageQueue.empty())
		{
			auto& message = messageQueue.front();
			for(auto& messenger : messengers)
				messenger->handleMessage(message);

			message.dispose();

			messageQueue.pop();
		}

	}
}
}
