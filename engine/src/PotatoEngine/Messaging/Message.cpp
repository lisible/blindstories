#include "Messaging/Message.hpp"

namespace pe
{
namespace messaging
{
	Message::Message(PEMsgId identifier, PEvoid* data, std::function<PEvoid(PEvoid*)> sentCallback)
		: identifier(identifier)
		, data(data)
		, sentCallback(sentCallback)
	{}

	Message::~Message()
	{}

	PEMsgId Message::getIdentifier() const
	{
		return identifier;
	}
	
	PEvoid* Message::getData()
	{
		return data;
	}

	PEvoid Message::dispose()
	{
		if(sentCallback)	
			sentCallback(data);
	}
}
}
