#include "Logic/LogicSystem.hpp"
#include "Messaging/Messages.hpp"
#include "Messaging/SimpleMessageData.hpp"
#include "Input/InputEvent.hpp"
#include "UI/CallbackData.hpp"
#include "UI/ComponentRegisteringData.hpp"


// TODO remove that
#include <iostream>

namespace pe
{
namespace logic
{
	LogicSystem::LogicSystem()
		: sceneFactory(nullptr)
	{
		registerDefaultHandlers();	
	}

	LogicSystem::~LogicSystem()
	{
		delete sceneFactory;
	}

	PEvoid LogicSystem::setSceneFactory(pe::scene::ISceneFactory* sceneFactory)
	{
		this->sceneFactory = sceneFactory;
	}


	PEvoid LogicSystem::popScene()
	{
		sceneStack.popScene();

		if(!sceneStack.isEmpty())
		{
			auto scene = sceneStack.currentScene();
			// Asking the UI system to use the ui of the scene
			std::string* uiId = new std::string();
			*uiId = scene->getUIIdentifier();
			sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_CHANGEUI_REQUEST,
											   uiId,
											   [&](PEvoid* data) {
												   std::string* strPtr = static_cast<std::string*>(data);
												   delete strPtr;
											   }));
		}
	}
	PEvoid LogicSystem::pushScene(const std::string& identifier)
	{
		// Creating the scene
		pe::scene::Scene* scene = sceneFactory->createScene(identifier);

		// Asking the UI system to use the ui of the scene
		std::string* uiId = new std::string();
		*uiId = scene->getUIIdentifier();
		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_CHANGEUI_REQUEST,
										   uiId,
										   [&](PEvoid* data){
											   std::string* strPtr = static_cast<std::string*>(data);
											   delete strPtr;}));


		// Pushing it onto the stack
		sceneStack.pushScene(scene);

		// Initializing the scene
		scene->setLogicSystem(this);
		scene->onInitialization();
	}
	PEvoid LogicSystem::changeScene(const std::string& identifier)
	{
		popScene();
		pushScene(identifier);
	}

	PEvoid LogicSystem::uiAddComponent(const std::string& parentIdentifier, ui::GUIComponent* component)
	{
        ui::ComponentRegisteringData* data = new ui::ComponentRegisteringData();
        data->parentIdentifier = parentIdentifier;
        data->component = component;



		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_ADDUICOMPONENT_REQUEST,
                                            data,
                                            [=](PEvoid*){delete data;}));
	}

	PEvoid LogicSystem::changeUIAttribute(const std::string& componentIdentifier,
										  const std::string& attribute,
										  const std::string& value)
	{
		pe::messaging::SimpleMessageData* messageData = new pe::messaging::SimpleMessageData();
		messageData->stringValue1 = componentIdentifier;
		messageData->stringValue2 = attribute;
		messageData->stringValue3 = value;

		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_CHANGEUIATTRIBUTE_REQUEST,
										   messageData,
										   [=](PEvoid*){delete messageData;}));
	}

	PEvoid LogicSystem::logMessage(const std::string& message)
	{
		std::string* messageData = new std::string(message);
		sendMessage(pe::messaging::Message(pe::messaging::MSG_LOGGING_LOG,
										   messageData,
										   [=](PEvoid*){delete messageData;}));
	}

	PEvoid LogicSystem::uiFocusNext()
	{
		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_FOCUSNEXT_REQUEST));
	}
	PEvoid LogicSystem::uiFocusPrevious()
	{
		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_FOCUSPREVIOUS_REQUEST));
	}
	PEvoid LogicSystem::uiPost()
	{
		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_POST_REQUEST));
	}

	PEvoid LogicSystem::ttsSay(const std::string& text)
	{
		std::string* textData = new std::string(text);
		sendMessage(pe::messaging::Message(pe::messaging::MSG_TTS_PRONOUNCE_REQUEST,
										   textData,
										   [=](PEvoid*){delete textData;}));
	}

    PEvoid LogicSystem::tick()
	{
		// If there is a scene, "tick it"
		if(!sceneStack.isEmpty())
			sceneStack.currentScene()->onTick();
		else
			sendMessage(pe::messaging::Message(pe::messaging::MSG_APPLICATION_STOP_REQUEST));
	}

	PEvoid LogicSystem::registerUIClickCallback(const std::string& componentIdentifier, std::function<PEvoid(PEvoid)> callback)
	{
		pe::ui::CallbackData* data = new pe::ui::CallbackData();
		data->componentIdentifier = componentIdentifier;
		data->callbackFunction = callback;

		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_REGISTERCLICKCALLBACK, data,
															   [=](PEvoid*){delete data;}));
	}
	PEvoid LogicSystem::registerUIPostCallback(const std::string& componentIdentifier, std::function<PEvoid(PEvoid)> callback)
	{
		pe::ui::CallbackData* data = new pe::ui::CallbackData();
		data->componentIdentifier = componentIdentifier;
		data->callbackFunction = callback;

		sendMessage(pe::messaging::Message(pe::messaging::MSG_UI_REGISTERPOSTCALLBACK, data,
															   [=](PEvoid*){delete data;}));
	}

	PEvoid LogicSystem::render()
	{

	}

	PEvoid LogicSystem::onPushSceneRequest(PEvoid* data)
	{
		std::string* sceneIdentifier = static_cast<std::string*>(data);
		pushScene(*sceneIdentifier);
	}

	PEvoid LogicSystem::onPopSceneRequest()
	{
		popScene();
	}

	PEvoid LogicSystem::onActionPressed(PEvoid* action)
	{
		std::string* actionString = static_cast<std::string*>(action);
		if(sceneStack.currentScene())
            sceneStack.currentScene()->onActionPressed(*actionString);
	}

	PEvoid LogicSystem::onTextTyped(PEvoid* text)
	{
		std::string* textString  = static_cast<std::string*>(text);
		sceneStack.currentScene()->onTextTyped(*textString);
	}

	PEvoid LogicSystem::registerDefaultHandlers()
	{
		// On Update request, tick
		registerCallback(pe::messaging::MSG_UPDATE_REQUEST,
						[&](PEvoid* p){tick();});

		// On Render request, render stuff
		registerCallback(pe::messaging::MSG_RENDER_REQUEST,
						[&](PEvoid* p){render();});

		// On push scene request, register scene
		registerCallback(pe::messaging::MSG_LOGIC_PUSHSCENE_REQUEST,
						 [&](PEvoid* p){onPushSceneRequest(p);});	
		// On pop scene request, pop scene
		registerCallback(pe::messaging::MSG_LOGIC_POPSCENE_REQUEST,
						 [&](PEvoid* p){onPopSceneRequest();});
		// On action pressed
		registerCallback(pe::messaging::MSG_INPUT_ACTIONPRESSED,
						[&](PEvoid* p){onActionPressed(p);});
		// On text typed
		registerCallback(pe::messaging::MSG_INPUT_TEXTTYPED,
						 [&](PEvoid* p){onTextTyped(p);});
	}

}
}
