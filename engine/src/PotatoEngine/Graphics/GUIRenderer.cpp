#include "Graphics/GUIRenderer.hpp"
#include "Graphics/GraphicSystem.hpp"
#include "UI/Anchor.hpp"

// TODO remove that
#include <iostream>
#include <cstdlib>
namespace pe
{
namespace graphics
{
	GUIRenderer::GUIRenderer(GraphicSystem* system, Renderer2D& renderer2D)
		: renderer(renderer2D)
		, system(system)
	{}

	GUIRenderer::~GUIRenderer()
	{}

	PEvoid GUIRenderer::resizeViewport(PEuint32 viewportWidth, PEuint32 viewportHeight)
	{
		viewport.setWidth(viewportWidth);
		viewport.setHeight(viewportHeight);
	}

	PEvoid GUIRenderer::draw(pe::ui::UserInterface& ui)
	{
		pem::mat4 identity = pem::mat4(1);
		drawComponent(*ui.getRoot(), identity);
	}

	PEvoid GUIRenderer::drawComponent(pe::ui::GUIComponent& component, const pem::mat4& transform)
	{
		/* TODO handle that
		 * switch(component.getAnchor())
		{
			case pe::ui::Anchor::NORTH:
				component.setPosition()
				break;
			case pe::ui::Anchor::NORTH_EAST:
				break;
			case pe::ui::Anchor::EAST:
				break;
			case pe::ui::Anchor::SOUTH_EAST:
				break;
			case pe::ui::Anchor::SOUTH:
				break;
			case pe::ui::Anchor::SOUTH_WEST:
				break;
			case pe::ui::Anchor::WEST:
				break;

			default:
				break;
		}*/

		component.render(renderer, transform);
		pem::mat4 newTransform = transform * component.getTransformMatrix();
		for(auto& child : component.getChildren())
		{
			drawComponent(*child.second, newTransform);
		}
	}
}
}
