#include "Graphics/GraphicSystem.hpp"
#include "Messaging/Messages.hpp"
#include "UI/UserInterface.hpp"


// TODO remove
#include <iostream>
namespace pe
{
namespace graphics
{
	GraphicSystem::GraphicSystem()
		: renderer2D(this, lowLevelRenderer)
		, guiRenderer(this, renderer2D)
	{
		registerDefaultHandlers();
	}

	GraphicSystem::~GraphicSystem()
	{
	}

	PEvoid GraphicSystem::setCamera(OrthographicCamera& camera)
	{
		lowLevelRenderer.setCamera(camera);
	}

	Texture* GraphicSystem::getTexture(const std::string& resourceName)
	{
		namespace pemsg = pe::messaging;
		std::string textureName = resourceName;
		auto texturePtr = sendDirectMessage(pemsg::Message(pemsg::MSG_RESOURCES_GETTEXTURE_DREQUEST, &textureName));
	
		Texture* texture = static_cast<Texture*>(texturePtr);	
		return texture;
	}

	Font* GraphicSystem::getFont(const std::string& resourceName)
	{
		namespace pemsg = pe::messaging;
		std::string fontName = resourceName;
		auto fontPtr = sendDirectMessage(pemsg::Message(pemsg::MSG_RESOURCES_GETFONT_DREQUEST, &fontName));		

		Font* font = static_cast<Font*>(fontPtr);

		return font;
	}
	
	Shader* GraphicSystem::getShader(const std::string& resourceName)
	{
		namespace pemsg = pe::messaging;
		std::string shaderName = resourceName;
		auto shaderPtr = sendDirectMessage(pemsg::Message(pemsg::MSG_RESOURCES_GETSHADER_DREQUEST, &shaderName));		

		Shader* shader = static_cast<Shader*>(shaderPtr);

		return shader;
	}

	PEvoid GraphicSystem::onRenderRequest(PEvoid* data)
	{
		lowLevelRenderer.prepareRendering();
		lowLevelRenderer.render();
		lowLevelRenderer.finishRendering();
	}

	PEvoid GraphicSystem::onRenderUIRequest(PEvoid* data)
	{
		pe::ui::UserInterface* ui = static_cast<pe::ui::UserInterface*>(data);	

		// Setting the shader
		namespace pemsg = pe::messaging;


		// TODO change ?
		std::string shaderName("testShader");
		auto shaderValue = sendDirectMessage(pemsg::Message(pemsg::MSG_RESOURCES_GETSHADER_DREQUEST, &shaderName));
		Shader* shader = static_cast<Shader*>(shaderValue);
		lowLevelRenderer.useShader(*shader);	

		guiRenderer.draw(*ui);
	}

	PEvoid GraphicSystem::onInitializationRequest(PEvoid* data)
	{
		lowLevelRenderer.initialize();
	}

	PEvoid GraphicSystem::registerDefaultHandlers()
	{
		registerCallback(pe::messaging::MSG_GRAPHICS_RENDERUI,
						[&](PEvoid* data){onRenderUIRequest(data);});

		registerCallback(pe::messaging::MSG_GRAPHICS_INITIALIZE,
						[&](PEvoid* data){onInitializationRequest(data);});

		registerCallback(pe::messaging::MSG_GRAPHICS_RENDER,
						[&](PEvoid* data){onRenderRequest(data);});
	}
}
}
