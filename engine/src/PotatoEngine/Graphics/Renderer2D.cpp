#include "Graphics/Renderer2D.hpp"
#include "Graphics/TexturedQuad.hpp"
#include "Graphics/GraphicSystem.hpp"

// TODO remove
#include <iostream>
namespace pe
{
namespace graphics
{
	Renderer2D::Renderer2D(GraphicSystem* system, LowLevelRenderer& lowLevelRenderer)
		: lowLevelRenderer(lowLevelRenderer)
		, system(system)
	{
	}

	Renderer2D::~Renderer2D()
	{
	}


	PEvoid Renderer2D::drawPoint(const Point& point, const Color& color, const pem::mat4& transform)
	{
		Vertex v(point.x, point.y, 0.f, color);

		// TODO change that
		lowLevelRenderer.useShader(*system->getShader("testShader"));

		lowLevelRenderer.drawVertex(v, transform);
	}
	PEvoid Renderer2D::drawLine(const Line& line, const Color& color, const pem::mat4& transform)
	{
		Vertex v1(line.firstPoint.x, line.firstPoint.y, 0.f, color);
		Vertex v2(line.secondPoint.x, line.secondPoint.y, 0.f, color);

		// TODO change that
		lowLevelRenderer.useShader(*system->getShader("testShader"));

		lowLevelRenderer.drawLine(v1, v2, transform);
	}
	PEvoid Renderer2D::drawRectangle(const pem::Rectangle& rectangle, const Color& color, const pem::mat4& transform)
	{
        this->lowLevelRenderer.useShader(*system->getShader("testShader"));
		Quad q;

		// Top left corner of the rectangle
		q.topLeft.position.x = rectangle.x;
		q.topLeft.position.y = rectangle.y;
		q.topLeft.position.z = 0.f;
		q.topLeft.color = color;
		q.topLeft.textureCoordinates.x = 0.f;
		q.topLeft.textureCoordinates.y = 0.f;

		// Top right corner of the rectangle
		q.topRight.position.x = rectangle.x + rectangle.width;
		q.topRight.position.y = rectangle.y;
		q.topRight.position.z = 0.f;
		q.topRight.color = color;
		q.topRight.textureCoordinates.x = 0.f;
		q.topRight.textureCoordinates.y = 0.f;

		// Bottom right corner of the rectangle
		q.bottomRight.position.x = rectangle.x + rectangle.width;
		q.bottomRight.position.y = rectangle.y + rectangle.height;
		q.bottomRight.position.z = 0.f;
		q.bottomRight.color = color;
		q.bottomRight.textureCoordinates.x = 0.f;
		q.bottomRight.textureCoordinates.y = 0.f;

		// Bottom left corner of the rectangle
		q.bottomLeft.position.x = rectangle.x;
		q.bottomLeft.position.y = rectangle.y + rectangle.height;
		q.bottomLeft.position.z = 0.f;
		q.bottomLeft.color = color;
		q.bottomLeft.textureCoordinates.x = 0.f;
		q.bottomLeft.textureCoordinates.y = 0.f;

		lowLevelRenderer.drawQuad(q, transform);
	}
	PEvoid Renderer2D::drawImage(const std::string& textureIdentifier, const pem::Rectangle& sourceRectangle,
								 const pem::Rectangle& destinationRectangle, const Color& color,
								 const pem::mat4& transform)
	{
		TexturedQuad q;
		Texture* texture = system->getTexture(textureIdentifier);
		auto textureSize = texture->getSize();
		q.textureIdentifier = texture->getIdentifier();

		// Top left corner of the image
		q.quad.topLeft.position.x = destinationRectangle.x;
		q.quad.topLeft.position.y = destinationRectangle.y;
		q.quad.topLeft.position.z = 0.f;
		q.quad.topLeft.textureCoordinates.x = sourceRectangle.x/textureSize.x;
		q.quad.topLeft.textureCoordinates.y = sourceRectangle.y/textureSize.y;
		q.quad.topLeft.color = color;

		// Top right corner of the image
		q.quad.topRight.position.x = destinationRectangle.x + destinationRectangle.width;
		q.quad.topRight.position.y = destinationRectangle.y;
		q.quad.topRight.position.z = 0.f;
		q.quad.topRight.textureCoordinates.x = (sourceRectangle.x + sourceRectangle.width)/textureSize.x;
		q.quad.topRight.textureCoordinates.y = sourceRectangle.y/textureSize.y;
		q.quad.topRight.color = color;

		// Bottom right corner of the image
		q.quad.bottomRight.position.x = destinationRectangle.x + destinationRectangle.width;
		q.quad.bottomRight.position.y = destinationRectangle.y + destinationRectangle.height;
		q.quad.bottomRight.position.z = 0.f;
		q.quad.bottomRight.textureCoordinates.x = (sourceRectangle.x + sourceRectangle.width)/textureSize.x;
		q.quad.bottomRight.textureCoordinates.y = (sourceRectangle.y + sourceRectangle.height)/textureSize.y;
		q.quad.bottomRight.color = color;

		// Bottom left corner of the image
		q.quad.bottomLeft.position.x = destinationRectangle.x;
		q.quad.bottomLeft.position.y = destinationRectangle.y + destinationRectangle.height;
		q.quad.bottomLeft.position.z = 0.f;
		q.quad.bottomLeft.textureCoordinates.x = sourceRectangle.x/textureSize.x;
		q.quad.bottomLeft.textureCoordinates.y = (sourceRectangle.y + sourceRectangle.height)/textureSize.y;
		q.quad.bottomLeft.color = color;

		lowLevelRenderer.drawTexturedQuad(q, transform);
	}
	PEvoid Renderer2D::drawText(const std::string& text, const std::string& fontIdentifier, PEfloat x, PEfloat y,
								PEuint32 fontSize, const Color& color, const pem::mat4& transform)
	{
		Font* fontPtr = system->getFont(fontIdentifier);
		if(fontPtr == nullptr)
		{
			// TODO error handling
			// system.error("null font") ??
			std::cout << "null font" << std::endl;
			return;
		}

		Texture* texture = fontPtr->getTexture();

		PEuint32 startX = x;
		PEuint32 startY = y;
		for(char c : text)
		{
			const FontCharacter& character = fontPtr->getFontCharacter(c);
			auto characterCoords = character.getPosition();
			auto characterSize = character.getSize();

			TexturedQuad q;
			q.textureIdentifier = texture->getIdentifier();
			pem::vec2 textureSize(texture->getSize().x, texture->getSize().y);

			// topLeft
			q.quad.topLeft.position.x = startX + character.getOffset().x;
			q.quad.topLeft.position.y = startY + character.getOffset().y;
			q.quad.topLeft.position.z = 0.0f;
			q.quad.topLeft.color = color;
			q.quad.topLeft.textureCoordinates.x = characterCoords.x/textureSize.x;
			q.quad.topLeft.textureCoordinates.y = characterCoords.y/textureSize.y;
			// topRight
			q.quad.topRight.position.x = startX + character.getOffset().x + characterSize.x;
			q.quad.topRight.position.y = startY + character.getOffset().y;
			q.quad.topRight.position.z = 0.0f;
			q.quad.topRight.color = color;
			q.quad.topRight.textureCoordinates.x = (characterCoords.x + characterSize.x)/textureSize.x;
			q.quad.topRight.textureCoordinates.y = characterCoords.y/textureSize.y;
			// bottomRight
			q.quad.bottomRight.position.x = startX + character.getOffset().x + characterSize.x;
			q.quad.bottomRight.position.y = startY + character.getOffset().y + characterSize.y;
			q.quad.bottomRight.position.z = 0.0f;
			q.quad.bottomRight.color = color;
			q.quad.bottomRight.textureCoordinates.x = (characterCoords.x + characterSize.x)/textureSize.x;
			q.quad.bottomRight.textureCoordinates.y = (characterCoords.y + characterSize.y)/textureSize.y;
			// bottomLeft
			q.quad.bottomLeft.position.x = startX + character.getOffset().x;
			q.quad.bottomLeft.position.y = startY + character.getOffset().y + characterSize.y;
			q.quad.bottomLeft.position.z = 0.0f;
			q.quad.bottomLeft.color = color;
			q.quad.bottomLeft.textureCoordinates.x = characterCoords.x/textureSize.x;
			q.quad.bottomLeft.textureCoordinates.y = (characterCoords.y + characterSize.y)/textureSize.y;
			// TODO change that
			lowLevelRenderer.useShader(*system->getShader("textShader"));
			lowLevelRenderer.drawTexturedQuad(q, transform);

			startX += character.getXAdvance();
		}
	}

	PEvoid Renderer2D::drawTextArea(const std::string& text,
						const std::string& fontName,
						PEuint32 width,
						PEuint32 height,
						Color color,
						const pem::mat4& transformMatrix)
	{
		Font* fontPtr = system->getFont(fontName);
		if(fontPtr == nullptr)
			std::cout << "font null" << std::endl;
		Texture* texture = fontPtr->getTexture();

		PEuint32 startX(0);
		PEuint32 startY(0);
		for(char c : text)
		{
            if(c == '\n')
            {
                startX = 0;
                startY += fontPtr->getLineHeight();
                continue;
            }
			const FontCharacter& character = fontPtr->getFontCharacter(c);
			auto characterCoords = character.getPosition();
			auto characterSize = character.getSize();

			if(startX + character.getXAdvance() > width)
			{
				startX = 0;
				startY += fontPtr->getLineHeight();
			}

            if(startY > height)
                break;



			TexturedQuad q;
			q.textureIdentifier = texture->getIdentifier();
			pem::vec2 textureSize(texture->getSize().x, texture->getSize().y);

			// topLeft
			q.quad.topLeft.position.x = startX + character.getOffset().x;
			q.quad.topLeft.position.y = startY + character.getOffset().y;
			q.quad.topLeft.position.z = 0.0f;
			q.quad.topLeft.color = color;
			q.quad.topLeft.textureCoordinates.x = characterCoords.x/textureSize.x;
			q.quad.topLeft.textureCoordinates.y = characterCoords.y/textureSize.y;
			// topRight
			q.quad.topRight.position.x = startX + character.getOffset().x + characterSize.x;
			q.quad.topRight.position.y = startY + character.getOffset().y;
			q.quad.topRight.position.z = 0.0f;
			q.quad.topRight.color = color;
			q.quad.topRight.textureCoordinates.x = (characterCoords.x + characterSize.x)/textureSize.x;
			q.quad.topRight.textureCoordinates.y = characterCoords.y/textureSize.y;
			// bottomRight
			q.quad.bottomRight.position.x = startX + character.getOffset().x + characterSize.x;
			q.quad.bottomRight.position.y = startY + character.getOffset().y + characterSize.y;
			q.quad.bottomRight.position.z = 0.0f;
			q.quad.bottomRight.color = color;
			q.quad.bottomRight.textureCoordinates.x = (characterCoords.x + characterSize.x)/textureSize.x;
			q.quad.bottomRight.textureCoordinates.y = (characterCoords.y + characterSize.y)/textureSize.y;
			// bottomLeft
			q.quad.bottomLeft.position.x = startX + character.getOffset().x;
			q.quad.bottomLeft.position.y = startY + character.getOffset().y + characterSize.y;
			q.quad.bottomLeft.position.z = 0.0f;
			q.quad.bottomLeft.color = color;
			q.quad.bottomLeft.textureCoordinates.x = characterCoords.x/textureSize.x;
			q.quad.bottomLeft.textureCoordinates.y = (characterCoords.y + characterSize.y)/textureSize.y;

			/*q.quad.topLeft = q.quad.topLeft.transform(transformMatrix);
			q.quad.topRight = q.quad.topRight.transform(transformMatrix);
			q.quad.bottomRight = q.quad.bottomRight.transform(transformMatrix);
			q.quad.bottomLeft = q.quad.bottomLeft.transform(transformMatrix);*/

			lowLevelRenderer.useShader(*system->getShader("textShader"));
			lowLevelRenderer.drawTexturedQuad(q, transformMatrix);

			startX += character.getXAdvance();
		}
	}

}
}
