#include "Graphics/Viewport.hpp"

namespace pe
{
namespace graphics
{
	const PEuint32 Viewport::DEFAULT_WIDTH = 800.f;
	const PEuint32 Viewport::DEFAULT_HEIGHT = 600.f;


	Viewport::Viewport()
		: width(DEFAULT_WIDTH)
		, height(DEFAULT_HEIGHT)
	{}

	Viewport::Viewport(PEuint32 viewportWidth, PEuint32 viewportHeight)
		: width(viewportWidth)
		, height(viewportHeight)
	{}

	Viewport::~Viewport()
	{}

	PEvoid Viewport::setWidth(PEuint32 viewportWidth)
	{
		width = viewportWidth;	
	}
	PEuint32 Viewport::getWidth() const
	{
		return width;
	}

	PEvoid Viewport::setHeight(PEuint32 viewportHeight)
	{
		height = viewportHeight;
	}
	PEuint32 Viewport::getHeight() const
	{
		return height;
	}
}
}
