#include "Graphics/Shader.hpp"

namespace pe
{
namespace graphics
{
	Shader::Shader(GLuint identifier)
		: shaderIdentifier(identifier)
	{}

	Shader::~Shader()
	{
		glDeleteProgram(shaderIdentifier);
	}

	GLuint Shader::getIdentifier() const
	{
		return shaderIdentifier;
	}
}
}
