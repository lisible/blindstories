#include "Graphics/RectangleShape.hpp"

namespace pe
{
namespace graphics
{
	RectangleShape::RectangleShape()
		: color(1.f, 1.f, 1.f)
		, size(1.f, 1.f)
	{
	}

	RectangleShape::~RectangleShape()
	{}

	Color RectangleShape::getColor() const
	{
		return color;
	}
	PEvoid RectangleShape::setColor(const Color& color)
	{
		this->color = color;
	}

	PEvoid RectangleShape::setSize(PEfloat width, PEfloat height)
	{
		size.x = width;
		size.y = height;
	}
	const pem::vec2& RectangleShape::getSize() const
	{
		return size;
	}
}
}
