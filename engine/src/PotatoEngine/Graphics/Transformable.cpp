#include "Graphics/Transformable.hpp"
#include <iostream>

namespace pe
{
namespace graphics
{
	Transformable::Transformable()
		: position(0.f, 0.f)
		, scale(1.f, 1.f)
		, angle(0.f, 0.f, 0.f, 0.f)
		, origin(0.f, 0.f)
		, transformMatrix(1)
		, inverseTransformMatrix(1)
		, needTransformMatrixUpdate(false)
		, needInverseTransformMatrixUpdate(false)
	{
	}

	Transformable::~Transformable()
	{
	}

	PEvoid Transformable::setPosition(PEfloat x, PEfloat y)
	{
		position.x = x;
		position.y = y;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	PEvoid Transformable::move(PEfloat x, PEfloat y)
	{
		position.x += x;
		position.y += y;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	const pem::vec2& Transformable::getPosition() const
	{
		return position;
	}

	PEvoid Transformable::setScale(PEfloat x, PEfloat y)
	{
		scale.x = x;
		scale.y = y;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	PEvoid Transformable::relScale(PEfloat x, PEfloat y)
	{
		scale.x += x;
		scale.y += y;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	const pem::vec2& Transformable::getScale() const
	{
		return scale;
	}

	PEvoid Transformable::setAngle(PEfloat angle)
	{
		this->angle.x = 0.f;
		this->angle.y = 0.f;
		this->angle.z = 1.f;
		this->angle.w = angle;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	PEvoid Transformable::rotate(PEfloat angle)
	{
		this->angle.x = 0.f;
		this->angle.y = 0.f;
		this->angle.z = 1.f;
		this->angle.w += angle;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	PEfloat Transformable::getAngle() const
	{
		return angle.w;
	}

	PEvoid Transformable::setOrigin(PEfloat x, PEfloat y)
	{
		origin.x = x;
		origin.y = y;

		needTransformMatrixUpdate = true;
		needInverseTransformMatrixUpdate = true;
	}
	const pem::vec2& Transformable::getOrigin() const
	{
		return origin;
	}

	pem::mat4& Transformable::getTransformMatrix()
	{
		if(needTransformMatrixUpdate)
		{
			transformMatrix = pem::mat4::identity();
			transformMatrix = pem::mat4::translationMatrix(position.x, position.y, 0.f) *
							  pem::mat4::scaleMatrix(scale.x, scale.y, 1.0f)  *
							  pem::mat4::rotationMatrix(angle.w, pem::vec3(angle.x, angle.y, angle.z), pem::vec3(origin.x, origin.y, 0.f)) *
							  transformMatrix;
			//pem::mat4::translationMatrix(position.x, position.y, 0.0f) * transformMatrix;

			needTransformMatrixUpdate = false;
		}

		return transformMatrix;
	}
	pem::mat4& Transformable::getInverseTransformMatrix() 
	{
		if(needInverseTransformMatrixUpdate)
		{
			inverseTransformMatrix = getTransformMatrix().invert();
			needInverseTransformMatrixUpdate = false;
		}

		return inverseTransformMatrix;
	}
}
}
