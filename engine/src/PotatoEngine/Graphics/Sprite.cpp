#include "Graphics/Sprite.hpp"

// TODO delete
#include <iostream>

namespace pe
{
namespace graphics
{
	Sprite::Sprite()
		: size(32.f, 32.f)
	{
	}

	Sprite::~Sprite()
	{
	}

	PEvoid Sprite::setTexture(const std::string& texture)
	{
		this->texture = texture;
	}
	const std::string& Sprite::getTexture() const
	{
		return texture;
	}

	PEvoid Sprite::setTextureRectangle(PEfloat x, PEfloat y,
									   PEfloat width, PEfloat height)
	{
		textureRectangle.x = x;
		textureRectangle.y = y;
		textureRectangle.width = width;
		textureRectangle.height = height;
	}
	const pem::Rectangle& Sprite::getTextureRectangle() const
	{
		return textureRectangle;
	}

	PEvoid Sprite::setSize(PEfloat width, PEfloat height)
	{
		size.x = width;
		size.y = height;
	}
	const pem::vec2& Sprite::getSize() const
	{
		return size;
	}
}
}
