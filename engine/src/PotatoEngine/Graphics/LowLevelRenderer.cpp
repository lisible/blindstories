#include "Graphics/LowLevelRenderer.hpp"
#include <GL/glew.h>


//TODO remove that shit
#include <iostream>

namespace pe
{
namespace graphics
{
	LowLevelRenderer::LowLevelRenderer()
	{
	}

	LowLevelRenderer::~LowLevelRenderer()
	{}

	PEvoid LowLevelRenderer::initialize()
	{
	}

	PEvoid LowLevelRenderer::setCamera(OrthographicCamera& camera)
	{
		this->camera = camera;
	}

	PEvoid LowLevelRenderer::prepareRendering()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		static GLuint quadIndices[] = {0, 1, 2, 0, 2, 3};

		// Initializing batches buffers
		for(auto& batch : batches)
		{
			glBindVertexArray(batch.vao);
			
			// Filling vertex buffer
			glBindBuffer(GL_ARRAY_BUFFER, batch.vbo);	
			glBufferData(GL_ARRAY_BUFFER, batch.vertices.size()*sizeof(Vertex), 
											&batch.vertices[0], GL_STATIC_DRAW);

			if(batch.type == LowLevelBatchType::QUAD)
			{
				// Filling index buffer
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch.ebo);

				std::vector<GLuint> indices;
				for(PEsize_t i = 0; i < batch.vertices.size()/4; i++)
				{
					for(PEsize_t j = 0; j < 6; j++)
					{
						indices.push_back(quadIndices[j]+i*4);
					}
				}
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*(batch.vertices.size()/4)*sizeof(GLuint), 
																	&indices[0], GL_STATIC_DRAW);	
			}

			// First attribute is vertex position (3*GLfloat)
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
					reinterpret_cast<void*>(offsetof(Vertex, position)));
			// Second attribute is vertex color (3*GLfloat)
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 
					reinterpret_cast<void*>(offsetof(Vertex, color)));
			// Third attribute is vertex texture coordinates
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 
					reinterpret_cast<void*>(offsetof(Vertex, textureCoordinates)));

			// Enabling attributes
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);		

			glBindVertexArray(0);

		}
	}

	PEvoid LowLevelRenderer::render()
	{
		//std::cout << "Rendering " << batches.size() << " batches..." << std::endl;

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		
		// Drawing each batch
		for(auto& batch : batches)
		{
			// Setting the shader
			glUseProgram(batch.shader);

			// Sending the batch's mvp matrix to the shader
			GLuint mvpLocation = glGetUniformLocation(this->currentShader, "mvp");
			glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, batch.mvp.value_ptr());

			GLuint useTextureLocation = glGetUniformLocation(this->currentShader, "useTexture");
			if(batch.texture != 0)
			{
				glUniform1i(useTextureLocation, GL_TRUE);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, batch.texture);				
			}
			else
				glUniform1i(useTextureLocation, GL_FALSE);
		
			glBindVertexArray(batch.vao);


	
			if(batch.type == LowLevelBatchType::QUAD)
			{
				// Quads use indices
				glDrawElements(GL_TRIANGLES, 6*(batch.vertices.size()/4), GL_UNSIGNED_INT, 0);
			}
			else if(batch.type == LowLevelBatchType::LINE)
			{
				glDrawArrays(GL_LINES, 0, batch.vertices.size());
			}
			else
			{
				glDrawArrays(GL_POINTS, 0, batch.vertices.size());
			}
		}
		GLenum err;
		while((err = glGetError()) != GL_NO_ERROR)
		{
			std::cout << "pb: " << err << std::endl;
		}
	}

	PEvoid LowLevelRenderer::finishRendering()
	{
		for(auto& batch : batches)
		{
			glDeleteVertexArrays(1, &batch.vao);
			glDeleteBuffers(1, &batch.vbo);
			glDeleteBuffers(1, &batch.ebo);
		}

		batches.clear();
	}

	PEvoid LowLevelRenderer::useShader(const Shader& shader)
	{
		currentShader = shader.getIdentifier();
	}

	PEvoid LowLevelRenderer::drawVertex(const Vertex& v, const pem::mat4& transformMatrix)
	{
		PEsize_t batchCount = batches.size();
		pem::mat4 mvp = camera.getProjectionMatrix() * transformMatrix;

		// Adding a new batch
		if(batchCount == 0 || 
		   batches[batchCount-1].type != LowLevelBatchType::VERTEX ||
		   batches[batchCount-1].mvp != mvp ||
		   batches[batchCount-1].shader != currentShader)
		{
			LowLevelBatch newBatch;

			// Initializing the new batch
			newBatch.shader = currentShader;
			newBatch.type = LowLevelBatchType::VERTEX;
			newBatch.mvp = mvp;
			glGenVertexArrays(1, &newBatch.vao);
			glGenBuffers(1, &newBatch.vbo);
			// No need for an ebo

			batches.push_back(newBatch);
		}

		auto& lastBatch = batches[batches.size()-1];

		lastBatch.vertices.push_back(v);
	}

	PEvoid LowLevelRenderer::drawLine(const Vertex& a, const Vertex& b, const pem::mat4& transformMatrix)
	{
		PEsize_t batchCount = batches.size();
		pem::mat4 mvp = camera.getProjectionMatrix() * transformMatrix;

		// Adding a new batch
		if(batchCount == 0 || 
		   batches[batchCount-1].type != LowLevelBatchType::LINE ||
		   batches[batchCount-1].mvp != mvp ||
		   batches[batchCount-1].shader != currentShader)
		{
			LowLevelBatch newBatch;

			// Initializing the new batch
			newBatch.shader = currentShader;
			newBatch.type = LowLevelBatchType::LINE;
			glGenVertexArrays(1, &newBatch.vao);
			glGenBuffers(1, &newBatch.vbo);
			// No need for an ebo

			batches.push_back(newBatch);
		}

		auto& lastBatch = batches[batches.size()-1];

		lastBatch.vertices.push_back(a);
		lastBatch.vertices.push_back(b);
	}

	PEvoid LowLevelRenderer::drawQuad(const Quad& q, const pem::mat4& transformationMatrix)
	{
		PEsize_t batchCount = batches.size();
		pem::mat4 mvp = camera.getProjectionMatrix()*transformationMatrix;
	
		// Adding a new batch
		if(batchCount == 0 || 
		   batches[batchCount-1].type != LowLevelBatchType::QUAD ||
		   batches[batchCount-1].mvp != mvp ||
		   batches[batchCount-1].texture != 0 ||
		   batches[batchCount-1].shader != currentShader)
		{
			LowLevelBatch newBatch;

			// Initializing the new batch
			newBatch.shader = currentShader;
			newBatch.type = LowLevelBatchType::QUAD;
			newBatch.mvp = mvp;
			glGenVertexArrays(1, &newBatch.vao);
			glGenBuffers(1, &newBatch.vbo);
			glGenBuffers(1, &newBatch.ebo);

			batches.push_back(newBatch);
		}

		auto& lastBatch = batches[batches.size()-1];

		lastBatch.vertices.push_back(q.topLeft);
		lastBatch.vertices.push_back(q.topRight);
		lastBatch.vertices.push_back(q.bottomRight);
		lastBatch.vertices.push_back(q.bottomLeft);

	}

	PEvoid LowLevelRenderer::drawTexturedQuad(const TexturedQuad& q, const pem::mat4& transformationMatrix)
	{
		PEsize_t batchCount = batches.size();
		pem::mat4 mvp = camera.getProjectionMatrix()*transformationMatrix;

		// Adding a new batch
		if(batchCount == 0 || 
		   batches[batchCount-1].type != LowLevelBatchType::QUAD ||
		   batches[batchCount-1].mvp != mvp ||
		   batches[batchCount-1].texture != q.textureIdentifier ||
		   batches[batchCount-1].shader != currentShader)
		{
			LowLevelBatch newBatch;

			// Initializing the new batch
			newBatch.shader = currentShader;
			newBatch.type = LowLevelBatchType::QUAD;
			newBatch.mvp = mvp;
			newBatch.texture = q.textureIdentifier;
			glGenVertexArrays(1, &newBatch.vao);
			glGenBuffers(1, &newBatch.vbo);
			glGenBuffers(1, &newBatch.ebo);

			batches.push_back(newBatch);
		}
		
		auto& lastBatch = batches[batches.size()-1];

		lastBatch.vertices.push_back(q.quad.topLeft);
		lastBatch.vertices.push_back(q.quad.topRight);
		lastBatch.vertices.push_back(q.quad.bottomRight);
		lastBatch.vertices.push_back(q.quad.bottomLeft);
	}
}
}
