#include "Graphics/Font.hpp"

namespace pe
{
namespace graphics
{
	Font::Font(Texture* texture)
		: texture(texture)
	{
	}

	Font::~Font()
	{
		delete texture;
	}

	Texture* Font::getTexture() const
	{
		return texture;	
	}


	PEvoid Font::setLineHeight(PEuint32 lineHeight)
	{
		this->lineHeight = lineHeight;
	}
	PEuint32 Font::getLineHeight() const
	{
		return lineHeight;
	}

	PEvoid Font::addCharacter(const FontCharacter& fontCharacter)
	{
		fontCharacters.insert(std::make_pair(fontCharacter.getCode(), fontCharacter));
	}
	const FontCharacter& Font::getFontCharacter(PEchar character) const
	{
		if(fontCharacters.count(character) == 0)
			return fontCharacters.at('?');

		return fontCharacters.at(character);
	}
}
}
