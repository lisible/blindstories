#include "Graphics/Texture.hpp"

namespace pe
{
namespace graphics
{
	Texture::Texture(GLuint identifier, int width, int height)
		: textureIdentifier(identifier)
		, textureSize(width, height)
	{	
	}

	Texture::~Texture()
	{
		glDeleteTextures(1, &textureIdentifier);
	}

	GLuint Texture::getIdentifier() const
	{
		return textureIdentifier;
	}

	const pem::vec2i& Texture::getSize() const
	{
		return textureSize;
	}
}
}
