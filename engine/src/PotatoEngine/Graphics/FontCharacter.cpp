#include "Graphics/FontCharacter.hpp"

namespace pe
{
namespace graphics
{
	FontCharacter::FontCharacter(PEchar code,
								 PEuint32 x, PEuint32 y,
								 PEuint32 width, PEuint32 height,
								 PEuint32 xOffset, PEuint32 yOffset,
								 PEuint32 xAdvance)
		: code(code)
		, position(x, y)
		, size(width, height)
		, offset(xOffset, yOffset)
		, xAdvance(xAdvance)
	{
	}

	FontCharacter::~FontCharacter()
	{
	}

	PEchar FontCharacter::getCode() const
	{
		return code;
	}


	const pem::vec2u& FontCharacter::getPosition() const
	{
		return position;
	}
	const pem::vec2u& FontCharacter::getSize() const
	{
		return size;
	}
	const pem::vec2u& FontCharacter::getOffset() const
	{
		return offset;
	}
	PEuint32 FontCharacter::getXAdvance() const
	{
		return xAdvance;
	}
}
}
