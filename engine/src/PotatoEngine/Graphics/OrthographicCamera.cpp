#include "Graphics/OrthographicCamera.hpp"

namespace pe
{
namespace graphics
{
	OrthographicCamera::OrthographicCamera()
		: projectionMatrix(1)
	{
	}

	OrthographicCamera::OrthographicCamera(PEfloat left, PEfloat top,
											PEfloat right, PEfloat bottom,
											PEfloat near, PEfloat far)
	{
		setProjection(left, top, right, bottom, near, far);
	}

	OrthographicCamera::~OrthographicCamera()
	{
	}

	PEvoid OrthographicCamera::setProjection(PEfloat left, PEfloat top,
											 PEfloat right, PEfloat bottom,
											 PEfloat near, PEfloat far)
	{
		projectionMatrix = pem::orthographicProjection(left, -top, right, bottom, near, far);
	}

	pem::mat4& OrthographicCamera::getProjectionMatrix()
	{
		return projectionMatrix;
	}
}
}
