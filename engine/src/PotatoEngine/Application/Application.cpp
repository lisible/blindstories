#include "Application/Application.hpp"
#include "Messaging/Message.hpp"
#include "Messaging/Messages.hpp"
#include "Time/Timer.hpp"

#include <chrono>
#include <thread>

// TODO remove
#include <SDL.h>
#include <iostream>


namespace pe
{
namespace app
{
	Application::Application(const std::string& applicationTitle)
		: title(applicationTitle)
		, running(false)
	{
	}

	Application::~Application()
	{
	}

	PEvoid Application::run()
	{
		// Initializing
		init();
	
		// Starting the main loop
		running = true;
		sendMessage(pe::messaging::Message(pe::messaging::MSG_APPLICATION_START));

		const int TICKS_PER_FRAME = 1000 / 60;
		time::Timer capTimer;

		while(running)
		{
			capTimer.start();

			// Request systems update
			sendMessage(pe::messaging::Message(pe::messaging::MSG_UPDATE_REQUEST));
			// Requesting window clearing
			sendMessage(pe::messaging::Message(pe::messaging::MSG_WINDOW_WINDOWCLEAR_REQUEST));
			// Request rendering to other components
			sendMessage(pe::messaging::Message(pe::messaging::MSG_RENDER_REQUEST));
			sendMessage(pe::messaging::Message(pe::messaging::MSG_GRAPHICS_RENDER));
			// Request window content displaying
			sendMessage(pe::messaging::Message(pe::messaging::MSG_WINDOW_WINDOWDISPLAY_REQUEST));

			// Publish the messages
			messageBus.publishMessages();


			auto ticks = capTimer.getTime();
			if(ticks < TICKS_PER_FRAME)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(TICKS_PER_FRAME - ticks));
			}
		}	

		// Cleaning up
		cleanup();	
	}

	const std::string& Application::getTitle() const
	{
		return title;
	}

	PEvoid Application::init()
	{
		// Registering to the bus	
		setMessageBus(messageBus);
		messageBus.registerMessenger(this);

		// Setting startup handler
		registerCallback(pe::messaging::MSG_APPLICATION_START,
						[&](auto&& p){onApplicationStartup(p);});
		// Setting stop request handler
		registerCallback(pe::messaging::MSG_APPLICATION_STOP_REQUEST,
						[&](auto&& p){onApplicationStopRequest(p);});
	}

	PEvoid Application::cleanup()
	{
		for(auto& system : systems)
			delete system;
	}

	PEvoid Application::registerSystem(pe::system::System* system)
	{
		// @TODO assert system != nullptr
		messageBus.registerMessenger(system);
		systems.push_back(system);	
	}

	PEvoid Application::onApplicationStartup(PEvoid* data)
	{

	}

	PEvoid Application::onApplicationStopRequest(PEvoid* data)
	{
		running = false;
	}
}
}

