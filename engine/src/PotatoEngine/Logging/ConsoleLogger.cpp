#include "Logging/ConsoleLogger.hpp"
#include <iostream>

namespace pe
{
namespace logging
{
	ConsoleLogger::ConsoleLogger()
	{}

	ConsoleLogger::~ConsoleLogger()
	{}

	PEvoid ConsoleLogger::log(const std::string& logString)
	{
		std::cout << logString << std::endl;
	}
}
}
