#include "Logging/LoggingSystem.hpp"
#include "Messaging/Messaging.hpp"
#include "Logging/ILogger.hpp"
#include "Input/InputEvent.hpp"

#include <functional>
#include <string>

namespace pe
{
namespace logging
{
	LoggingSystem::LoggingSystem()
		: System()
	{
		registerDefaultHandlers();	
	}

	LoggingSystem::~LoggingSystem()
	{
		for(auto& logger : loggers)
			delete logger;	
	}

	PEvoid LoggingSystem::addLogger(ILogger* logger)
	{
		loggers.push_back(logger);
	}

	PEvoid LoggingSystem::log(const std::string& logString)
	{
		for(auto& logger : loggers)
			logger->log(logString);
	}

	PEvoid LoggingSystem::onLogRequest(PEvoid* data)
	{
		// Converting the pointer to the correct type	
		std::string* dataStr = reinterpret_cast<std::string*>(data);
		log(*dataStr);	
	}


	PEvoid LoggingSystem::onApplicationStartup(PEvoid* data)
	{
		log("Application is starting up");
	}

	PEvoid LoggingSystem::onWindowCreated(PEvoid* data)
	{
		auto md = reinterpret_cast<pemsg::SimpleMessageData*>(data);
		std::string logString = "Window \"" + md->stringValue1 + "\" "
								"(" +  std::to_string(md->integerValue1) + 
								"x" + std::to_string(md->integerValue2) + ")"
								" created";


		log(logString);
	}

	PEvoid LoggingSystem::onWindowShown(PEvoid* data)
	{
		log("Window shown");
	}

	PEvoid LoggingSystem::onWindowHidden(PEvoid* data)
	{
		log("Window hidden");
	}

	PEvoid LoggingSystem::onInputReceived(PEvoid* data)
	{
		auto inputEvent = reinterpret_cast<pe::input::InputEvent*>(data);
		if(inputEvent->type == pe::input::InputEvent::KEY_PRESSED)	
		{
			log("Key \"" + pe::input::keyboardToString(inputEvent->keyboard.key) + "\" pressed");
		}
	}

	PEvoid LoggingSystem::onActionPressed(PEvoid* data)
	{
		log("Action \"" + *reinterpret_cast<std::string*>(data) + "\" pressed");
	}

	PEvoid LoggingSystem::onActionReleased(PEvoid* data)
	{
		log("Action \"" + *reinterpret_cast<std::string*>(data) + "\" released");
	}

	PEvoid LoggingSystem::onTTSPronouncedRequest(PEvoid* data)
	{
		log("TTS: \"" + *reinterpret_cast<std::string*>(data) + "\"");
	}

	PEvoid LoggingSystem::registerDefaultHandlers()
	{
		// MSG_LOGGING_LOG
		registerCallback(pe::messaging::MSG_LOGGING_LOG,
						[&](auto&& p){onLogRequest(p);}); 
		/*// MSG_APPLICATION_START
		registerCallback(pe::messaging::MSG_APPLICATION_START,
						[&](auto&& p){onApplicationStartup(p);});
		// MSG_WINDOW_WINDOWCREATED
		registerCallback(pe::messaging::MSG_WINDOW_WINDOWCREATED,
						[&](auto&& p){onWindowCreated(p);});
		// MSG_WINDOW_WINDOWSHOWN
		registerCallback(pe::messaging::MSG_WINDOW_WINDOWSHOWN,
						[&](auto&& p){onWindowShown(p);});
		// MSG_WINDOW_WINDOWHIDDEN
		registerCallback(pe::messaging::MSG_WINDOW_WINDOWHIDDEN,
						[&](auto&& p){onWindowHidden(p);});
		// MSG_WINDOW_INPUTRECEIVED
		registerCallback(pe::messaging::MSG_WINDOW_INPUTRECEIVED,
						[&](auto&& p){onInputReceived(p);});
		
		// MSG_INPUT_ACTIONPRESSED
		registerCallback(pemsg::MSG_INPUT_ACTIONPRESSED,
						[&](auto&& p){onActionPressed(p);});
		// MSG_INPUT_ACTIONRELEASED
		registerCallback(pemsg::MSG_INPUT_ACTIONRELEASED,
						[&](auto&& p){onActionReleased(p);});*/
		
		// MSG_TTS_PRONOUNCE_REQUEST
		registerCallback(pemsg::MSG_TTS_PRONOUNCE_REQUEST, 
						[&](auto&& p){onTTSPronouncedRequest(p);});
	}
}
}

