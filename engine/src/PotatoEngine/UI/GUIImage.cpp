#include "UI/GUIImage.hpp"
#include "Graphics/GUIRenderer.hpp"


// TODO remove
#include <iostream>
namespace pe
{
namespace ui
{
	GUIImage::GUIImage(const std::string& identifier)
		: GUIComponent(identifier)	
	{
	}

	GUIImage::~GUIImage()
	{
	}

	PEvoid GUIImage::render(pe::graphics::Renderer2D& renderer,
							const pem::mat4& transform)
	{
		pem::Rectangle destinationRectangle;
		destinationRectangle.x = getPosition().x;
		destinationRectangle.y = getPosition().y;
		destinationRectangle.width = getSize().x;
		destinationRectangle.height = getSize().y;

		renderer.drawImage(getTexture(), getTextureRectangle(), destinationRectangle,
						   pe::graphics::Color(1.f, 1.f, 1.f), transform);

	}

	PEvoid GUIImage::setTexture(const std::string& texture)
	{
		sprite.setTexture(texture);
	}
	const std::string& GUIImage::getTexture() const
	{
		return sprite.getTexture();
	}

	PEvoid GUIImage::setSize(PEfloat width, PEfloat height)
	{
		GUIComponent::setSize(width, height);
		sprite.setSize(width, height);
	}

	const pe::graphics::Sprite& GUIImage::getSprite() const
	{
		return sprite;
	}

	PEvoid GUIImage::setTextureRectangle(PEfloat x, PEfloat y,
										 PEfloat width, PEfloat height)
	{
		sprite.setTextureRectangle(x, y, width, height);
	}
	const pem::Rectangle& GUIImage::getTextureRectangle() const
	{
		return sprite.getTextureRectangle();
	}
}
}
