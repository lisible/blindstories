#include "UI/GUITextArea.hpp"
#include "Graphics/GUIRenderer.hpp"
#include "Utils/String.hpp"

namespace pe
{
namespace ui
{
    GUITextArea::GUITextArea(const std::string& identifier,
                            const std::string& fontName,
                            const std::string& text)
        : GUIComponent(identifier)
        , text(text)
        , font(fontName)
    {
	}
    GUITextArea::~GUITextArea()
	{
	}


    PEvoid GUITextArea::setText(const std::string& text)
	{
        this->text = text;
	}
    const std::string& GUITextArea::getText() const
	{
        return text;
	}

    const std::string& GUITextArea::getFontName() const
	{
        return font;
	}

    PEvoid GUITextArea::setColor(pe::graphics::Color color)
	{
        this->color = color;
	}
    pe::graphics::Color GUITextArea::getColor() const
	{
        return color;
	}

    PEvoid GUITextArea::changeAttribute(const std::string& attribute, const std::string& value)
    {
        GUIComponent::changeAttribute(attribute, value);
        if(attribute == "text")
        {
            setText(value);
        }
        else if(attribute == "color")
        {
            setColor(pe::graphics::Color(value));
        }
    }

    PEvoid GUITextArea::render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform)
    {
        auto lines = pe::utils::split(text, '\n');

        for(size_t i = 0; i < lines.size(); i++)
            renderer.drawText(lines[i], font, getPosition().x, getPosition().y + i*32, 32, getColor(), transform);
    }
}
}