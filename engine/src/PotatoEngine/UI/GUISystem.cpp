#include "UI/GUISystem.hpp"
#include "Messaging/Messages.hpp"
#include "Input/InputEvent.hpp"
#include "UI/CallbackData.hpp"
#include "UI/ComponentRegisteringData.hpp"

#include <iostream>
#include <PotatoEngine/Messaging/SimpleMessageData.hpp>

namespace pe
{
namespace ui
{
    GUISystem::GUISystem()
        : ui(nullptr)
    {
        registerDefaultHandlers();
    }
    GUISystem::~GUISystem()
    {

    }

    PEvoid GUISystem::changeUI(const std::string& identifier)
    {
        if(ui != nullptr)
            ui->clearDynamicComponents();

        std::string uiIdentifier = identifier;
        void* userInterfaceRawPtr = sendDirectMessage(pe::messaging::Message(pe::messaging::MSG_RESOURCES_GETUI_DREQUEST,
                                                                             &uiIdentifier));

        // Setting the UI
        ui = static_cast<pe::ui::UserInterface*>(userInterfaceRawPtr);
        ui->setSystem(this);

        for(auto component : ui->getComponentList())
        {
            component->resetState();
        }
    }

    PEvoid GUISystem::requestRendering()
    {
        sendMessage(pe::messaging::Message(pe::messaging::MSG_GRAPHICS_RENDERUI, ui));
    }

    PEvoid GUISystem::requestTextToSpeech(const std::string& text)
    {
        std::string* str = new std::string(text);
        sendMessage(pe::messaging::Message(pe::messaging::MSG_TTS_PRONOUNCE_REQUEST,
                                           str,
                                           [=](PEvoid*){delete str;}));
    }



    // Callbacks
    PEvoid GUISystem::onAddUIComponentRequest(PEvoid* data)
    {
        auto registeringData = static_cast<ComponentRegisteringData*>(data);

        auto component = ui->getRoot();
        if(registeringData->parentIdentifier != "_root")
            component = ui->getRoot()->getSuccessor(registeringData->parentIdentifier);

        component->addChild(registeringData->component);
        ui->registerDynamicComponent(registeringData->component);
    }
    PEvoid GUISystem::onChangeUIRequest(PEvoid* data)
    {
        std::string* uiIdentifier = static_cast<std::string*>(data);
        changeUI(*uiIdentifier);
    }

    PEvoid GUISystem::onRenderRequest(PEvoid* data)
    {
        requestRendering();
    }

    PEvoid GUISystem::onMouseMoved(PEvoid* data)
    {
        pe::input::InputEvent::MouseMotionEvent* mouseMotion = static_cast<pe::input::InputEvent::MouseMotionEvent*>(data);

        for(auto component : ui->getComponentList())
        {
            if(!component->isPressed() && component->isInBounds(mouseMotion->x, mouseMotion->y))
                component->onHover();
            else
            {
                component->onHoverExit();
            }
        }
    }

    PEvoid GUISystem::onMouseButtonDown(PEvoid* data)
    {
        pe::input::InputEvent::MouseClickEvent* mouseClick = static_cast<pe::input::InputEvent::MouseClickEvent*>(data);

        for(auto component : ui->getComponentList())
        {
            if(component->isInBounds(mouseClick->x, mouseClick->y))
            {
                component->onPress();
                if(clickCallbacks.count(component->getIdentifier()) != 0)
                    clickCallbacks.at(component->getIdentifier())();
            }
        }
    }
    PEvoid GUISystem::onMouseButtonUp(PEvoid* data)
    {
        pe::input::InputEvent::MouseClickEvent* mouseClick = static_cast<pe::input::InputEvent::MouseClickEvent*>(data);

        for(auto component : ui->getComponentList())
        {
            if(component->isPressed())
                component->onPressExit();

            if(component->isInBounds(mouseClick->x, mouseClick->y))
                component->onHover();
        }
    }

    PEvoid GUISystem::onRegisterClickCallback(PEvoid* data)
    {
        pe::ui::CallbackData* callbackData = static_cast<pe::ui::CallbackData*>(data);

        if(clickCallbacks.count(callbackData->componentIdentifier))
            clickCallbacks.erase(callbackData->componentIdentifier);

        clickCallbacks.insert(std::make_pair(callbackData->componentIdentifier, callbackData->callbackFunction));
    }
    PEvoid GUISystem::onRegisterPostCallback(PEvoid* data)
    {
        pe::ui::CallbackData* callbackData = static_cast<pe::ui::CallbackData*>(data);

        if(postCallbacks.count(callbackData->componentIdentifier))
            postCallbacks.erase(callbackData->componentIdentifier);

        postCallbacks.insert(std::make_pair(callbackData->componentIdentifier, callbackData->callbackFunction));
    }

    PEvoid GUISystem::onChangeUIAttributeRequest(PEvoid* data)
    {
        auto messageData = static_cast<pe::messaging::SimpleMessageData*>(data);
        std::string componentIdentifier = messageData->stringValue1;
        std::string attribute = messageData->stringValue2;
        std::string value = messageData->stringValue3;

        auto component = ui->getRoot()->getSuccessor(componentIdentifier);
        if(component)
            component->changeAttribute(attribute, value);
    }

    PEvoid GUISystem::onFocusNextRequest(PEvoid* data)
	{
        for(auto component : ui->getComponentList())
        {
            if(component->isFocused())
            {
                std::string nextComponent = component->getNextComponent();
                if(nextComponent != "unknown")
                    ui->getRoot()->getSuccessor(nextComponent)->onFocus();

                component->onFocusExit();
                break;
            }
        }
	}
    PEvoid GUISystem::onFocusPreviousRequest(PEvoid* data)
	{
        for(auto component : ui->getComponentList())
        {
            if(component->isFocused())
            {
                std::string previousComponent = component->getPreviousComponent();
                if(previousComponent != "unknown")
                    ui->getRoot()->getSuccessor(previousComponent)->onFocus();

                component->onFocusExit();
                break;
            }
        }
	}
    PEvoid GUISystem::onPostRequest(PEvoid* data)
    {
        for(auto component : ui->getComponentList())
        {
            if(component->isFocused())
            {
                std::string identifier = component->getIdentifier();
                if(postCallbacks.count(identifier) != 0)
                    postCallbacks[identifier]();
            }
        }
    }

    PEvoid GUISystem::registerDefaultHandlers()
    {
        registerCallback(pe::messaging::MSG_INPUT_MOUSEMOVED,
                         [&](PEvoid* data){onMouseMoved(data);});
        registerCallback(pe::messaging::MSG_INPUT_MOUSEBUTTONDOWN,
                         [&](PEvoid* data){onMouseButtonDown(data);});
        registerCallback(pe::messaging::MSG_INPUT_MOUSEBUTTONUP,
                         [&](PEvoid* data){onMouseButtonUp(data);});

        registerCallback(pe::messaging::MSG_UI_ADDUICOMPONENT_REQUEST,
                         [&](PEvoid* data){onAddUIComponentRequest(data);});
        registerCallback(pe::messaging::MSG_UI_CHANGEUI_REQUEST,
                        [&](PEvoid* data){onChangeUIRequest(data);});

        registerCallback(pe::messaging::MSG_UI_REGISTERCLICKCALLBACK,
                        [&](PEvoid* data){onRegisterClickCallback(data);});
        registerCallback(pe::messaging::MSG_UI_REGISTERPOSTCALLBACK,
                        [&](PEvoid* data){onRegisterPostCallback(data);});

        registerCallback(pe::messaging::MSG_UI_CHANGEUIATTRIBUTE_REQUEST,
                        [&](PEvoid* data){onChangeUIAttributeRequest(data);});
        registerCallback(pe::messaging::MSG_UI_FOCUSNEXT_REQUEST,
                        [&](PEvoid* data){onFocusNextRequest(data);});
        registerCallback(pe::messaging::MSG_UI_FOCUSPREVIOUS_REQUEST,
                        [&](PEvoid* data){onFocusPreviousRequest(data);});;
        registerCallback(pe::messaging::MSG_UI_POST_REQUEST,
                        [&](PEvoid* data){onPostRequest(data);});

        registerCallback(pe::messaging::MSG_RENDER_REQUEST,
                         [&](PEvoid* data){onRenderRequest(data);});
    }
}
}