#include "UI/GUIRoot.hpp"
#include "Graphics/GUIRenderer.hpp"

namespace pe
{
namespace ui
{
	GUIRoot::GUIRoot()
		: GUIComponent("_root")
	{}

	GUIRoot::~GUIRoot()
	{}

	PEvoid GUIRoot::render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform)
	{
	}
}
}
