#include "UI/GUIFrame.hpp"
#include "Graphics/GUIRenderer.hpp"

// TODO remove that
#include <cstdlib>
namespace pe
{
namespace ui
{
	GUIFrame::GUIFrame(const std::string& identifier)
		: GUIComponent(identifier)
		, color(1.0f, 1.0f, 1.0f)
	{
	}

	GUIFrame::~GUIFrame()
	{
	}

	PEvoid GUIFrame::render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform)
	{
		pem::Rectangle rectangle;
		rectangle.x = getPosition().x;
		rectangle.y = getPosition().y;
		rectangle.width = getSize().x;
		rectangle.height = getSize().y;

		renderer.drawRectangle(rectangle, color, transform);
	}

	PEvoid GUIFrame::setColor(const pe::graphics::Color& color)
	{
		this->color = color;
	}
	const pe::graphics::Color& GUIFrame::getColor() const
	{
		return color;
	}
}
}
