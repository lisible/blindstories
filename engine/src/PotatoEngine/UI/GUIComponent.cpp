#include "UI/GUIComponent.hpp"
#include "Graphics/GUIRenderer.hpp"
#include "UI/GUISystem.hpp"

namespace pe
{
namespace ui
{
	GUIComponent::GUIComponent(const std::string& identifier)
		: identifier(identifier)
	  	, ui(nullptr)
        , anchor(Anchor::NORTH_WEST)
        , parent(nullptr)
	  	, nextComponent("unknown")
	  	, previousComponent("unknown")
	  	, ttsOnFocus("")
	    , hasFocus(false)
		, hovered(false)
        , pressed(false)
	{}

	GUIComponent::~GUIComponent()
	{
		clearSuccessors();
	}

	PEvoid GUIComponent::setUI(UserInterface* ui)
	{
		this->ui = ui;
	}
	UserInterface* GUIComponent::getUI() const
	{
		return ui;
	}

	PEvoid GUIComponent::addChild(GUIComponent* child)
	{
		children.insert(std::make_pair(child->getIdentifier(), child));
		child->setParent(this);
	}

	GUIComponent* GUIComponent::getSuccessor(const std::string& identifier) const
	{
		GUIComponent* component(nullptr);
		
		// Checking each child
		for(auto& child : children)
		{
			// If the direct child has the identifier, return it
			if(child.first == identifier){
				component = child.second;
				break;
			}
		
			// Else checking the successors	
			component = child.second->getSuccessor(identifier);
			if(component != nullptr)
				break;
		}	

		return component;
	}

	PEvoid GUIComponent::setParent(GUIComponent* parent)
	{
		this->parent = parent;
	}
	GUIComponent* GUIComponent::getParent() const
	{
		return parent;
	}

	PEvoid GUIComponent::removeChild(const std::string& childIdentifier)
	{
		children.erase(childIdentifier);
	}

	PEvoid GUIComponent::clearSuccessors()
	{
		for(auto& child : children)
		{
			child.second->clearSuccessors();
			delete child.second;
		}

		children.clear();
	}

	const std::string& GUIComponent::getIdentifier() const
	{
		return identifier;
	}

	std::map<std::string, GUIComponent*>& GUIComponent::getChildren() 
	{
		return children;	
	}
	
	PEvoid GUIComponent::setNextComponent(const std::string& next)
	{
		nextComponent = next;
	}
	const std::string& GUIComponent::getNextComponent() const
	{
		return nextComponent;
	}

	PEvoid GUIComponent::setPreviousComponent(const std::string& previous)
	{
		previousComponent = previous;
	}
	const std::string& GUIComponent::getPreviousComponent() const
	{
		return previousComponent;
	}

	PEvoid GUIComponent::setSize(PEfloat width, PEfloat height)
	{
		size.x = width;
		size.y = height;
	}
	const pem::vec2& GUIComponent::getSize() const
	{
		return size;
	}

    PEvoid GUIComponent::setAnchor(Anchor anchor)
    {
        this->anchor = anchor;
    }
    Anchor GUIComponent::getAnchor() const
    {
        return anchor;
    }

    pem::mat4 GUIComponent::getCombinedInvTransformMatrix()
    {
        if(parent != nullptr)
            return parent->getInverseTransformMatrix() * parent->getCombinedInvTransformMatrix();
        return getInverseTransformMatrix();
    }

	PEbool GUIComponent::isInBounds(PEfloat x, PEfloat y)
	{
		pem::vec4 v(x, y, 0.f, 1.f);
        v = v*getCombinedInvTransformMatrix()*getInverseTransformMatrix();

		return v.x > 0 && v.x < size.x && v.y > 0 && v.y < size.y;
	}

	PEvoid GUIComponent::changeAttribute(const std::string& attribute, const std::string& value)
	{
	}

	PEvoid GUIComponent::resetState()
	{
		onHoverExit();
		onPressExit();

		if(isFocused())
			onFocus();
	}

	PEvoid GUIComponent::setTTSOnFocus(const std::string& ttsOnFocus)
	{
		this->ttsOnFocus = ttsOnFocus;
	}
	const std::string& GUIComponent::getTTSOnFocus() const
	{
		return ttsOnFocus;
	}


	PEvoid GUIComponent::onFocus()
	{
		hasFocus = true;
		if(ui)
			ui->getSystem()->requestTextToSpeech(ttsOnFocus);
	}
	PEvoid GUIComponent::onFocusExit()
	{
		hasFocus = false;
	}
	PEbool GUIComponent::isFocused() const
	{
		return hasFocus;
	}

	PEbool GUIComponent::isHovered() const
	{
		return hovered;
	}
    PEbool GUIComponent::isPressed() const
    {
        return pressed;
    }

	PEvoid GUIComponent::onHover()
	{
		hovered = true;
	}
	PEvoid GUIComponent::onHoverExit()
	{
		hovered = false;
	}

    PEvoid GUIComponent::onPress()
    {
        pressed = true;
    }
    PEvoid GUIComponent::onPressExit()
    {
        pressed = false;
    }
}
}
