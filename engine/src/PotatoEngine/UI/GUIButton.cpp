#include "UI/GUIButton.hpp"
#include "Graphics/GUIRenderer.hpp"
#include "Maths/Rectangle.hpp"

namespace pe
{
namespace ui
{
    GUIButton::GUIButton(const std::string& identifier, const std::string& font)
        : GUIComponent(identifier)
        , buttonLabel("", font)
        , buttonFrame("")
        , defaultColor(1.0f, 1.0f, 1.0f)
        , hoverColor(1.0f, 1.0f, 1.0f)
        , downColor(1.0f, 1.0f, 1.0f)
	{
	}
    GUIButton::~GUIButton()
	{
	}

    PEvoid GUIButton::setText(const std::string& text)
	{
        buttonLabel.setText(text);
        updateLabelPosition();
	}
    PEvoid GUIButton::setSize(PEfloat width, PEfloat height)
	{
        GUIComponent::setSize(width, height);
        buttonFrame.setSize(width, height);
        updateLabelPosition();
	}

    PEvoid GUIButton::setDefaultColor(const pe::graphics::Color& color)
	{
        defaultColor = color;
        buttonFrame.setColor(color);
	}
    PEvoid GUIButton::setHoverColor(const pe::graphics::Color& color)
	{
        hoverColor = color;
	}
    PEvoid GUIButton::setDownColor(const pe::graphics::Color& color)
	{
        downColor = color;
	}

    PEvoid GUIButton::render(pe::graphics::Renderer2D& renderer, const pem::mat4& transform)
	{
        buttonFrame.render(renderer, this->getTransformMatrix() * transform);
        buttonLabel.render(renderer, this->getTransformMatrix() * transform);
        updateLabelPosition();
	}

    PEvoid GUIButton::updateLabelPosition()
    {
        auto& size = getSize();
        auto& labelSize = buttonLabel.getSize();
        buttonLabel.setPosition(size.x/2-labelSize.x/2, size.y/2-labelSize.y/2);
    }

    PEvoid GUIButton::onFocus()
    {
        GUIComponent::onFocus();
        buttonFrame.setColor(hoverColor);
    }
    PEvoid GUIButton::onFocusExit()
    {
        GUIComponent::onFocusExit();
        buttonFrame.setColor(defaultColor);
    }


    PEvoid GUIButton::onHover() 
	{
        GUIComponent::onHover();
        buttonFrame.setColor(hoverColor);
	}
    PEvoid GUIButton::onHoverExit() 
	{
        GUIComponent::onHoverExit();
        buttonFrame.setColor(defaultColor);
	}

    PEvoid GUIButton::onPress()
    {
        GUIComponent::onPress();
        buttonFrame.setColor(downColor);
    }
    PEvoid GUIButton::onPressExit()
    {
        GUIComponent::onPressExit();
        buttonFrame.setColor(defaultColor);
    }
}
}