#include "UI/UserInterface.hpp"
#include "UI/GUISystem.hpp"
#include <algorithm>

namespace pe
{
namespace ui
{
	UserInterface::UserInterface()
		: root()
		, components()
	{}

	UserInterface::~UserInterface()
	{}

	PEvoid UserInterface::setSystem(GUISystem* system)
	{
		this->system = system;
	}
	GUISystem* UserInterface::getSystem() const
	{
		return system;
	}

	GUIComponent* UserInterface::getRoot()
	{
		return static_cast<GUIComponent*>(&root);
	}

	PEvoid UserInterface::registerComponent(GUIComponent* component)
	{
		components.push_back(component);
		component->setUI(this);
	}
	PEvoid UserInterface::registerDynamicComponent(GUIComponent* component)
	{
		registerComponent(component);
		dynamicComponents.push_back(component);
	}
	std::vector<GUIComponent*>& UserInterface::getComponentList()
	{
		return components;
	}

	PEvoid UserInterface::clearDynamicComponents()
	{
		for(auto dynamicComponent : dynamicComponents)
		{
			dynamicComponent->getParent()->removeChild(dynamicComponent->getIdentifier());
			std::remove_if(components.begin(), components.end(), [=](GUIComponent* component){return component == dynamicComponent;});
		}

		dynamicComponents.clear();
	}

	PEvoid UserInterface::clear()
	{
		root.clearSuccessors();
	}
}
}
