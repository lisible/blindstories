#include "UI/GUILabel.hpp"
#include "Graphics/GUIRenderer.hpp"

namespace pe
{
namespace ui
{
	GUILabel::GUILabel(const std::string& identifier,
					   const std::string& fontName,
					   const std::string& text)
		: GUIComponent(identifier)
		, text(text)
		, fontName(fontName)
		, color(1.f, 1.f, 1.f)
		, defaultColor(1.f, 1.f, 1.f)
		, hoverColor(1.f, 1.f, 1.f)
	{
	}

	GUILabel::~GUILabel()
	{
	}

	PEvoid GUILabel::setText(const std::string& text)
	{
		this->text = text;
	}

	const std::string& GUILabel::getText() const
	{
		return text;
	}
	const std::string& GUILabel::getFontName() const
	{
		return fontName;
	}

	PEvoid GUILabel::setColor(pe::graphics::Color color)
	{
		this->color = color;
	}
	pe::graphics::Color GUILabel::getColor() const
	{
		return color;
	}

	PEvoid GUILabel::setDefaultColor(pe::graphics::Color color)
	{
		this->defaultColor = color;
		setColor(color);
	}
	pe::graphics::Color GUILabel::getDefaultColor() const
	{
		return defaultColor;
	}

	PEvoid GUILabel::setHoverColor(pe::graphics::Color color)
	{
		this->hoverColor = color;
	}
	pe::graphics::Color GUILabel::getHoverColor() const
	{
		return hoverColor;
	}

	PEvoid GUILabel::changeAttribute(const std::string& attribute, const std::string& value)
	{
		GUIComponent::changeAttribute(attribute, value);
		if(attribute == "text")
		{
			setText(value);
		}
		else if(attribute == "color")
		{
			setColor(pe::graphics::Color(value));
		}
	}

	PEvoid GUILabel::render(pe::graphics::Renderer2D& renderer,
						    const pem::mat4& transform)
	{
		renderer.drawText(getText(), getFontName(), getPosition().x, getPosition().y, 32, getColor(), transform);
	}

	PEvoid GUILabel::onFocus()
	{
		GUIComponent::onFocus();
		setColor(hoverColor);
	}
	PEvoid GUILabel::onFocusExit()
	{
		GUIComponent::onFocusExit();
		setColor(defaultColor);
	}

	PEvoid GUILabel::onHover()
	{
		GUIComponent::onHover();
		setColor(hoverColor);
	}
	PEvoid GUILabel::onHoverExit()
	{
		GUIComponent::onHoverExit();
		setColor(defaultColor);
	}
}
}
