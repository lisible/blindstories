#include "UI/GUIComponentFactory.hpp"
#include "UI/GUIRoot.hpp"
#include "UI/GUIFrame.hpp"
#include "UI/GUILabel.hpp"
#include "UI/GUITextArea.hpp"
#include "UI/GUIImage.hpp"
#include "UI/GUIButton.hpp"

namespace pe
{
namespace ui
{
    using namespace tinyxml2;
    GUIComponentFactory::GUIComponentFactory()
	{
	}
    GUIComponentFactory::~GUIComponentFactory()
	{
	}


    GUIComponent* GUIComponentFactory::createComponentFromXML(tinyxml2::XMLNode* node)
	{

        XMLElement* element = node->ToElement();
        if(element == nullptr)
            return nullptr;

        if(std::strcmp(element->Name(), "root") == 0)
            return createRootFromXML(element);
        if(std::strcmp(element->Name(), "frame") == 0)
            return createFrameFromXML(element);
        if(std::strcmp(element->Name(), "label") == 0)
            return createLabelFromXML(element);
        if(std::strcmp(element->Name(), "textArea") == 0)
            return createTextAreaFromXML(element);
        if(std::strcmp(element->Name(), "image") == 0)
            return createImageFromXML(element);
        if(std::strcmp(element->Name(), "button") == 0)
            return createButtonFromXML(element);

        // TODO handle custom components cases (add an empty virtual method "createCustomComponentFromXML ?")

        return nullptr;
	}


    GUIComponent* GUIComponentFactory::createRootFromXML(tinyxml2::XMLElement* element)
	{
        return new GUIRoot();
	}
    GUIComponent* GUIComponentFactory::createFrameFromXML(tinyxml2::XMLElement* element)
	{
        const char* identifier = element->Attribute("id");
        float xCoordinate = element->FloatAttribute("x");
        float yCoordinate = element->FloatAttribute("y");
        float width = element->FloatAttribute("width");
        float height = element->FloatAttribute("height");
        const char* hexColor = element->Attribute("color");


        const char* anchor = element->Attribute("anchor");
        float angle = 0.f;
        element->QueryFloatAttribute("angle", &angle);



        GUIFrame* frame = new GUIFrame(identifier);
        frame->setPosition(xCoordinate, yCoordinate);
        frame->setSize(width, height);
        frame->setAngle(angle);
        frame->setColor(pe::graphics::Color(std::string(hexColor)));

        if(anchor != nullptr)
            frame->setAnchor(stringToAnchor(anchor));
        else
            frame->setAnchor(Anchor::NORTH_WEST);

        return frame;
	}
    GUIComponent* GUIComponentFactory::createLabelFromXML(tinyxml2::XMLElement* element)
    {
        // Required attributes
        const char* identifier = element->Attribute("id");
        float xCoordinate = element->FloatAttribute("x");
        float yCoordinate = element->FloatAttribute("y");
        const char* font = element->Attribute("font");
        const char* text = element->Attribute("text");


        // Optional attributes
        float angle = 0.f;
        element->QueryFloatAttribute("angle", &angle);

        const char* color = element->Attribute("color");
        const char* hoverColor = element->Attribute("hoverColor");

        const char* next = element->Attribute("next");
        const char* previous = element->Attribute("previous");


        const char* ttsOnFocus = element->Attribute("ttsOnFocus");

        GUILabel* label = new GUILabel(identifier, font);
        label->setPosition(xCoordinate, yCoordinate);
        label->setText(text);
        label->setAngle(angle);

        if(color != nullptr)
        {
            label->setDefaultColor(pe::graphics::Color(color));
            label->setHoverColor(pe::graphics::Color(color));
        }

        if(hoverColor != nullptr)
            label->setHoverColor(pe::graphics::Color(hoverColor));

        if(next)
            label->setNextComponent(next);
        if(previous)
            label->setPreviousComponent(previous);

        if(ttsOnFocus)
            label->setTTSOnFocus(ttsOnFocus);


        return label;
    }
    GUIComponent* GUIComponentFactory::createTextAreaFromXML(tinyxml2::XMLElement* element)
    {
        // Required attributes
        const char* identifier = element->Attribute("id");
        float xCoordinate = element->FloatAttribute("x");
        float yCoordinate = element->FloatAttribute("y");
        float width = element->FloatAttribute("width");
        float height = element->FloatAttribute("height");
        const char* font = element->Attribute("font");

        std::string text(element->GetText());
        text.erase(text.begin(), std::find_if(text.begin(), text.end(), [](int ch) {
            return !std::isspace(ch);
        }));
        text.erase(std::find_if(text.rbegin(), text.rend(), [](int ch) {
            return !std::isspace(ch);
        }).base(), text.end());


        // Optional attributes
        float angle = 0.f;
        element->QueryFloatAttribute("angle", &angle);

        const char* color = element->Attribute("color");

        GUITextArea* textArea = new GUITextArea(identifier, font);
        textArea->setPosition(xCoordinate, yCoordinate);
        textArea->setSize(width, height);
        textArea->setText(text);
        textArea->setAngle(angle);

        if(color != nullptr)
            textArea->setColor(pe::graphics::Color(color));

        return textArea;
    }
    GUIComponent* GUIComponentFactory::createImageFromXML(tinyxml2::XMLElement* element)
	{
        // Required attribute
        const char* identifier = element->Attribute("id");
        const char* texture = element->Attribute("texture");
        float xCoordinate = element->FloatAttribute("x");
        float yCoordinate = element->FloatAttribute("y");
        float width = element->FloatAttribute("width");
        float height = element->FloatAttribute("height");

        // Optional attributes
        float angle = 0.f;
        element->QueryFloatAttribute("angle", &angle);

        float textureX = 0.f;
        element->QueryFloatAttribute("textureX", &textureX);
        float textureY = 0.f;
        element->QueryFloatAttribute("textureY", &textureY);
        float textureWidth = 800.f;
        element->QueryFloatAttribute("textureWidth", &textureWidth);
        float textureHeight = 400.f;
        element->QueryFloatAttribute("textureHeight", &textureHeight);

        GUIImage* image = new GUIImage(identifier);
        image->setPosition(xCoordinate, yCoordinate);
        image->setSize(width, height);
        image->setAngle(angle);
        image->setTexture(texture);
        image->setTextureRectangle(textureX, textureY, textureWidth, textureHeight);

        return image;
	}
    GUIComponent* GUIComponentFactory::createButtonFromXML(tinyxml2::XMLElement* element)
    {
        const char* identifier = element->Attribute("id");
        float xCoordinate = element->FloatAttribute("x");
        float yCoordinate = element->FloatAttribute("y");
        float width = element->FloatAttribute("width");
        float height = element->FloatAttribute("height");
        const char* font =  element->Attribute("font");
        const char* text = element->Attribute("text");


        const char* anchor = element->Attribute("anchor");
        const char* hexDefaultColor = element->Attribute("defaultColor");
        const char* hexHoverColor = element->Attribute("hoverColor");
        const char* hexDownColor = element->Attribute("downColor");
        const char* ttsOnFocus = element->Attribute("ttsOnFocus");
        const char* next = element->Attribute("next");
        const char* previous = element->Attribute("previous");

        float angle = 0.f;
        element->QueryFloatAttribute("angle", &angle);

        bool focus = false;
        element->QueryBoolAttribute("focus", &focus);


        GUIButton* button = new GUIButton(identifier, font);
        button->setPosition(xCoordinate, yCoordinate);
        button->setSize(width, height);
        button->setAngle(angle);
        button->setText(text);

        if(focus)
            button->onFocus();

        if(anchor != nullptr)
            button->setAnchor(stringToAnchor(anchor));
        else
            button->setAnchor(Anchor::NORTH_WEST);

        if(hexDefaultColor != nullptr)
            button->setDefaultColor(pe::graphics::Color(hexDefaultColor));
        else
            button->setDefaultColor(pe::graphics::Color("112233"));

        if(hexHoverColor != nullptr)
            button->setHoverColor(pe::graphics::Color(hexHoverColor));
        else
            button->setHoverColor(pe::graphics::Color("0000E5"));

        if(hexDownColor != nullptr)
            button->setDownColor(pe::graphics::Color(hexDownColor));
        else
            button->setDownColor(pe::graphics::Color("FFFFFF"));

        if(next)
            button->setNextComponent(next);
        if(previous)
            button->setPreviousComponent(previous);

        if(ttsOnFocus)
            button->setTTSOnFocus(ttsOnFocus);


        return button;
    }



}
}
