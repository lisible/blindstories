#include "Audio/TextToSpeechSystem.hpp"
#include "Messaging/Messaging.hpp"

namespace pe
{
namespace audio
{
	PEvoid TextToSpeechSystem::registerDefaultHandlers()
	{
		// Registering the callback for pronounce request
		registerCallback(pe::messaging::MSG_TTS_PRONOUNCE_REQUEST, 
						[&](auto&& p){onAudioPronounceRequest(p);});
	}

	PEvoid TextToSpeechSystem::onAudioPronounceRequest(PEvoid* data)
	{
		auto textString = reinterpret_cast<std::string*>(data);	
		pronounce(*textString);
	}
}
}
