#include "Audio/FestivalTextToSpeechSystem.hpp"

#include <signal.h>
#include <unistd.h>
#include <string>

namespace pe
{
namespace audio
{
	FestivalTextToSpeechSystem::FestivalTextToSpeechSystem()
		: initialized(false)
	{
        bool hasFestival = access("/usr/bin/festival", F_OK) != -1;
		if(hasFestival)
		{
			registerDefaultHandlers();

			pipe(fd);

			pid = fork();
			if(pid == 0)
			{
				close(fd[1]);
				dup2(fd[0], 0);
				execlp("/usr/bin/festival", "festival",  NULL);
			}
			close(fd[0]);

			std::string cmd("(audio_mode 'async)\n");
			write(fd[1], cmd.c_str(), cmd.size());

			initialized = true;
		}


	}

	FestivalTextToSpeechSystem::~FestivalTextToSpeechSystem()
	{
		if(initialized)
			kill(pid, SIGQUIT);
	}

	PEvoid FestivalTextToSpeechSystem::pronounce(const std::string& text)
	{
		std::string output = "(audio_mode 'shutup)(SayText \"" + text + "\")\n";

		write(fd[1], output.c_str(), output.size());
	}
}
}

