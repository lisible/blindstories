#include "File/ResourceFileReader.hpp"
#include "Utils/String.hpp"

// TODO delete
#include <iostream>
namespace pe
{
namespace file
{
	ResourceFileReader::ResourceFileReader()
	{
	}	

	ResourceFileReader::~ResourceFileReader()
	{
	}

	PEvoid ResourceFileReader::openFile(File& file)
	{
		fileReader.openFile(file);	
	}
	PEvoid ResourceFileReader::closeFile()
	{
		fileReader.closeFile();
	}

	pe::resources::ResourceDescription ResourceFileReader::readResourceDescription()
	{
		pe::resources::ResourceDescription resourceDescription;	
		readHeader(resourceDescription);
		readParameters(resourceDescription);
		return resourceDescription;
	}
	

	PEvoid ResourceFileReader::readHeader(pe::resources::ResourceDescription& resourceDescription)
	{
		std::string line;
		fileReader.readLine(line);
		resourceDescription.setResourceName(pe::utils::split(line, '=')[1]);
		fileReader.readLine(line);
		resourceDescription.setResourceType(pe::resources::resourceTypeFromString(
													pe::utils::split(line, '=')[1]));
	}

	PEvoid ResourceFileReader::readParameters(pe::resources::ResourceDescription& resourceDescription)
	{
		std::string line;
		while(fileReader.readLine(line) > 0)
		{
			std::cout << "a" << std::endl;	
			std::vector<std::string> splitString = pe::utils::split(line, '=');
			std::cout << "b" << std::endl;
			std::cout << splitString.size() << std::endl;
			resourceDescription.addResourceParameter(splitString[0], splitString[1]);
		}
	}
}
}
