#include "File/File.hpp"

namespace pe
{
namespace file
{
	File::File(const std::string& pathString)
		: pathString(pathString)
	{}

	File::~File()
	{}

	const std::string& File::getPath() const
	{
		return pathString;
	}
}
}
