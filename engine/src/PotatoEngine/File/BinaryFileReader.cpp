#include "File/BinaryFileReader.hpp"

namespace pe
{
namespace file
{
	BinaryFileReader::BinaryFileReader()
	{
	}

	BinaryFileReader::~BinaryFileReader()
	{
	}

	PEvoid BinaryFileReader::openFile(File& file)
	{
		stream.open(file.getPath(), std::ifstream::binary);
	}
	PEvoid BinaryFileReader::closeFile()
	{
		stream.close();
	}

	std::vector<char> BinaryFileReader::readBytes(PEuint64 length)
	{
		std::vector<char> bytes;	
		char* buffer = new char[length];
		
		stream.read(buffer, length);
		std::streamsize count = stream.gcount();

		for(int i = 0; i < count; i++)
			bytes.push_back(buffer[i]);

		delete [] buffer;
		return bytes;
	}

	std::vector<char> BinaryFileReader::readFile()
	{
		stream.seekg(0, std::ios::end);
		std::streamsize size = stream.tellg();
		stream.seekg(0, std::ios::beg);

		std::vector<char> bytes(size);
		stream.read(bytes.data(), size);

		return bytes;
	}
}
}
