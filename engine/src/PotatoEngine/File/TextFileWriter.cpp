#include "File/TextFileWriter.hpp"

namespace pe
{
namespace file
{
	TextFileWriter::TextFileWriter()
	{}

	TextFileWriter::~TextFileWriter()
	{}

	PEvoid TextFileWriter::openFile(const File& file)
	{
		// TODO error checking
		// Opens the stream with the path of the given file
		stream.open(file.getPath().c_str());
	}

	PEvoid TextFileWriter::closeFile()
	{
		stream.close();
	}

	PEvoid TextFileWriter::write(const std::string& string)
	{
		stream << string;
	}

	PEvoid TextFileWriter::writeLine(const std::string& line)
	{
		stream << line << std::endl;
	}
}
}
