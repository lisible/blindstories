#include "File/TextFileReader.hpp"
#include <sstream>
#include <iostream>

namespace pe
{
namespace file
{
	TextFileReader::TextFileReader()
	{}

	TextFileReader::~TextFileReader()
	{}

	PEvoid TextFileReader::openFile(File& file)
	{
		stream.open(file.getPath().c_str());
	}

	PEvoid TextFileReader::closeFile()
	{
		stream.close();
	}

	std::string TextFileReader::readString(PEuint64 length)
	{
		std::string readStr;	
		char* buffer = new char[length];

		stream.read(buffer, length);
		readStr += buffer;

		delete [] buffer;

		return readStr;	
	}

	PEsize_t TextFileReader::readLine(std::string& line)
	{
		if(stream.eof())
			return 0;
	
		getline(stream, line);
		
		PEsize_t count = line.length();	
		

		return !stream ? 0 : count;
	}

	std::string TextFileReader::readFile()
	{
		std::stringstream buffer;
		buffer << stream.rdbuf();
		
		return buffer.str();
	}
}
}
