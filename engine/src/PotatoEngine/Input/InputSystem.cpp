#include "Input/InputSystem.hpp"
#include "Messaging/Messaging.hpp"

#include <cstring>


// TODO remove
#include <iostream>
namespace pe
{
namespace input
{
	InputSystem::InputSystem()
	{
		registerDefaultHandlers();	
	}

	InputSystem::~InputSystem()
	{}
	
	PEvoid InputSystem::setKeyMap(const KeyMap& keyMap)
	{
		this->keyMap = keyMap;
	}

	PEvoid InputSystem::onInputReceived(PEvoid* data)
	{
		auto eventData = reinterpret_cast<InputEvent*>(data);

		switch(eventData->type)
		{
			case InputEvent::KEY_PRESSED:
				onKeyPressed(eventData);
				break;
			case InputEvent::KEY_RELEASED:
				onKeyReleased(eventData);
				break;
			case InputEvent::MOUSE_MOVED:
				onMouseMoved(eventData);
				break;
			case InputEvent::MOUSE_BUTTON_DOWN:
				onMouseButtonDown(eventData);
				break;
			case InputEvent::MOUSE_BUTTON_UP:
				onMouseButtonUp(eventData);
				break;
			case InputEvent::TEXT_TYPED:
				onTextTyped(eventData);
				break;
			default:
				break;
		}
	}

	PEvoid InputSystem::onKeyPressed(InputEvent* data)
	{
		auto key = data->keyboard.key;	
		if(!keyMap.hasKey(key))		
			return;

		std::string* action = new std::string();
		*action = keyMap.getActionForKey(key);	
		
		sendMessage(pemsg::Message(pemsg::MSG_INPUT_ACTIONPRESSED,	
								reinterpret_cast<PEvoid*>(action),
								[=](PEvoid*){delete action;}));
	}

	PEvoid InputSystem::onKeyReleased(InputEvent* data)
	{
		auto key = data->keyboard.key;
		if(!keyMap.hasKey(data->keyboard.key))
			return;

		std::string* action = new std::string(keyMap.getActionForKey(key));

		sendMessage(pemsg::Message(pemsg::MSG_INPUT_ACTIONRELEASED,
								   reinterpret_cast<PEvoid*>(action),
								   [=](PEvoid*){delete action;}));


	}

	PEvoid InputSystem::onMouseMoved(InputEvent* data)
	{
		InputEvent::MouseMotionEvent* motion = new InputEvent::MouseMotionEvent();
		motion->x = data->mouseMotion.x;
		motion->y = data->mouseMotion.y;
		motion->relX = data->mouseMotion.relX;
		motion->relY = data->mouseMotion.relY;


		sendMessage(pemsg::Message(pemsg::MSG_INPUT_MOUSEMOVED,
								   static_cast<PEvoid*>(motion),
								   [=](PEvoid*){delete motion;}));
	}

	PEvoid InputSystem::onMouseButtonDown(InputEvent* data)
	{
		InputEvent::MouseClickEvent* click = new InputEvent::MouseClickEvent();
		click->x = data->mouseClick.x;
		click->y = data->mouseClick.y;
		click->button = data->mouseClick.button;


		sendMessage(pemsg::Message(pemsg::MSG_INPUT_MOUSEBUTTONDOWN,
								   static_cast<PEvoid*>(click),
								   [=](PEvoid*){delete click;}));
	}
	PEvoid InputSystem::onMouseButtonUp(InputEvent* data)
	{
		InputEvent::MouseClickEvent* click = new InputEvent::MouseClickEvent();
		click->x = data->mouseClick.x;
		click->y = data->mouseClick.y;
		click->button = data->mouseClick.button;


		sendMessage(pemsg::Message(pemsg::MSG_INPUT_MOUSEBUTTONUP,
								   static_cast<PEvoid*>(click),
								   [=](PEvoid*){delete click;}));
	}
	PEvoid InputSystem::onTextTyped(InputEvent* data)
	{
		std::string* str = new std::string(data->text.text);
		sendMessage(pemsg::Message(pemsg::MSG_INPUT_TEXTTYPED,
								   str,
								   [=](PEvoid*){delete str;}));
	}

	PEvoid InputSystem::registerDefaultHandlers()
	{
		registerCallback(pemsg::MSG_WINDOW_INPUTRECEIVED,
						[&](auto&& p){onInputReceived(p);});
	}
}
}
