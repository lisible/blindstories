#include "Input/Keyboard.hpp"

namespace pe
{
namespace input
{
	std::string keyboardToString(Keyboard key)
	{
		switch(key)
		{
			case KEY_A:
				return "KEY_A";
			case KEY_B:
				return "KEY_B";
			case KEY_C:
				return "KEY_C";
			case KEY_D:
				return "KEY_D";
			case KEY_E:
				return "KEY_E";
			case KEY_F:
				return "KEY_F";
			case KEY_G:
				return "KEY_G";
			case KEY_H:
				return "KEY_H";
			case KEY_I:
				return "KEY_I";
			case KEY_J:
				return "KEY_J";
			case KEY_K:
				return "KEY_K";
			case KEY_L:
				return "KEY_L";
			case KEY_M:
				return "KEY_M";
			case KEY_N:
				return "KEY_N";
			case KEY_O:
				return "KEY_O";
			case KEY_P:
				return "KEY_P";
			case KEY_Q:
				return "KEY_Q";
			case KEY_R:
				return "KEY_R";
			case KEY_S:
				return "KEY_S";
			case KEY_T:
				return "KEY_T";
			case KEY_U:
				return "KEY_U";
			case KEY_V:
				return "KEY_V";
			case KEY_W:
				return "KEY_W";
			case KEY_X:
				return "KEY_X";
			case KEY_Y:
				return "KEY_Y";
			case KEY_Z:
				return "KEY_Z";
			case KEY_ESCAPE:
				return "KEY_ESCAPE";
			case KEY_ENTER:
				return "KEY_ENTER";
			case KEY_LCTRL:
				return "KEY_LCTRL";
			case KEY_RCTRL:
				return "KEY_RCTRL";
			case KEY_LSHIFT:
				return "KEY_LSHIFT";
			case KEY_RSHIFT:	
				return "KEY_RSHIFT";
			case KEY_SPACE:
				return "KEY_SPACE";
			case KEY_UNKNOWN:
			default:
				return "KEY_UNKNOWN";
		}
	}
	
	Keyboard stringToKeyboard(const std::string& keyString)
	{
		if(keyString == "KEY_UNKNOWN")
			return KEY_UNKNOWN;
		if(keyString == "KEY_A")
			return KEY_A;
		if(keyString == "KEY_B")
			return KEY_B;
		if(keyString == "KEY_C")
			return KEY_C;
		if(keyString == "KEY_D")
			return KEY_D;
		if(keyString == "KEY_E")
			return KEY_E;
		if(keyString == "KEY_F")
			return KEY_F;
		if(keyString == "KEY_G")
			return KEY_G;
		if(keyString == "KEY_H")
			return KEY_H;
		if(keyString == "KEY_I")
			return KEY_I;
		if(keyString == "KEY_J")
			return KEY_J;
		if(keyString == "KEY_K")
			return KEY_K;
		if(keyString == "KEY_L")
			return KEY_L;
		if(keyString == "KEY_M")
			return KEY_M;
		if(keyString == "KEY_N")
			return KEY_N;
		if(keyString == "KEY_O")
			return KEY_O;
		if(keyString == "KEY_P")
			return KEY_P;
		if(keyString == "KEY_Q")
			return KEY_Q;
		if(keyString == "KEY_R")
			return KEY_R;
		if(keyString == "KEY_S")
			return KEY_S;
		if(keyString == "KEY_T")
			return KEY_T;
		if(keyString == "KEY_U")
			return KEY_U;
		if(keyString == "KEY_V")
			return KEY_V;
		if(keyString == "KEY_W")
			return KEY_W;
		if(keyString == "KEY_X")
			return KEY_X;
		if(keyString == "KEY_Y")
			return KEY_Y;
		if(keyString == "KEY_Z")
			return KEY_Z;
		if(keyString == "KEY_ESCAPE")
			return KEY_ESCAPE;
		if(keyString == "KEY_ENTER")
			return KEY_ENTER;
		if(keyString == "KEY_LCTRL")
			return KEY_LCTRL;
		if(keyString == "KEY_RCTRL")
			return KEY_RCTRL;
		if(keyString == "KEY_LSHIFT")
			return KEY_LSHIFT;
		if(keyString == "KEY_RSHIFT")
			return KEY_RSHIFT;
		if(keyString == "KEY_SPACE")
			return KEY_SPACE;

		return KEY_UNKNOWN;
	}
}
}
