#include "Input/KeyMap.hpp"

namespace pe
{
namespace input
{

	KeyMap::KeyMap()
	{}

	KeyMap::KeyMap(const KeyMap& keymap)
		: actions(keymap.actions)
		, keys(keymap.keys)
	{
	}

	KeyMap::~KeyMap()
	{}

	PEvoid KeyMap::addKey(Keyboard key, const std::string& actionIdentifier)
	{
		// @TODO check is already existing
		actions.insert(std::make_pair(key, actionIdentifier));
		keys.insert(std::make_pair(actionIdentifier, key));
	}

	PEvoid KeyMap::removeKey(Keyboard key)
	{
		// @TODO check if exists
		std::string correspondingAction = actions[key];
		actions.erase(key);
		keys.erase(correspondingAction);
	}

	Keyboard KeyMap::getKeyForAction(const std::string& action)
	{
		return keys[action];
	}

	const std::string& KeyMap::getActionForKey(Keyboard key)
	{
		return actions[key];
	}

	PEbool KeyMap::hasKey(Keyboard key) const
	{
		return actions.find(key) != actions.end();
	}
}
}

