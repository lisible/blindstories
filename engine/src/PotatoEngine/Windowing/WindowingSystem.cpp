#include "Windowing/WindowingSystem.hpp"
#include "Messaging/Messaging.hpp"

namespace pe
{
namespace windowing
{
	WindowingSystem::WindowingSystem(IWindow* window)
		: window(window)
	{
		registerDefaultHandlers();	
	}

	WindowingSystem::~WindowingSystem()
	{
		delete window;	
	}

	PEvoid WindowingSystem::createWindow(const std::string& title, PEuint32 width, PEuint32 height)
	{
		window->create(title, width, height);

		// Notifying window creation
		auto messageData = new pemsg::SimpleMessageData();
		messageData->stringValue1 = title;
		messageData->integerValue1 = width;
		messageData->integerValue2 = height;

		sendMessage(pemsg::Message(pemsg::MSG_WINDOW_WINDOWCREATED,
									messageData,
									[=](void* data){
										auto md = reinterpret_cast<pemsg::SimpleMessageData*>(data);
										delete md;
									}));
											
	}

	PEvoid WindowingSystem::showWindow()
	{
		window->show();

		// Notifying window shown
		sendMessage(pemsg::Message(pemsg::MSG_WINDOW_WINDOWSHOWN));
	}

	PEvoid WindowingSystem::hideWindow()
	{
		window->hide();

		// Notifying window hidden
		sendMessage(pemsg::Message(pemsg::MSG_WINDOW_WINDOWHIDDEN));
	}

	PEvoid WindowingSystem::displayWindow()
	{
		window->render();
	}

	PEvoid WindowingSystem::clearWindow()
	{
		window->clear();
	}

	PEvoid WindowingSystem::registerDefaultHandlers()
	{
		// Registering window creation request callback
		registerCallback(pemsg::MSG_WINDOW_WINDOWCREATION_REQUEST,
						[&](auto&& p){onWindowCreationRequest(p);});

		// Registering window show request callback
		registerCallback(pemsg::MSG_WINDOW_WINDOWSHOW_REQUEST,
						[&](auto&& p){onWindowShowRequest(p);});
		
		// Registering window hide request callback
		registerCallback(pemsg::MSG_WINDOW_WINDOWHIDE_REQUEST,
						[&](auto&& p){onWindowHideRequest(p);});
		
		// Registering window display request callback
		registerCallback(pemsg::MSG_WINDOW_WINDOWDISPLAY_REQUEST,
						[&](auto&& p){onWindowDisplayRequest(p);});
		
		// Registering window clear request callback
		registerCallback(pemsg::MSG_WINDOW_WINDOWCLEAR_REQUEST,
						[&](auto&& p){onWindowClearRequest(p);});
		
		// Registering update request callback
		registerCallback(pemsg::MSG_UPDATE_REQUEST,
						[&](auto&& p){onUpdateRequest(p);});
	
	}

	PEvoid WindowingSystem::onWindowCreationRequest(PEvoid* data)
	{
		pemsg::SimpleMessageData* messageData = reinterpret_cast<pemsg::SimpleMessageData*>(data);

		createWindow(messageData->stringValue1,
					messageData->integerValue1,
					messageData->integerValue2);
	}

	PEvoid WindowingSystem::onWindowShowRequest(PEvoid* data)
	{
		showWindow();
	}

	PEvoid WindowingSystem::onWindowHideRequest(PEvoid* data)
	{
		hideWindow();
	}

	PEvoid WindowingSystem::onWindowDisplayRequest(PEvoid* data)
	{
		displayWindow();
	}

	PEvoid WindowingSystem::onWindowClearRequest(PEvoid* data)
	{
		clearWindow();
	}

	PEvoid WindowingSystem::onUpdateRequest(PEvoid* data)
	{
		bool run(true);
		while(run)
		{
			pe::input::InputEvent* event = new pe::input::InputEvent();
			run = window->pollEvent(event);

			if(run)
			{
				// Notifying of the input events
				sendMessage(pemsg::Message(pemsg::MSG_WINDOW_INPUTRECEIVED,
										   event,
										   [=](PEvoid* d){
											   delete event;
										   }));
			}
		}
	}
}
}
