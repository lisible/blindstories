#include "Windowing/SDLWindowingSystem.hpp"
#include "Messaging/Messages.hpp"
#include "Messaging/SimpleMessageData.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


namespace pe
{
namespace windowing
{
	SDLWindowingSystem::SDLWindowingSystem()
		: WindowingSystem(new SDLWindow())
	{}

	SDLWindowingSystem::~SDLWindowingSystem()
	{}

	PEvoid SDLWindowingSystem::init()
	{
		// Initializing SDL
		SDL_Init(SDL_INIT_VIDEO);

		// Setting up OpenGL version
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

		// Initializing SDL Image
		IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
	}

	PEvoid SDLWindowingSystem::shutdown()
	{
		// Quitting SDL Image
		IMG_Quit();

		// Quitting SDL
		SDL_Quit();
	}
}
}
