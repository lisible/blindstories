#include "Windowing/SDLWindow.hpp"
#include "GL/glew.h"

#include <cstring>

// TODO remove
#include <iostream>

namespace pe
{
namespace windowing
{
	SDLWindow::SDLWindow()
		: sdlWindow(nullptr)
	{}

	SDLWindow::~SDLWindow()
	{
		SDL_DestroyWindow(sdlWindow);	
	}

	PEvoid SDLWindow::create(const std::string& title, PEuint32 width, PEuint32 height)
	{
		// Creating the window
		sdlWindow = SDL_CreateWindow(title.c_str(),
									SDL_WINDOWPOS_CENTERED,
									SDL_WINDOWPOS_CENTERED,
									static_cast<int>(width),
									static_cast<int>(height),
									SDL_WINDOW_OPENGL);

		// Enabling text input
		SDL_StartTextInput();

		// Caching the window attributes
		windowTitle = title;
		windowWidth = width;
		windowHeight = height;

		// Creating the OpenGL context
		oglContext = SDL_GL_CreateContext(sdlWindow);

		// Initializing GLEW
		glewExperimental = GL_TRUE;
		GLenum glewStatus = glewInit();
		if(glewStatus != GLEW_OK)
		{
			// @TODO something
		}



		SDL_GL_SetSwapInterval(0);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	
	}

	PEvoid SDLWindow::show()
	{
		SDL_ShowWindow(sdlWindow);
	}

	PEvoid SDLWindow::hide()
	{
		SDL_HideWindow(sdlWindow);
	}

	PEvoid SDLWindow::render()
	{
		SDL_GL_SwapWindow(sdlWindow);
	}

	PEvoid SDLWindow::clear()
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	PEvoid SDLWindow::setTitle(const std::string& title)
	{
		windowTitle = title;
		SDL_SetWindowTitle(sdlWindow, title.c_str());	
	}

	const std::string& SDLWindow::getTitle() const
	{
		return windowTitle;
	}

	PEvoid SDLWindow::setWidth(PEuint32 width)
	{
		windowWidth = width;
		SDL_SetWindowSize(sdlWindow, width, getHeight());
	}

	PEuint32 SDLWindow::getWidth() const
	{
		return windowWidth;
	}

	PEvoid SDLWindow::setHeight(PEuint32 height)
	{
		windowHeight = height;
		SDL_SetWindowSize(sdlWindow, getWidth(), height);
	}

	PEuint32 SDLWindow::getHeight() const
	{
		return windowHeight;
	}

	/**
 	 * Translates the given SDL_Event into an input event
 	 * @param sdlEvent The SDL event to translate
 	 * @param event The input event
 	 */
	PEvoid translateSDLEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates the given SDL keydown event into an input event
 	 * @param sdlEvent The SDL keydown event
 	 * @param event The input event
 	 */
	PEvoid translateSDLKeydownEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates the given SDL keyup event into an input event
 	 * @param sdlEvent The SDL keyup event
 	 * @param event The input event
 	 */
	PEvoid translateSDLKeyupEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates the given SDL mouse motion event into an input event
 	 * @param sdlEvent The SDL mouse motion event
 	 * @param event The input event
 	 */
	PEvoid translateSDLMouseMotionEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates the given SDL mouse button down event into an input event
 	 * @param sdlEvent The SDL mouse button event
 	 * @param event The input event
 	 */
	PEvoid translateSDLMouseButtonDownEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates the given SDL mouse button up event into an input event
 	 * @param sdlEvent The SDL mouse button event
 	 * @param event The input event
 	 */
	PEvoid translateSDLMouseButtonUpEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
	 * Translates the given SDL TextInputEvent into an input event
	 * @param sdlEvent The SDL text input event
	 * @param event The input event
	 */
	PEvoid translateSDLTextInputEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event);
	/**
 	 * Translates an SDL Keycode into a pe::input::Keyboard enum value
 	 * @param sym The keycode to translate
 	 * @return The translated keycode
 	 */
	pe::input::Keyboard translateSDLKey(SDL_Keycode sym);
	/**
	 * Translates an SDL button code into a pe::input::MouseButton enum value
	 * @param button The button to translate
	 * @return The translated button
	 */
	pe::input::MouseButton translateSDLMouseButton(Uint8 button);

	
	PEbool SDLWindow::pollEvent(pe::input::InputEvent* event)
	{
		PEbool eventPolled(false);

		// TODO cleanup

		SDL_Event sdlEvent;
		eventPolled = SDL_PollEvent(&sdlEvent) == 1;
		if(eventPolled)
			translateSDLEvent(&sdlEvent, event);

		return eventPolled;
	}

	PEvoid translateSDLEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		switch(sdlEvent->type)
		{
			case SDL_KEYDOWN:
				translateSDLKeydownEvent(sdlEvent, event);
				break;
			case SDL_KEYUP:
				translateSDLKeyupEvent(sdlEvent, event);
				break;
			case SDL_MOUSEMOTION:
				translateSDLMouseMotionEvent(sdlEvent, event);
				break;
			case SDL_MOUSEBUTTONDOWN:
				translateSDLMouseButtonDownEvent(sdlEvent, event);
				break;
			case SDL_MOUSEBUTTONUP:
				translateSDLMouseButtonUpEvent(sdlEvent, event);
				break;
			case SDL_TEXTINPUT:
				translateSDLTextInputEvent(sdlEvent, event);
				break;
			default:
				event->type = pe::input::InputEvent::UNKNOWN;
				break;
		}
	}

	PEvoid translateSDLKeydownEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::KEY_PRESSED;
		event->keyboard.key = translateSDLKey(sdlEvent->key.keysym.sym);
	}
	PEvoid translateSDLKeyupEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::KEY_RELEASED;
		event->keyboard.key = translateSDLKey(sdlEvent->key.keysym.sym);
	}
	PEvoid translateSDLMouseMotionEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::MOUSE_MOVED;
		event->mouseMotion.x = sdlEvent->motion.x;
		event->mouseMotion.y = sdlEvent->motion.y;
		event->mouseMotion.relX = sdlEvent->motion.xrel;
		event->mouseMotion.relY = sdlEvent->motion.yrel;
	}
	PEvoid translateSDLMouseButtonDownEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::MOUSE_BUTTON_DOWN;
		event->mouseClick.x = sdlEvent->button.x;
		event->mouseClick.y = sdlEvent->button.y;
		event->mouseClick.button = translateSDLMouseButton(sdlEvent->button.button);
	}

	PEvoid translateSDLMouseButtonUpEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::MOUSE_BUTTON_UP;
		event->mouseClick.x = sdlEvent->button.x;
		event->mouseClick.y = sdlEvent->button.y;
		event->mouseClick.button = translateSDLMouseButton(sdlEvent->button.button);
	}

	PEvoid translateSDLTextInputEvent(SDL_Event* sdlEvent, pe::input::InputEvent* event)
	{
		event->type = pe::input::InputEvent::TEXT_TYPED;

		char* textPtr = event->text.text;
		std::strcpy(textPtr, sdlEvent->text.text);
	}

	pe::input::MouseButton translateSDLMouseButton(Uint8 button)
	{
		switch(button)
		{
			case SDL_BUTTON_LEFT:
				return pe::input::MouseButton::LEFT;
			case SDL_BUTTON_RIGHT:
				return pe::input::MouseButton::RIGHT;
			default:
				return pe::input::MouseButton::UNKNOWN;
		}
	}

	pe::input::Keyboard translateSDLKey(SDL_Keycode sym)
	{
		switch(sym)
		{
			case SDLK_a:
				return pe::input::Keyboard::KEY_A;
			case SDLK_b:
				return pe::input::Keyboard::KEY_B;
			case SDLK_c:
				return pe::input::Keyboard::KEY_C;
			case SDLK_d:
				return pe::input::Keyboard::KEY_D;
			case SDLK_e:
				return pe::input::Keyboard::KEY_E;
			case SDLK_f:
				return pe::input::Keyboard::KEY_F;
			case SDLK_g:
				return pe::input::Keyboard::KEY_G;
			case SDLK_h:
				return pe::input::Keyboard::KEY_H;
			case SDLK_i:
				return pe::input::Keyboard::KEY_I;
			case SDLK_j:
				return pe::input::Keyboard::KEY_J;
			case SDLK_k:
				return pe::input::Keyboard::KEY_K;
			case SDLK_l:
				return pe::input::Keyboard::KEY_L;
			case SDLK_m:
				return pe::input::Keyboard::KEY_M;
			case SDLK_n:
				return pe::input::Keyboard::KEY_N;
			case SDLK_o:
				return pe::input::Keyboard::KEY_O;
			case SDLK_p:
				return pe::input::Keyboard::KEY_P;
			case SDLK_q:
				return pe::input::Keyboard::KEY_Q;
			case SDLK_r:
				return pe::input::Keyboard::KEY_R;
			case SDLK_s:
				return pe::input::Keyboard::KEY_S;
			case SDLK_t:
				return pe::input::Keyboard::KEY_T;
			case SDLK_u:
				return pe::input::Keyboard::KEY_U;
			case SDLK_v:
				return pe::input::Keyboard::KEY_V;
			case SDLK_w:
				return pe::input::Keyboard::KEY_W;
			case SDLK_x:
				return pe::input::Keyboard::KEY_X;
			case SDLK_y:
				return pe::input::Keyboard::KEY_Y;
			case SDLK_z:
				return pe::input::Keyboard::KEY_Z;	
			case SDLK_ESCAPE:
				return pe::input::Keyboard::KEY_ESCAPE;
			case SDLK_LSHIFT:
				return pe::input::Keyboard::KEY_LSHIFT;
			case SDLK_RSHIFT:
				return pe::input::Keyboard::KEY_RSHIFT;
			case SDLK_SPACE:
				return pe::input::Keyboard::KEY_SPACE;
			case SDLK_RETURN:
			case SDLK_KP_ENTER:
				return pe::input::Keyboard::KEY_ENTER;
			case SDLK_UP:
				return pe::input::Keyboard::KEY_UP;
			case SDLK_DOWN:
				return pe::input::Keyboard::KEY_DOWN;
			case SDLK_LEFT:
				return pe::input::Keyboard::KEY_LEFT;
			case SDLK_RIGHT:
				return pe::input::Keyboard::KEY_RIGHT;
			case SDLK_TAB:
				return pe::input::Keyboard::KEY_TAB;
			case SDLK_BACKSPACE:
				return pe::input::Keyboard::KEY_BACKSPACE;
			default:
				return pe::input::Keyboard::KEY_UNKNOWN;
		}
	}
}
}
