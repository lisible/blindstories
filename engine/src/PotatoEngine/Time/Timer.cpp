#include "Time/Timer.hpp"

namespace pe
{
namespace time
{
    Timer::Timer()
    {
    }
    Timer::~Timer()
    {
    }

    PEvoid Timer::start()
    {
        startTime = std::chrono::high_resolution_clock::now();
    }

    std::chrono::milliseconds::rep Timer::getTime()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - startTime).count();
    }
    std::chrono::milliseconds::rep Timer::restart()
    {
        auto time = getTime();
        start();
        return time;
    }
}
}