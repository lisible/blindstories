#include "Scene/SceneStack.hpp"

namespace pe
{
namespace scene
{
	SceneStack::SceneStack()
	{}

	SceneStack::~SceneStack()
	{
		while(!isEmpty())
			popScene();	
	}

	PEvoid SceneStack::pushScene(Scene* scene)
	{
		stack.push(scene);
	}

	PEvoid SceneStack::popScene()
	{
		if(stack.size() > 0)
		{
			delete stack.top();
			stack.pop();
		}
	}

	PEvoid SceneStack::changeScene(Scene* scene)
	{
		popScene();
		pushScene(scene);
	}

	Scene* SceneStack::currentScene() const
	{
		return stack.top();
	}

	PEbool SceneStack::isEmpty() const
	{
		return stack.empty();
	}
}
}
