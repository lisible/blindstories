#include "Scene/Scene.hpp"
#include "Logic/LogicSystem.hpp"

namespace pe
{
namespace scene
{
	Scene::Scene()
		: logicSystem(nullptr)
	{}

	Scene::~Scene()
	{}

	PEvoid Scene::setLogicSystem(pe::logic::LogicSystem* logicSystem)
	{
		this->logicSystem = logicSystem;
	}

	PEvoid Scene::pop_scene()
	{
		logicSystem->popScene();
	}
	PEvoid Scene::push_scene(const std::string& sceneIdentifier)
	{
		logicSystem->pushScene(sceneIdentifier);
	}
	PEvoid Scene::change_scene(const std::string& sceneIdentifier)
	{
		logicSystem->changeScene(sceneIdentifier);
	}

    PEvoid Scene::log_message(const std::string& message)
    {
        logicSystem->logMessage(message);
    }

	PEvoid Scene::ui_add_component(const std::string& parentIdentifier, ui::GUIComponent* component)
	{
		logicSystem->uiAddComponent(parentIdentifier, component);
	}

    PEvoid Scene::ui_edit_attribute(const std::string& componentIdentifier,
									const std::string& attribute,
									const std::string& value)
    {
        logicSystem->changeUIAttribute(componentIdentifier, attribute, value);
    }
	
	PEvoid Scene::ui_focus_next()
	{
		logicSystem->uiFocusNext();
	}
	PEvoid Scene::ui_focus_previous()
	{
		logicSystem->uiFocusPrevious();
	}
	PEvoid Scene::ui_post()
	{
		logicSystem->uiPost();
	}

	PEvoid Scene::tts_say(const std::string& text)
	{
		logicSystem->ttsSay(text);
	}

	tinyxml2::XMLDocument* Scene::open_data_file(const std::string& file)
	{
		tinyxml2::XMLDocument* document = new tinyxml2::XMLDocument;
		std::string path("data/custom/" + file);
		document->LoadFile(path.c_str());
		return document;
	}

	PEvoid Scene::close_data_file(tinyxml2::XMLDocument *file)
	{
		delete file;
	}

	PEvoid Scene::onActionPressed(const std::string& action)
	{
	}
	PEvoid Scene::onTextTyped(const std::string& text)
	{
	}

	PEvoid Scene::register_click_callback(const std::string& componentIdentifier,
										  std::function<PEvoid(PEvoid)> callback)
	{
		logicSystem->registerUIClickCallback(componentIdentifier, callback);
	}

	PEvoid Scene::register_post_callback(const std::string& componentIdentifier, std::function<PEvoid(PEvoid)> callback)
	{
		logicSystem->registerUIPostCallback(componentIdentifier, callback);
	}
}
}
